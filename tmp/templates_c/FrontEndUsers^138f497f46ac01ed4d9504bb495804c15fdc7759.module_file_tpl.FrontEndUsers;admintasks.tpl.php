<?php /* Smarty version Smarty-3.1.14, created on 2013-10-24 14:07:25
         compiled from "module_file_tpl:FrontEndUsers;admintasks.tpl" */ ?>
<?php /*%%SmartyHeaderCode:126948533852696fedca66e3-19047917%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '138f497f46ac01ed4d9504bb495804c15fdc7759' => 
    array (
      0 => 'module_file_tpl:FrontEndUsers;admintasks.tpl',
      1 => 1382022836,
      2 => 'module_file_tpl',
    ),
  ),
  'nocache_hash' => '126948533852696fedca66e3-19047917',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'message' => 0,
    'startform' => 0,
    'input_hidden' => 0,
    'legend_importusers' => 0,
    'info_importusersfileformat' => 0,
    'prompt_importuserstogroup' => 0,
    'input_importuserstogroup' => 0,
    'mod' => 0,
    'actionid' => 0,
    'prompt_importusersfile' => 0,
    'input_importusersfile' => 0,
    'input_importusersbtn' => 0,
    'legend_exportusers' => 0,
    'prompt_exportusers' => 0,
    'input_exportusers' => 0,
    'legend_userhistorymaintenance' => 0,
    'prompt_exportuserhistory' => 0,
    'input_exportuserhistory' => 0,
    'button_exportuserhistory' => 0,
    'prompt_clearuserhistory' => 0,
    'input_clearuserhistory' => 0,
    'button_clearuserhistory' => 0,
    'endform' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_52696fedd8d8a4_88054931',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52696fedd8d8a4_88054931')) {function content_52696fedd8d8a4_88054931($_smarty_tpl) {?><?php if (isset($_smarty_tpl->tpl_vars['message']->value)){?><p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>


<?php echo $_smarty_tpl->tpl_vars['startform']->value;?>
<?php echo $_smarty_tpl->tpl_vars['input_hidden']->value;?>

<fieldset>
<legend><?php echo $_smarty_tpl->tpl_vars['legend_importusers']->value;?>
</legend>
<?php echo $_smarty_tpl->tpl_vars['info_importusersfileformat']->value;?>
<hr/>
<div class="pageoverflow">
  <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_importuserstogroup']->value;?>
:</p>
  <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_importuserstogroup']->value;?>
</p>
</div>
<div class="pageoverflow">
  <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('ignore_userid');?>
:</p>
  <p class="pageinput">
    <input type="checkbox" name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
import_ignore_userid" value="1"/>
    <br/> 
    <?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('info_ignore_userid');?>

  </p>
</div>
<div class="pageoverflow">
   <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_importusersfile']->value;?>
:</p>
   <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_importusersfile']->value;?>
</p>
</div>
<div class="pageoverflow">
  <p class="pagetext">&nbsp;</p>
  <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_importusersbtn']->value;?>
</p>
</div>
</fieldset>

<fieldset>
<legend><?php echo $_smarty_tpl->tpl_vars['legend_exportusers']->value;?>
</legend>
<div class="pageoverflow">
  <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('export_passhash');?>
:</p>
  <p class="pageinput">
    <input type="checkbox" name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
export_passhash" value="1"/>
    <br/>
    <?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('info_export_passhash');?>

  </p>
</div>
<div class="pageoverflow">
  <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_exportusers']->value;?>
:</p>
  <p class="pageinput">
    <?php echo $_smarty_tpl->tpl_vars['input_exportusers']->value;?>

  </p>
</div>
</fieldset>

<fieldset>
<legend><?php echo $_smarty_tpl->tpl_vars['legend_userhistorymaintenance']->value;?>
</legend>
<div class="pageoverflow">
  <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_exportuserhistory']->value;?>
:</p>
  <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_exportuserhistory']->value;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['button_exportuserhistory']->value;?>
</p>
</div>
<div class="pageoverflow">
  <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_clearuserhistory']->value;?>
:</p>
  <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_clearuserhistory']->value;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['button_clearuserhistory']->value;?>
</p>
</div>
</fieldset>
<?php echo $_smarty_tpl->tpl_vars['endform']->value;?>

<?php }} ?>