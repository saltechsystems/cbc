<?php /* Smarty version Smarty-3.1.14, created on 2013-10-24 14:07:25
         compiled from "module_file_tpl:FrontEndUsers;userlist.tpl" */ ?>
<?php /*%%SmartyHeaderCode:119205401352696fed708d03-83901893%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cc4641aa229c7d51a77024e54e94e9e0578f9a14' => 
    array (
      0 => 'module_file_tpl:FrontEndUsers;userlist.tpl',
      1 => 1382022836,
      2 => 'module_file_tpl',
    ),
  ),
  'nocache_hash' => '119205401352696fed708d03-83901893',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'feuactionid' => 0,
    'mod' => 0,
    'startform' => 0,
    'prompt_filter' => 0,
    'prompt_group' => 0,
    'filter_group' => 0,
    'prompt_userfilter' => 0,
    'filter_regex' => 0,
    'prompt_propertyfiltersel' => 0,
    'filter_propertysel' => 0,
    'prompt_propertyfilter' => 0,
    'filter_property' => 0,
    'prompt_loggedinonly' => 0,
    'filter_loggedinonly' => 0,
    'prompt_limit' => 0,
    'filter_limit' => 0,
    'actionid' => 0,
    'alldefns' => 0,
    'viewprops' => 0,
    'prompt_sortby' => 0,
    'filter_sortby' => 0,
    'input_select' => 0,
    'input_hidden' => 0,
    'filter_applied' => 0,
    'numusers' => 0,
    'usersingroup' => 0,
    'navigation' => 0,
    'itemcount' => 0,
    'usernametext' => 0,
    'createdtext' => 0,
    'expirestext' => 0,
    'one' => 0,
    'items' => 0,
    'entry' => 0,
    'addlink' => 0,
    'perm_removeusers' => 0,
    'endform' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_52696feda4fff1_24787791',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52696feda4fff1_24787791')) {function content_52696feda4fff1_24787791($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include '/var/www/html/lib/smarty/plugins/function.html_options.php';
?>
<script type="text/javascript">
/*<![CDATA[*/
$(document).ready(function(){
  $('#feu_filterbox').click(function(){
    if( this.checked ) {
      $('#feu_filterform').show();
    }
    else {
      $('#feu_filterform').hide();
    }
  });
});
function select_all()
{
  cb = document.getElementsByName('<?php echo $_smarty_tpl->tpl_vars['feuactionid']->value;?>
selected[]');
  el = document.getElementById('selectall');
  st = el.checked;
  for( i = 0; i < cb.length; i++ )
  {
    if( cb[i].type == "checkbox" )
    {
      cb[i].checked=st;
    }
  }
}

function confirm_delete()
{
  var cb = document.getElementsByName('<?php echo $_smarty_tpl->tpl_vars['feuactionid']->value;?>
selected[]');
  var count = 0;
  for( i = 0; i < cb.length; i++ )
  {
     if( cb[i].checked )
     {
       count++;
     }
  }

  if( count > 250 )
  {
     alert('<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('error_toomanyselected');?>
');
     return false;
  }
  return confirm('<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('confirm_delete_selected');?>
');
}

/*]]> */
</script>


<?php echo $_smarty_tpl->tpl_vars['startform']->value;?>

<div id="feu_filterform" style="display: none;">
<table width="100%">
<tr><td width="50%" valign="top">
  <fieldset>
  <legend><?php echo $_smarty_tpl->tpl_vars['prompt_filter']->value;?>
:</legend>
  <div class="pageoverflow">
   <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_group']->value;?>
:</p>
   <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['filter_group']->value;?>
</p>
  </div>
  <div class="pageoverflow">
   <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_userfilter']->value;?>
:</p>
   <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['filter_regex']->value;?>
</p>
  </div>
  <div class="pageoverflow">
   <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_propertyfiltersel']->value;?>
:</p>
   <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['filter_propertysel']->value;?>
</p>
  </div>
  <div class="pageoverflow">
   <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_propertyfilter']->value;?>
:</p>
   <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['filter_property']->value;?>
</p>
  </div>
  <div class="pageoverflow">
   <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_loggedinonly']->value;?>
:</p>
   <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['filter_loggedinonly']->value;?>
</p>
  </div>
  <div class="pageoverflow">
   <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_limit']->value;?>
:</p>
   <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['filter_limit']->value;?>
</p>
  </div>
  </fieldset>
</td><td valign="top">
  <fieldset>
  <legend><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('view');?>
:</legend>
  <div class="pageoverflow">
   <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_viewprops');?>
:</p>
   <p class="pageinput">
     <select name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
filter_viewprops[]" multiple="multiple" size="5">
       <?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['alldefns']->value,'selected'=>$_smarty_tpl->tpl_vars['viewprops']->value),$_smarty_tpl);?>

     </select>
   </p>
  </div>
  <div class="pageoverflow">
   <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_sortby']->value;?>
:</p>
   <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['filter_sortby']->value;?>
</p>
  </div>
</fieldset>
</td></tr>
<tr>
  <td colspan="2" align="center"><?php echo $_smarty_tpl->tpl_vars['input_select']->value;?>
<?php echo $_smarty_tpl->tpl_vars['input_hidden']->value;?>
</td>
</tr>
</table>
<br/>
</div>

<div class="pageoverflow">
 <div style="width: 50%; float: left;">
  <input id="feu_filterbox" type="checkbox" value="1"/><label for="feu_filterbox"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('view_filter');?>
 <?php if ($_smarty_tpl->tpl_vars['filter_applied']->value){?>(<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('applied');?>
)<?php }?></label>&nbsp;
   <?php echo $_smarty_tpl->tpl_vars['numusers']->value;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['usersingroup']->value;?>

 </div>
 <div style="width: 49%; float: right; text-align: right;">
 <?php if (isset($_smarty_tpl->tpl_vars['navigation']->value['firstpage_url'])){?>
   <a href="<?php echo $_smarty_tpl->tpl_vars['navigation']->value['firstpage_url'];?>
"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('firstpage');?>
</a>&nbsp;
   <a href="<?php echo $_smarty_tpl->tpl_vars['navigation']->value['prevpage_url'];?>
"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prevpage');?>
</a>&nbsp;
 <?php }?>
 <?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('page');?>
 <?php echo $_smarty_tpl->tpl_vars['navigation']->value['curpage'];?>
 <?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_of');?>
 <?php echo $_smarty_tpl->tpl_vars['navigation']->value['npages'];?>

 <?php if (isset($_smarty_tpl->tpl_vars['navigation']->value['lastpage_url'])){?>
   &nbsp;<a href="<?php echo $_smarty_tpl->tpl_vars['navigation']->value['nextpage_url'];?>
"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('nextpage');?>
</a>
   &nbsp;<a href="<?php echo $_smarty_tpl->tpl_vars['navigation']->value['lastpage_url'];?>
"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('lastpage');?>
</a>
 <?php }?>
 </div>
</div>
<?php if ($_smarty_tpl->tpl_vars['itemcount']->value>0){?>
<table cellspacing="0" class="pagetable cms_sortable tablesorter">
	<thead>
		<tr>
			<th><?php echo $_smarty_tpl->tpl_vars['usernametext']->value;?>
</th>
			<th><?php echo $_smarty_tpl->tpl_vars['createdtext']->value;?>
</th>
			<th><?php echo $_smarty_tpl->tpl_vars['expirestext']->value;?>
</th>
                        <?php if (isset($_smarty_tpl->tpl_vars['viewprops']->value)&&is_array($_smarty_tpl->tpl_vars['viewprops']->value)){?>
                        <?php  $_smarty_tpl->tpl_vars['one'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['one']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['viewprops']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['one']->key => $_smarty_tpl->tpl_vars['one']->value){
$_smarty_tpl->tpl_vars['one']->_loop = true;
?>
                        <th><?php echo $_smarty_tpl->tpl_vars['alldefns']->value[$_smarty_tpl->tpl_vars['one']->value];?>
</th>
                        <?php } ?>
                        <?php }?>
			<th class="pageicon {sorter: false}">&nbsp;</th>
			<th class="pageicon {sorter: false}">&nbsp;</th>
			<th class="pageicon {sorter: false}">&nbsp;</th>
			<th class="pageicon {sorter: false}">&nbsp;</th>
			<th class="pageicon {sorter: false}"><input id="selectall" type="checkbox" name="junk" onclick="select_all();"/></th>
		</tr>
	</thead>
	<tbody>
<?php  $_smarty_tpl->tpl_vars['entry'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['entry']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['entry']->key => $_smarty_tpl->tpl_vars['entry']->value){
$_smarty_tpl->tpl_vars['entry']->_loop = true;
?>
		<tr>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->username;?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->created;?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->expires;?>
</td>
                        <?php if (isset($_smarty_tpl->tpl_vars['viewprops']->value)&&isset($_smarty_tpl->tpl_vars['entry']->value->extra)){?>
                        <?php  $_smarty_tpl->tpl_vars['one'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['one']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['viewprops']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['one']->key => $_smarty_tpl->tpl_vars['one']->value){
$_smarty_tpl->tpl_vars['one']->_loop = true;
?>
                        <td><?php if (isset($_smarty_tpl->tpl_vars['entry']->value->extra[$_smarty_tpl->tpl_vars['one']->value])&&$_smarty_tpl->tpl_vars['entry']->value->extra[$_smarty_tpl->tpl_vars['one']->value]){?><?php echo $_smarty_tpl->tpl_vars['entry']->value->extra[$_smarty_tpl->tpl_vars['one']->value];?>
<?php }?></td>
                        <?php } ?>
                        <?php }?>
			<td><?php if (isset($_smarty_tpl->tpl_vars['entry']->value->logoutlink)){?><?php echo $_smarty_tpl->tpl_vars['entry']->value->logoutlink;?>
<?php }?></td>
			<td><?php echo (($tmp = @$_smarty_tpl->tpl_vars['entry']->value->historylink)===null||$tmp==='' ? '' : $tmp);?>
</td>
			<td><?php echo (($tmp = @$_smarty_tpl->tpl_vars['entry']->value->editlink)===null||$tmp==='' ? '' : $tmp);?>
</td>
			<td><?php if (isset($_smarty_tpl->tpl_vars['entry']->value->deletelink)){?><?php echo $_smarty_tpl->tpl_vars['entry']->value->deletelink;?>
<?php }?></td>
			<td><input type="checkbox" name="<?php echo $_smarty_tpl->tpl_vars['feuactionid']->value;?>
selected[]" value="<?php echo $_smarty_tpl->tpl_vars['entry']->value->id;?>
"/></td>
		</tr>	 
<?php } ?>
	</tbody>
</table>
<?php }?>
<div class="pageoverflow">
 <div style="float: left;"><?php echo $_smarty_tpl->tpl_vars['addlink']->value;?>
</div>
 <div style="float: right;">
   <?php if (isset($_smarty_tpl->tpl_vars['perm_removeusers']->value)&&$_smarty_tpl->tpl_vars['perm_removeusers']->value==1){?><input type="submit" name="<?php echo $_smarty_tpl->tpl_vars['feuactionid']->value;?>
bulkdelete" value="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('delete_selected');?>
" onclick="return confirm_delete();"/><?php }?>
 </div>
</div>
<?php echo $_smarty_tpl->tpl_vars['endform']->value;?>

<?php }} ?>