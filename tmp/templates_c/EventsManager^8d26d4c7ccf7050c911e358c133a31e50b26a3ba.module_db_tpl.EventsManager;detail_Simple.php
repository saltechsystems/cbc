<?php /* Smarty version Smarty-3.1.14, created on 2013-10-24 13:32:04
         compiled from "module_db_tpl:EventsManager;detail_Simple" */ ?>
<?php /*%%SmartyHeaderCode:2979183015268620102cd29-88327409%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8d26d4c7ccf7050c911e358c133a31e50b26a3ba' => 
    array (
      0 => 'module_db_tpl:EventsManager;detail_Simple',
      1 => 1382639500,
      2 => 'module_db_tpl',
    ),
  ),
  'nocache_hash' => '2979183015268620102cd29-88327409',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_526862011a7212_31728486',
  'variables' => 
  array (
    'entry' => 0,
    'oneval' => 0,
    'onecheckbox' => 0,
    'FrontEndUsers' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_526862011a7212_31728486')) {function content_526862011a7212_31728486($_smarty_tpl) {?><?php if (!is_callable('smarty_cms_modifier_cms_date_format')) include '/var/www/html/plugins/modifier.cms_date_format.php';
if (!is_callable('smarty_function_math')) include '/var/www/html/lib/smarty/plugins/function.math.php';
?>

<?php echo FrontEndUsers::function_plugin(array('form'=>"logout",'returnlast'=>"1"),$_smarty_tpl);?>

<h3><?php echo $_smarty_tpl->tpl_vars['entry']->value->name;?>
</h3>
<?php if (isset($_smarty_tpl->tpl_vars['entry']->value->category)){?>
	<p>Category: <?php echo $_smarty_tpl->tpl_vars['entry']->value->category->name;?>
</p>
<?php }?>
<p>Start: <?php echo smarty_cms_modifier_cms_date_format($_smarty_tpl->tpl_vars['entry']->value->start_datetime,"M d, Y @ g:ia");?>
</p>
<p>End: <?php echo smarty_cms_modifier_cms_date_format($_smarty_tpl->tpl_vars['entry']->value->end_datetime,"M d, Y @ g:ia");?>
</p>
<p><?php echo $_smarty_tpl->tpl_vars['entry']->value->description;?>
</p>


<?php if (isset($_smarty_tpl->tpl_vars['entry']->value->fields)){?>
	<?php  $_smarty_tpl->tpl_vars['oneval'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['oneval']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['entry']->value->fields; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['oneval']->key => $_smarty_tpl->tpl_vars['oneval']->value){
$_smarty_tpl->tpl_vars['oneval']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['oneval']->key;
?>
		<h4><?php echo $_smarty_tpl->tpl_vars['oneval']->value->name;?>
</h4>
		<div>
			<?php if ($_smarty_tpl->tpl_vars['oneval']->value->type=='image'&&$_smarty_tpl->tpl_vars['oneval']->value->value!=''){?>
				<img src="<?php echo $_smarty_tpl->tpl_vars['oneval']->value->file_url;?>
" style="max-width: 200px" />
			<?php }elseif($_smarty_tpl->tpl_vars['oneval']->value->type=='checkboxes'&&$_smarty_tpl->tpl_vars['oneval']->value->value!=''){?>
				<ul>
					<?php  $_smarty_tpl->tpl_vars['onecheckbox'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['onecheckbox']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['oneval']->value->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['onecheckbox']->key => $_smarty_tpl->tpl_vars['onecheckbox']->value){
$_smarty_tpl->tpl_vars['onecheckbox']->_loop = true;
?>
						<li><?php echo $_smarty_tpl->tpl_vars['onecheckbox']->value;?>
</li>
					<?php } ?>
				</ul>
			<?php }elseif($_smarty_tpl->tpl_vars['oneval']->value->value!=''){?>
				<?php echo $_smarty_tpl->tpl_vars['oneval']->value->value;?>

			<?php }else{ ?>
				<p>No value for <?php echo $_smarty_tpl->tpl_vars['oneval']->value->name;?>
</p>
			<?php }?>
		</div>
		<br /><br />

		
		
	<?php } ?>
<?php }?>


<p>Registered places: <?php echo $_smarty_tpl->tpl_vars['entry']->value->nb_regusers;?>
 / Capacity: <?php if ($_smarty_tpl->tpl_vars['entry']->value->capacity>0){?><?php echo $_smarty_tpl->tpl_vars['entry']->value->capacity;?>
 / Remaining places: <?php echo smarty_function_math(array('equation'=>"x - y",'x'=>$_smarty_tpl->tpl_vars['entry']->value->capacity,'y'=>$_smarty_tpl->tpl_vars['entry']->value->nb_regusers),$_smarty_tpl);?>
 <?php }else{ ?>Unlimited<?php }?></p>

<?php if ($_smarty_tpl->tpl_vars['FrontEndUsers']->value->loggedIn()){?>
	<?php echo EventsManager::function_plugin(array('action'=>'register','event_id'=>$_smarty_tpl->tpl_vars['entry']->value->id,'redirectdetail'=>1),$_smarty_tpl);?>

<?php }else{ ?>
	<h2>Please <?php echo cms_user_tag_FEULoginAndReturn(array('linkonly'=>1),$_smarty_tpl);?>
 to register for the event</h2>
<?php }?>
<?php echo cms_user_tag_event_back_link(array(),$_smarty_tpl);?>
<?php }} ?>