<?php /* Smarty version Smarty-3.1.14, created on 2013-10-24 14:06:58
         compiled from "module_file_tpl:EventsManager;admin_header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:135539617152696fd294fad3-54589457%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4235b971321483a30de8c29ad9c31fa9fbb21c7e' => 
    array (
      0 => 'module_file_tpl:EventsManager;admin_header.tpl',
      1 => 1382022850,
      2 => 'module_file_tpl',
    ),
  ),
  'nocache_hash' => '135539617152696fd294fad3-54589457',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'datepicker_lang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_52696fd2a12496_31922434',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52696fd2a12496_31922434')) {function content_52696fd2a12496_31922434($_smarty_tpl) {?><?php if (!is_callable('smarty_function_root_url')) include '/var/www/html/plugins/function.root_url.php';
?><script type="text/javascript" src="<?php echo smarty_function_root_url(array(),$_smarty_tpl);?>
/modules/EventsManager/js/datepicker-locale/jquery.ui.datepicker-<?php echo $_smarty_tpl->tpl_vars['datepicker_lang']->value;?>
.js"></script>
<?php if ($_smarty_tpl->tpl_vars['datepicker_lang']->value!='en'){?>
	<script type="text/javascript" src="<?php echo smarty_function_root_url(array(),$_smarty_tpl);?>
/modules/EventsManager/js/i18n/jquery.ui.timepicker-<?php echo $_smarty_tpl->tpl_vars['datepicker_lang']->value;?>
.js"></script>
<?php }?>
<script type="text/javascript" src="<?php echo smarty_function_root_url(array(),$_smarty_tpl);?>
/modules/EventsManager/js/jquery.ui.timepicker.js"></script>
<script type="text/javascript" src="<?php echo smarty_function_root_url(array(),$_smarty_tpl);?>
/modules/AireLibs/js/jquery.ui.ufd.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo smarty_function_root_url(array(),$_smarty_tpl);?>
/lib/jquery/css/smoothness/jquery-ui-1.8.12.custom.css" />
<link rel="stylesheet" type="text/css" href="<?php echo smarty_function_root_url(array(),$_smarty_tpl);?>
/modules/EventsManager/js/jquery.ui.timepicker.css" />
<link rel="stylesheet" type="text/css" href="<?php echo smarty_function_root_url(array(),$_smarty_tpl);?>
/modules/AireLibs/js/css/ufd-base.css" />
<link rel="stylesheet" type="text/css" href="<?php echo smarty_function_root_url(array(),$_smarty_tpl);?>
/modules/AireLibs/js/css/plain.css" />

<script style="text/javascript">

	$(function() {
		$.datepicker.setDefaults( $.datepicker.regional["<?php echo $_smarty_tpl->tpl_vars['datepicker_lang']->value;?>
"] );
		// Event dates
		var dates = $( "#m1_start_date, #m1_end_date" ).datepicker({
			defaultDate: "+1w",
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			numberOfMonths: 3,
			onSelect: function( selectedDate ) {
				var option = this.id == "m1_start_date" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
				
				if ( (this.id == "m1_start_date") && $('#m1_end_date').val() == '')
				{
					$('#m1_end_date').val($('#m1_start_date').val());
				}
			}
		});
		// Registration period dates
		var dates2 = $( "#m1_reg_start_date, #m1_reg_end_date" ).datepicker({
			defaultDate: "+1w",
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			numberOfMonths: 3,
			onSelect: function( selectedDate ) {
				var option = this.id == "m1_reg_start_date" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates2.not( this ).datepicker( "option", option, date );

				if ( (this.id == "m1_reg_start_date") && $('#m1_reg_end_date').val() == '')
				{
					$('#m1_reg_end_date').val($('#m1_reg_start_date').val());
				}
			}
		});
		$('#m1_start_time').timepicker();
		$('#m1_end_time').timepicker();
		$('#m1_reg_start_time').timepicker();
		$('#m1_reg_end_time').timepicker();
		
		// Display / hide the registrations informations in event edition
		if ($('#allow_registration input').attr('checked'))
		{
			$('#options_registration').show();
		}
		$('#allow_registration input').click(function () {
			$('#options_registration').toggle();
		});

		// Display / hide the registrations period informations in event edition
		if ($('#limited_reg_period input').attr('checked'))
		{
			$('#reg_period_datetimes').show();
		}
		$('#limited_reg_period input').click(function () {
			$('#reg_period_datetimes').toggle();
		});

		// Select filter in the backend registration page
		$('#user_select select').ufd();

		// Hide capacity in event edit
		$('#m1_capacity_unlimited').change(function () {
			$('#m1_capacity_input').toggle();
		});

		// Hide capacity per feu in event edit
		$('#m1_capacity_per_feu_unlimited').change(function () {
			$('#m1_capacity_per_feu_input').toggle();
		});
	});


</script><?php }} ?>