<?php /* Smarty version Smarty-3.1.14, created on 2013-10-24 14:06:53
         compiled from "module_file_tpl:EventsManager;admin_tab_mails.tpl" */ ?>
<?php /*%%SmartyHeaderCode:40594464052696fcd438fb7-69412037%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'caaf18c6cc893a0eb2f5bf56a9aad6e0d6e81347' => 
    array (
      0 => 'module_file_tpl:EventsManager;admin_tab_mails.tpl',
      1 => 1382022850,
      2 => 'module_file_tpl',
    ),
  ),
  'nocache_hash' => '40594464052696fcd438fb7-69412037',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'form_start' => 0,
    'mod' => 0,
    'newregistration_mail_sendtouser_input' => 0,
    'newregistration_mail_sendtouser_label' => 0,
    'newregistration_mail_sendtoother_input' => 0,
    'newregistration_mail_sendtoother_label' => 0,
    'newregistration_mail_sendtoother_mails_input' => 0,
    'mail_newregistration_template_label' => 0,
    'mail_newregistration_template_help' => 0,
    'mail_newregistration_subject_label' => 0,
    'mail_newregistration_subject_input' => 0,
    'mail_newregistration_template_input' => 0,
    'submit' => 0,
    'cancel' => 0,
    'cancelregistration_mail_sendtouser_input' => 0,
    'cancelregistration_mail_sendtouser_label' => 0,
    'cancelregistration_mail_sendtoother_input' => 0,
    'cancelregistration_mail_sendtoother_label' => 0,
    'cancelregistration_mail_sendtoother_mails_input' => 0,
    'mail_cancelregistration_template_label' => 0,
    'mail_cancelregistration_template_help' => 0,
    'mail_cancelregistration_subject_label' => 0,
    'mail_cancelregistration_subject_input' => 0,
    'mail_cancelregistration_template_input' => 0,
    'form_end' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_52696fcd5270e3_37073305',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52696fcd5270e3_37073305')) {function content_52696fcd5270e3_37073305($_smarty_tpl) {?><?php echo $_smarty_tpl->tpl_vars['form_start']->value;?>


<fieldset>
	<legend><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('mail_newregistration_title');?>
</legend>
	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('email_to_who_newregistration');?>
</p>
		<p class="pageinput">
			<?php echo $_smarty_tpl->tpl_vars['newregistration_mail_sendtouser_input']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['newregistration_mail_sendtouser_label']->value;?>
<br />
			<?php echo $_smarty_tpl->tpl_vars['newregistration_mail_sendtoother_input']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['newregistration_mail_sendtoother_label']->value;?>
:
			<?php echo $_smarty_tpl->tpl_vars['newregistration_mail_sendtoother_mails_input']->value;?>

		</p>
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['mail_newregistration_template_label']->value;?>
</p>
		<p class="pageinput">
			<?php echo $_smarty_tpl->tpl_vars['mail_newregistration_template_help']->value;?>

		</p>
		<p class="pagetext">
			<?php echo $_smarty_tpl->tpl_vars['mail_newregistration_subject_label']->value;?>
: <?php echo $_smarty_tpl->tpl_vars['mail_newregistration_subject_input']->value;?>
<br /><br />
		</p>
		<p class="pageinput">
			<?php echo $_smarty_tpl->tpl_vars['mail_newregistration_template_input']->value;?>

		</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['submit']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['cancel']->value;?>
</p>
	</div>
</fieldset>

<fieldset>
	<legend><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('mail_cancelregistration_title');?>
</legend>
	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('email_to_who_cancelregistration');?>
</p>
		<p class="pageinput">
			<?php echo $_smarty_tpl->tpl_vars['cancelregistration_mail_sendtouser_input']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['cancelregistration_mail_sendtouser_label']->value;?>
<br />
			<?php echo $_smarty_tpl->tpl_vars['cancelregistration_mail_sendtoother_input']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['cancelregistration_mail_sendtoother_label']->value;?>
:
			<?php echo $_smarty_tpl->tpl_vars['cancelregistration_mail_sendtoother_mails_input']->value;?>

		</p>
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['mail_cancelregistration_template_label']->value;?>
</p>
		<p class="pageinput">
			<?php echo $_smarty_tpl->tpl_vars['mail_cancelregistration_template_help']->value;?>
</p>
		<p class="pagetext">
			<?php echo $_smarty_tpl->tpl_vars['mail_cancelregistration_subject_label']->value;?>
: <?php echo $_smarty_tpl->tpl_vars['mail_cancelregistration_subject_input']->value;?>
<br /><br />
		</p>
		<p class="pageinput">
			<?php echo $_smarty_tpl->tpl_vars['mail_cancelregistration_template_input']->value;?>

		</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['submit']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['cancel']->value;?>
</p>
	</div>
</fieldset>

<?php echo $_smarty_tpl->tpl_vars['form_end']->value;?>
<?php }} ?>