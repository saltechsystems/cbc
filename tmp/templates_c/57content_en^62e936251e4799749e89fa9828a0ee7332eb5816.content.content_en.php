<?php /* Smarty version Smarty-3.1.14, created on 2013-10-24 11:50:57
         compiled from "content:content_en" */ ?>
<?php /*%%SmartyHeaderCode:22396544752694ff10578b2-30961873%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '62e936251e4799749e89fa9828a0ee7332eb5816' => 
    array (
      0 => 'content:content_en',
      1 => 1382122614,
      2 => 'content',
    ),
  ),
  'nocache_hash' => '22396544752694ff10578b2-30961873',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_52694ff106b382_18952991',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52694ff106b382_18952991')) {function content_52694ff106b382_18952991($_smarty_tpl) {?><!-- New Row -->
<div class="investigator_row"><!-- New Investigator-->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/investigators/slide0002_image002.png" alt="" /></div>
<div class="investigator_information"><strong class="name">Dr. Thomas Baum</strong>
<p class="title">Senior Investigator</p>
<p>Department of Plant Pathology and Microbiology</p>
<p><strong>Role:</strong> Trait discovery and validation</p>
<p><a href="http://www.plantpath.iastate.edu/people/baum">Read full bio here</a></p>
</div>
</div>
<!-- New Investigator -->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/investigators/slide0002_image004.png" alt="" /></div>
<div class="investigator_information"><strong class="name">Dr. William Beavis</strong>
<p class="title">Senior Investigator</p>
<p>Department of Agronomy</p>
<p><strong>Role:</strong> Modeling and optimization</p>
<p><a href="http://www.agron.iastate.edu/personnel/userspage.aspx?id=1542">Read full bio here</a></p>
</div>
</div>
</div>
<!-- End row --> <!-- New Row -->
<div class="investigator_row"><!-- New Investigator--> <!-- Profile picture -->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/investigators/slide0002_image006.png" alt="" /></div>
<div class="investigator_information"><!-- Name --> <strong class="name">Dr. Phil Becraft</strong> <!-- Title -->
<p class="title">Senior Investigator</p>
<!-- Department -->
<p>Department of Genetics, Development and Cell Biology</p>
<!-- Role -->
<p><strong>Role:</strong> Trait discovery and validation</p>
<!-- Full Bio Link -->
<p><a href="http://www.gdcb.iastate.edu/faculty_and_research/bios/pbecraft.shtml">Read full bio here</a></p>
</div>
</div>
<!-- New Investigator -->
<div class="investigator_profile"><!-- Profile picture -->
<div class="profile_image"><img src="uploads/images/investigators/slide0002_image008.png" alt="" /></div>
<div class="investigator_information"><!-- Name --> <strong class="name">Dr. Madan Bhattacharyya</strong> <!-- Title -->
<p class="title">Senior Investigator</p>
<!-- Department -->
<p>Department of Agronomy</p>
<!-- Role -->
<p><strong>Role:</strong> Trait discovery and validation</p>
<!-- Full Bio Link -->
<p><a href="http://www.agron.iastate.edu/personnel/userspage.aspx?id=824">Read full bio here</a></p>
</div>
</div>
</div>
<!-- End row --> <!-- New Row -->
<div class="investigator_row"><!-- New Investigator--> <!-- Profile picture -->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/investigators/slide0002_image012.png" alt="" /></div>
<div class="investigator_information"><!-- Name --> <strong class="name">Dr. Volker Brendel</strong> <!-- Title -->
<p class="title">Crop genome informaticsr</p>
<!-- Department -->
<p>Department of Biology, Indiana University</p>
<!-- Role -->
<p><strong>Role:</strong> Crop genome informatics</p>
<!-- Full Bio Link -->
<p><a href="http://www.bio.indiana.edu/faculty/directory/profile.php?person=vbrendel">Read full bio here</a></p>
</div>
</div>
<!-- New Investigator -->
<div class="investigator_profile"><!-- Profile picture -->
<div class="profile_image"><img src="uploads/images/investigators/slide0002_image010.png" alt="" /></div>
<div class="investigator_information"><!-- Name --> <strong class="name">Dr. Dermot Hayes</strong> <!-- Title -->
<p class="title">Senior Investigator</p>
<!-- Department -->
<p>Department of Economics</p>
<!-- Role -->
<p><strong>Role:</strong> Economics and policy issues</p>
<!-- Full Bio Link -->
<p><a href="http://www.econ.iastate.edu/people/faculty/hayes-dermot-j">Read full bio here</a></p>
</div>
</div>
</div>
<!-- End row --> <!--New Row -->
<div class="investigator_row"><!-- New Investigator--> <!-- Profile picture -->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/investigators/slide0003_image015.png" alt="" /></div>
<div class="investigator_information"><!-- Name --> <strong class="name">Dr. Stephen Howell</strong> <!-- Title -->
<p class="title">Senior Investigator</p>
<!-- Department -->
<p>Department of Genetics, Development, and Cell Biology</p>
<!-- Role -->
<p><strong>Role:</strong> Trait discovery and validation</p>
<!-- Full Bio Link -->
<p><a href="http://www.gdcb.iastate.edu/faculty_and_research/bios/showell.shtml">Read full bio here</a></p>
</div>
</div>
<!-- New Investigator -->
<div class="investigator_profile"><!-- Profile picture -->
<div class="profile_image"><img src="uploads/images/investigators/slide0003_image023.png" alt="" /></div>
<div class="investigator_information"><!-- Name --> <strong class="name">Dr. Steven Huber</strong> <!-- Title -->
<p class="title">Senior Investigator</p>
<!-- Department -->
<p>USDA-ARS and Department of Plant Biology and Crop Sciences, University of Illinois-Champaign/Urbana</p>
<!-- Role -->
<p><strong>Role:</strong> Trait discovery and validation</p>
<!-- Full Bio Link -->
<p><a href="http://www.life.illinois.edu/plantbio/People/Faculty/Huber.htm">Read full bio here</a></p>
</div>
</div>
</div>
<!-- End row --> <!--New Row -->
<div class="investigator_row"><!-- New Investigator--> <!-- Profile picture -->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/investigators/slide0003_image025.png" alt="" /></div>
<div class="investigator_information"><!-- Name --> <strong class="name">Dr. Istvan Ladunga</strong> <!-- Title -->
<p class="title">Senior Investigator</p>
<!-- Department -->
<p>Department of Statistics, University of Nebraska-Lincoln</p>
<!-- Role -->
<p><strong>Role:</strong> Crop genome informatics</p>
<!-- Full Bio Link -->
<p><a href="http://statistics.unl.edu/people/staff/faculty/ladunga.shtml">Read full bio here</a></p>
</div>
</div>
<!-- New Investigator -->
<div class="investigator_profile"><!-- Profile picture -->
<div class="profile_image"><img src="uploads/images/investigators/slide0003_image017.png" alt="" /></div>
<div class="investigator_information"><!-- Name --> <strong class="name">Dr. Kendall Lamkey</strong> <!-- Title -->
<p class="title">Senior Investigator</p>
<!-- Department -->
<p>Department of Agronomy</p>
<!-- Role -->
<p><strong>Role:</strong> Modeling and optimization</p>
<!-- Full Bio Link -->
<p><a href="http://www.agron.iastate.edu/personnel/userspage.aspx?id=130">Read full bio here</a></p>
</div>
</div>
</div>
<!-- End row --> <!--New Row -->
<div class="investigator_row"><!-- New Investigator--> <!-- Profile picture -->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/investigators/slide0003_image019.png" alt="" /></div>
<div class="investigator_information"><!-- Name --><strong class="name">Dr. Carolyn Lawrence</strong> <!-- Title -->
<p class="title">Senior Investigator</p>
<!-- Department -->
<p>USDA-ARS and Department of Genetics, Development, and Cell Biology</p>
<!-- Role -->
<p><strong>Role:</strong> Crop genome informatics. Leader, Bioinformatics Team</p>
<!-- Full Bio Link -->
<p><a href="http://www.gdcb.iastate.edu/faculty_and_research/bios/clawrence.shtml">Read full bio here</a></p>
</div>
</div>
<!-- New Investigator -->
<div class="investigator_profile"><!-- Profile picture -->
<div class="profile_image"><img src="uploads/images/investigators/slide0003_image021.png" alt="" /></div>
<div class="investigator_information"><!-- Name --><strong class="name">Dr. Thomas Lubberstedt</strong> <!-- Title -->
<p class="title">Senior Investigator</p>
<!-- Department -->
<p>Department of Agronomy</p>
<!-- Role -->
<p><strong>Role:</strong> Trait discovery, validation, and allele optimization</p>
<!-- Full Bio Link -->
<p><a href="http://www.agron.iastate.edu/personnel/userspage.aspx?id=1541">Read full bio here</a></p>
</div>
</div>
</div>
<!-- End row --> <!--New Row -->
<div class="investigator_row"><!-- New Investigator--> <!-- Profile picture -->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/investigators/slide0004_image035.png" alt="" /></div>
<div class="investigator_information"><!-- Name --><strong class="name">Dr. Maria Salas-Fernandez</strong> <!-- Title -->
<p class="title">Senior Investigator</p>
<!-- Department -->
<p>Department of Agronomy</p>
<!-- Role -->
<p><strong>Role:</strong> Trait discovery, validation, and integration</p>
<!-- Full Bio Link -->
<p><a href="http://www.agron.iastate.edu/personnel/userspage.aspx?id=1640">Read full bio here</a></p>
</div>
</div>
<!-- New Investigator -->
<div class="investigator_profile"><!-- Profile picture -->
<div class="profile_image"><img src="uploads/images/investigators/slide0004_image028.png" alt="" /></div>
<div class="investigator_information"><!-- Name --> <strong class="name">Dr. Patrick Schnable</strong> <!-- Title -->
<p class="title">Senior Investigator</p>
<!-- Department -->
<p>Department of Agronomy</p>
<!-- Role -->
<p><strong>Role:</strong> Crop genome informatics, sequencing technology and trait discovery</p>
<!-- Full Bio Link -->
<p><a href="http://schnablelab.plantgenomics.iastate.edu/personnel/schnable_patrick.php">Read full bio here</a></p>
</div>
</div>
</div>
<!-- End row --> <!--New Row -->
<div class="investigator_row"><!-- New Investigator--> <!-- Profile picture -->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/investigators/slide0004_image037.png" alt="" /></div>
<div class="investigator_information"><!-- Name --> <strong class="name">Dr. Martin Spalding</strong> <!-- Title -->
<p class="title">Principle Investigator</p>
<!-- Department -->
<p>Department of Genetics, Development and Cell Biology</p>
<!-- Role -->
<p><strong>Role:</strong> Genome editing technology and trait discovery. Chair, Advisory Council</p>
<!-- Full Bio Link -->
<p><a href="http://www.gdcb.iastate.edu/faculty_and_research/bios/mspalding.shtml">Read full bio here</a></p>
</div>
</div>
<!-- New Investigator -->
<div class="investigator_profile"><!-- Profile picture -->
<div class="profile_image"><img src="uploads/images/investigators/slide0004_image030.png" alt="" /></div>
<!-- Name -->
<div class="investigator_information"><strong class="name">Dr. Erik Vollbrecht</strong> <!-- Title -->
<p class="title">Senior Investigator</p>
<!-- Department -->
<p>Department of Genetics, Development and Cell Biology</p>
<!-- Role -->
<p><strong>Role:</strong> Trait discovery and validation. Co-leader, Gene/Trait Discovery Team</p>
<!-- Full Bio Link -->
<p><a href="http://www.gdcb.iastate.edu/faculty_and_research/bios/evollbrecht.shtml">Read full bio here</a></p>
</div>
</div>
</div>
<!-- End row --><!-- New Row -->
<div class="investigator_row"><!-- New Investigator-->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/investigators/slide0004_image039.png" alt="" /></div>
<div class="investigator_information"><strong class="name">Dr. Kan Wang<br /></strong>
<p class="title">Co-Principle Investigator</p>
<p>Department of Agronomy</p>
<p><strong>Role:</strong> Genetic transformation and genome editing technology development. Co-leader, Technology Team</p>
<p><a href="http://agron-www.agron.iastate.edu/ptf/employee/director.aspx">Read full bio here</a></p>
</div>
</div>
<!-- New Investigator -->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/investigators/slide0004_image033.png" alt="" /></div>
<div class="investigator_information"><strong class="name">Dr. Donald Weeks<br /></strong>
<p class="title">Senior Investigator</p>
<p>Department of Biochemistry, University of Nebraska-Lincoln</p>
<p><strong>Role:</strong> Genome editing technology and trait discovery</p>
<p><a href="http://www.unl.edu/psi/faculty_weeks.shtml">Read full bio here</a></p>
</div>
</div>
</div>
<!-- End row --> <!-- New Row -->
<div class="investigator_row"><!-- New Investigator-->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/investigators/slide0004_image032.jpg" alt="" /></div>
<div class="investigator_information"><strong class="name">Dr. Steve Whitham<br /></strong>
<p class="title">Senior Investigator</p>
<p>Department of Plant Pathology and Microbiology</p>
<p><strong>Role:</strong> Trait discovery and validation. Co-leader, Trait Discovery Team</p>
<p><a href="http://www.plantpath.iastate.edu/people/whitham">Read full bio here</a></p>
</div>
</div>
<!-- New Investigator -->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/investigators/slide0005_image046.png" alt="" /></div>
<div class="investigator_information"><strong class="name">Dr. Clark Wolf<br /></strong>
<p class="title">Senior Investigator</p>
<p>Department of Philosphy and Religious Studies</p>
<p><strong>Role:</strong> Societal impacts and bioethics</p>
<p><a href="http://www.las.iastate.edu/150/wolf.shtml">Read full bio here</a></p>
</div>
</div>
</div>
<!-- End row --> <!-- New Row -->
<div class="investigator_row"><!-- New Investigator-->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/investigators/slide0005_image048.png" alt="" /></div>
<div class="investigator_information"><strong class="name">Dr. Jeffery Wolt<br /></strong>
<p class="title">Senior Investigator</p>
<p>Department of Agronomy</p>
<p><strong>Role:</strong> Societal impacts and bioethics. Leader, Regulatory, Economic, Environmental and Societal Impacts Team</p>
<p><a href="http://www.ensci.iastate.edu/grad/faculty/wolt.html">Read full bio here</a></p>
</div>
</div>
<!-- New Investigator -->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/David_7489.jpg" alt="" /></div>
<div class="investigator_information"><strong class="name">Dr. David Wright<br /></strong>
<p class="title">Senior Investigator</p>
<p>Department of Genetics, Development, and Cell Biology</p>
<p><strong>Role:</strong> Trait Discovery and validation</p>
<p><a href="http://www.sdstate.edu/index/directory/directory-detail.cfm?view=detail&amp;ci=7489">Read full bio here</a></p>
</div>
</div>
</div>
<!-- End row --> <!-- New Row -->
<div class="investigator_row"><!-- New Investigator-->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/investigators/slide0005_image050.png" alt="" /></div>
<div class="investigator_information"><strong class="name">Dr. Bing Yang<br /></strong>
<p class="title">Co-Principle Investigator</p>
<p>Department of Genetics, Development and Cell Biology</p>
<p><strong>Role:</strong> Genome editing technology and trait discovery. Co-leader, Technology Team</p>
<p><a href="http://www.gdcb.iastate.edu/faculty_and_research/bios/byang.shtml">Read full bio here</a></p>
</div>
</div>
<!-- New Investigator -->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/investigators/slide0005_image042.png" alt="" /></div>
<div class="investigator_information"><strong class="name">Dr. Yanhai Yin<br /></strong>
<p class="title">Senior Investigator</p>
<p>Department of Genetics, Development and Cell Biology</p>
<p><strong>Role:</strong> Trait discovery and validation</p>
<p><a href="http://www.gdcb.iastate.edu/faculty_and_research/bios/yyin.shtml">Read full bio here</a></p>
</div>
</div>
</div>
<!-- End row --> <!-- New Row -->
<div class="investigator_row"><!-- New Investigator-->
<div class="investigator_profile">
<div class="profile_image"><img src="uploads/images/investigators/slide0005_image044.png" alt="" /></div>
<div class="investigator_information"><strong class="name">Dr. Jianming Yu<br /></strong>
<p class="title">Senior Investigator</p>
<p>Department of Agronomy</p>
<p><strong>Role:</strong> Trait discovery, validation and integration. Leader, Trait Verification and Integration Team</p>
<p><a href="http://www.agron.iastate.edu/personnel/userspage.aspx?id=1964">Read full bio here</a></p>
</div>
</div>
<!-- End row --></div><?php }} ?>