<?php /* Smarty version Smarty-3.1.14, created on 2013-10-24 12:44:20
         compiled from "module_db_tpl:EventsManager;summary_Table" */ ?>
<?php /*%%SmartyHeaderCode:675311963526945e0af38e9-52851296%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e3d999593e2435bcd591a3918806f57fbb3b3b16' => 
    array (
      0 => 'module_db_tpl:EventsManager;summary_Table',
      1 => 1382635555,
      2 => 'module_db_tpl',
    ),
  ),
  'nocache_hash' => '675311963526945e0af38e9-52851296',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_526945e0c509c4_36651960',
  'variables' => 
  array (
    'items' => 0,
    'entry' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_526945e0c509c4_36651960')) {function content_526945e0c509c4_36651960($_smarty_tpl) {?><?php if (!is_callable('smarty_cms_modifier_cms_date_format')) include '/var/www/html/plugins/modifier.cms_date_format.php';
if (!is_callable('smarty_function_math')) include '/var/www/html/lib/smarty/plugins/function.math.php';
?>


<?php echo FrontEndUsers::function_plugin(array('form'=>"logout"),$_smarty_tpl);?>

<?php echo cms_user_tag_FEULoginAndReturn(array(),$_smarty_tpl);?>


<?php if (count($_smarty_tpl->tpl_vars['items']->value)){?>
<table>
	<tr>
		<th>Name</th>
		<th>Category</th>
		<th>Start</th>
		<th>End</th>
		<th>Price</th>
		<th>Registered</th>
		<th>Remaining</th>
		<th></th>
	</tr>
	<?php  $_smarty_tpl->tpl_vars['entry'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['entry']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['entry']->key => $_smarty_tpl->tpl_vars['entry']->value){
$_smarty_tpl->tpl_vars['entry']->_loop = true;
?>
	
		
		<?php if (isset($_smarty_tpl->tpl_vars['overquotanb'])) {$_smarty_tpl->tpl_vars['overquotanb'] = clone $_smarty_tpl->tpl_vars['overquotanb'];
$_smarty_tpl->tpl_vars['overquotanb']->value = 0; $_smarty_tpl->tpl_vars['overquotanb']->nocache = null; $_smarty_tpl->tpl_vars['overquotanb']->scope = 0;
} else $_smarty_tpl->tpl_vars['overquotanb'] = new Smarty_variable(0, null, 0);?>
	<tr>
		<td><a href="<?php echo $_smarty_tpl->tpl_vars['entry']->value->detailurl;?>
"><?php echo $_smarty_tpl->tpl_vars['entry']->value->name;?>
</a></td>
		<?php if (isset($_smarty_tpl->tpl_vars['entry']->value->category)){?>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->category->name;?>
</td>
		<?php }else{ ?>
			<td></td>
		<?php }?>
		<td><?php echo smarty_cms_modifier_cms_date_format($_smarty_tpl->tpl_vars['entry']->value->start_datetime,"M d, Y @ g:ia");?>
</td>
		<td><?php echo smarty_cms_modifier_cms_date_format($_smarty_tpl->tpl_vars['entry']->value->end_datetime,"M d, Y @ g:ia");?>
</td>
		<?php if ($_smarty_tpl->tpl_vars['entry']->value->price>0){?>
			<td>$<?php echo number_format($_smarty_tpl->tpl_vars['entry']->value->price,2);?>
</td>
		<?php }else{ ?>
			<td>Free</td>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['entry']->value->allow_registration){?>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->nb_regusers;?>
</td>
			<?php if ($_smarty_tpl->tpl_vars['entry']->value->capacity>0){?>
				<td><?php echo smarty_function_math(array('equation'=>"x - y",'x'=>$_smarty_tpl->tpl_vars['entry']->value->capacity,'y'=>$_smarty_tpl->tpl_vars['entry']->value->nb_regusers),$_smarty_tpl);?>
</td>
			<?php }else{ ?>
				<td>Unlimited</td>
			<?php }?>

			<?php if (isset($_smarty_tpl->tpl_vars['entry']->value->registration_url)){?>
				<td><a href="<?php echo $_smarty_tpl->tpl_vars['entry']->value->registration_url;?>
">Register</a></td>
			<?php }?>
		<?php }?>
		</tr>
		
		</tr>
	<?php } ?>
	</table>
<?php }else{ ?>
	<p>No event</p>
<?php }?><?php }} ?>