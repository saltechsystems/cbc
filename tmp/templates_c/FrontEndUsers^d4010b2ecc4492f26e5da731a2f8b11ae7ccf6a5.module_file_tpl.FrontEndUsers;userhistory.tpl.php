<?php /* Smarty version Smarty-3.1.14, created on 2013-10-24 14:07:25
         compiled from "module_file_tpl:FrontEndUsers;userhistory.tpl" */ ?>
<?php /*%%SmartyHeaderCode:131674065652696feda97f19-75201167%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd4010b2ecc4492f26e5da731a2f8b11ae7ccf6a5' => 
    array (
      0 => 'module_file_tpl:FrontEndUsers;userhistory.tpl',
      1 => 1382022836,
      2 => 'module_file_tpl',
    ),
  ),
  'nocache_hash' => '131674065652696feda97f19-75201167',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'message' => 0,
    'error' => 0,
    'multiuser' => 0,
    'title_userhistory' => 0,
    'for' => 0,
    'user_username' => 0,
    'formstart' => 0,
    'title_legend_filter' => 0,
    'prompt_filter_eventtype' => 0,
    'input_filter_eventtype' => 0,
    'prompt_username_regex' => 0,
    'input_username_regex' => 0,
    'prompt_filter_date' => 0,
    'input_filter_date' => 0,
    'prompt_pagelimit' => 0,
    'input_pagelimit' => 0,
    'title_legend_groupsort' => 0,
    'prompt_group_ip' => 0,
    'input_group_ip' => 0,
    'prompt_sortorder' => 0,
    'input_sortorder' => 0,
    'submit' => 0,
    'reset' => 0,
    'formend' => 0,
    'recordcount' => 0,
    'prompt_recordsfound' => 0,
    'itemcount' => 0,
    'pagecount' => 0,
    'pagenumber' => 0,
    'firstpage' => 0,
    'prevpage' => 0,
    'oftext' => 0,
    'nextpage' => 0,
    'lastpage' => 0,
    'prompt_username' => 0,
    'prompt_ipaddress' => 0,
    'prompt_action' => 0,
    'prompt_refdate' => 0,
    'items' => 0,
    'entry' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_52696fedc83735_60210596',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52696fedc83735_60210596')) {function content_52696fedc83735_60210596($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/html/lib/smarty/plugins/modifier.date_format.php';
?><?php if (isset($_smarty_tpl->tpl_vars['message']->value)){?>
  <?php if (isset($_smarty_tpl->tpl_vars['error']->value)){?>
    <font color="red"><h4><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</h4></font>
  <?php }else{ ?>
    <h4><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</h4>
  <?php }?>
<?php }?>

<?php if (!isset($_smarty_tpl->tpl_vars['multiuser']->value)){?>
<h4><?php echo $_smarty_tpl->tpl_vars['title_userhistory']->value;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['for']->value;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['user_username']->value;?>
</h4>
<?php }?>
<?php echo $_smarty_tpl->tpl_vars['formstart']->value;?>

<fieldset>
<legend><?php echo $_smarty_tpl->tpl_vars['title_legend_filter']->value;?>
</legend>
  <div class="pageoverflow">
     <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_filter_eventtype']->value;?>
:</p>
     <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_filter_eventtype']->value;?>
</p>
  </div>
<?php if (isset($_smarty_tpl->tpl_vars['multiuser']->value)){?>
  <div class="pageoverflow">
     <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_username_regex']->value;?>
:</p>
     <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_username_regex']->value;?>
</p>
  </div>
<?php }?>
  <div class="pageoverflow">
     <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_filter_date']->value;?>
:</p>
     <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_filter_date']->value;?>
</p>
  </div>
  <div class="pageoverflow">
     <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_pagelimit']->value;?>
:</p>
     <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_pagelimit']->value;?>
</p>
  </div>
</fieldset>
<fieldset>
<legend><?php echo $_smarty_tpl->tpl_vars['title_legend_groupsort']->value;?>
</legend>
  <div class="pageoverflow">
     <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_group_ip']->value;?>
:</p>
     <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_group_ip']->value;?>
</p>
  </div>
  <div class="pageoverflow">
     <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_sortorder']->value;?>
:</p>
     <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_sortorder']->value;?>
</p>
  </div>
</fieldset>
  <div class="pageoverflow">
     <p class="pagetext">&nbsp;</p>
     <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['submit']->value;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['reset']->value;?>
</p>
  </div>
<?php echo $_smarty_tpl->tpl_vars['formend']->value;?>


<p><?php echo $_smarty_tpl->tpl_vars['recordcount']->value;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['prompt_recordsfound']->value;?>
</p>
<?php if ($_smarty_tpl->tpl_vars['itemcount']->value>0){?>
<?php if ($_smarty_tpl->tpl_vars['pagecount']->value>1){?>
  <p>
<?php if ($_smarty_tpl->tpl_vars['pagenumber']->value>1){?>
<?php echo $_smarty_tpl->tpl_vars['firstpage']->value;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['prevpage']->value;?>
&nbsp;
<?php }?>
<?php echo $_smarty_tpl->tpl_vars['pagenumber']->value;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['oftext']->value;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['pagecount']->value;?>

<?php if ($_smarty_tpl->tpl_vars['pagenumber']->value<$_smarty_tpl->tpl_vars['pagecount']->value){?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['nextpage']->value;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['lastpage']->value;?>

<?php }?>
</p>
<?php }?>
<br/>
<table cellspacing="0" class="pagetable">
  <thead>
    <tr>
<?php if (isset($_smarty_tpl->tpl_vars['multiuser']->value)){?>
	<th><?php echo $_smarty_tpl->tpl_vars['prompt_username']->value;?>
</th>
<?php }?>
	<th><?php echo $_smarty_tpl->tpl_vars['prompt_ipaddress']->value;?>
</th>
	<th><?php echo $_smarty_tpl->tpl_vars['prompt_action']->value;?>
</th>
	<th><?php echo $_smarty_tpl->tpl_vars['prompt_refdate']->value;?>
</th>
    </tr>
  </thead>
  <tbody>
<?php  $_smarty_tpl->tpl_vars['entry'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['entry']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['entry']->key => $_smarty_tpl->tpl_vars['entry']->value){
$_smarty_tpl->tpl_vars['entry']->_loop = true;
?>
  <tr class="<?php echo $_smarty_tpl->tpl_vars['entry']->value->rowclass;?>
">
<?php if (isset($_smarty_tpl->tpl_vars['multiuser']->value)){?>
        <td><?php echo $_smarty_tpl->tpl_vars['entry']->value->username;?>
</td>
<?php }?>
	<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->ipaddress;?>
</td>
	<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->action;?>
</td>
	<td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['entry']->value->refdate,"%b %e, %Y - %X");?>
</td>
  </tr>	 
<?php } ?>
  </tbody>
</table>
<?php }?>
<?php }} ?>