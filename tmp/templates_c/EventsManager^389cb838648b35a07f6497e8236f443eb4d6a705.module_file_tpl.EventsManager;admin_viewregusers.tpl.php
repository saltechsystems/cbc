<?php /* Smarty version Smarty-3.1.14, created on 2013-10-24 14:06:58
         compiled from "module_file_tpl:EventsManager;admin_viewregusers.tpl" */ ?>
<?php /*%%SmartyHeaderCode:146153202452696fd2b081c8-94722785%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '389cb838648b35a07f6497e8236f443eb4d6a705' => 
    array (
      0 => 'module_file_tpl:EventsManager;admin_viewregusers.tpl',
      1 => 1382022850,
      2 => 'module_file_tpl',
    ),
  ),
  'nocache_hash' => '146153202452696fd2b081c8-94722785',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'mod' => 0,
    'event' => 0,
    'items' => 0,
    'backlink' => 0,
    'addlink' => 0,
    'exportlink' => 0,
    'nmsexportlink' => 0,
    'feu_props_names' => 0,
    'feu_prop' => 0,
    'entry' => 0,
    'count_feu_props' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_52696fd2c5e440_99271921',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52696fd2c5e440_99271921')) {function content_52696fd2c5e440_99271921($_smarty_tpl) {?><?php if (!is_callable('smarty_cms_modifier_cms_date_format')) include '/var/www/html/plugins/modifier.cms_date_format.php';
?>
<h3><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('registrations_for');?>
: <?php echo $_smarty_tpl->tpl_vars['event']->value->name;?>
</h3>

<?php if (count($_smarty_tpl->tpl_vars['items']->value)>0){?>
<div class="pageoptions"><p class="pageoptions">
	<?php echo $_smarty_tpl->tpl_vars['backlink']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['addlink']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['exportlink']->value;?>

	<?php if (isset($_smarty_tpl->tpl_vars['nmsexportlink']->value)){?>
		<?php echo $_smarty_tpl->tpl_vars['nmsexportlink']->value;?>

	<?php }?>
</p></div>

<table cellspacing="0" class="pagetable">
	<thead>
		<tr>
			<th>ID</th>
			<th><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('regdate');?>
</th>
			<th><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('nb_persons');?>
</th>
			<th><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('feuname');?>
</th>
			<?php  $_smarty_tpl->tpl_vars['feu_prop'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['feu_prop']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['feu_props_names']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['feu_prop']->key => $_smarty_tpl->tpl_vars['feu_prop']->value){
$_smarty_tpl->tpl_vars['feu_prop']->_loop = true;
?>
				<th><?php echo $_smarty_tpl->tpl_vars['feu_prop']->value;?>
</th>
			<?php } ?>
			<th class="pageicon">&nbsp;</th>
			<th class="pageicon">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	
	<?php  $_smarty_tpl->tpl_vars['entry'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['entry']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['entry']->key => $_smarty_tpl->tpl_vars['entry']->value){
$_smarty_tpl->tpl_vars['entry']->_loop = true;
?>
		<tr class="<?php echo $_smarty_tpl->tpl_vars['entry']->value->rowclass;?>
">
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->id;?>
</td>
			<td><?php echo smarty_cms_modifier_cms_date_format($_smarty_tpl->tpl_vars['entry']->value->modify_datetime);?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->nb_persons;?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->username;?>
</td>
			<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['loop'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['count_feu_props']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['name'] = 'loop';
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['loop']['total']);
?>
				<td>
					<?php echo $_smarty_tpl->tpl_vars['entry']->value->feu_props[$_smarty_tpl->getVariable('smarty')->value['section']['loop']['index']];?>

				</td>
			<?php endfor; endif; ?>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->editlink;?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->deletelink;?>
</td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<?php }else{ ?>
	<p><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('no_registrations');?>
</p>
<?php }?>

<div class="pageoptions">
	<p class="pageoptions">
		<?php echo $_smarty_tpl->tpl_vars['backlink']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['addlink']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['exportlink']->value;?>
 
		<?php if (isset($_smarty_tpl->tpl_vars['nmsexportlink']->value)){?>
			<?php echo $_smarty_tpl->tpl_vars['nmsexportlink']->value;?>

		<?php }?>
	</p>
</div><?php }} ?>