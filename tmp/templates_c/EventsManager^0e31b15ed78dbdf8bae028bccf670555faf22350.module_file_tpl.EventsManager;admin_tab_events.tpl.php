<?php /* Smarty version Smarty-3.1.14, created on 2013-10-24 14:06:52
         compiled from "module_file_tpl:EventsManager;admin_tab_events.tpl" */ ?>
<?php /*%%SmartyHeaderCode:189677415552696fccee6bc4-74108576%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0e31b15ed78dbdf8bae028bccf670555faf22350' => 
    array (
      0 => 'module_file_tpl:EventsManager;admin_tab_events.tpl',
      1 => 1382022850,
      2 => 'module_file_tpl',
    ),
  ),
  'nocache_hash' => '189677415552696fccee6bc4-74108576',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'items' => 0,
    'addlink' => 0,
    'reg_addlink' => 0,
    'mod' => 0,
    'entry' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_52696fcd1eafa0_03829346',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52696fcd1eafa0_03829346')) {function content_52696fcd1eafa0_03829346($_smarty_tpl) {?>
<?php if (count($_smarty_tpl->tpl_vars['items']->value)>0){?>
<div class="pageoptions"><p class="pageoptions"><?php echo $_smarty_tpl->tpl_vars['addlink']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['reg_addlink']->value;?>
</p></div>

<table cellspacing="0" class="pagetable">
	<thead>
		<tr>
			<th>ID</th>
			<th><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('name');?>
</th>
			<th><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('published');?>
</th>
			<th>&nbsp;</th>
			<th><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('start_datetime');?>
</th>
			<th><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('end_datetime');?>
</th>
			<th><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('registration_allowed');?>
</th>
			<th><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('number_of_regusers');?>
 / <?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('capacity');?>
</th>
			<th><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('manage_regusers');?>
</th>
			<th><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('export_regusers');?>
</th>
			<th class="pageicon">&nbsp;</th>
			<th class="pageicon">&nbsp;</th>
			<th class="pageicon">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	
	<?php  $_smarty_tpl->tpl_vars['entry'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['entry']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['entry']->key => $_smarty_tpl->tpl_vars['entry']->value){
$_smarty_tpl->tpl_vars['entry']->_loop = true;
?>
		<tr class="<?php echo $_smarty_tpl->tpl_vars['entry']->value->rowclass;?>
">
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->id;?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->name_editlink;?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->togglestatuslink;?>
</td>
			<td>
				<?php if ($_smarty_tpl->tpl_vars['entry']->value->cancelled){?>
					<span style="color: red"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('cancelled');?>

					
				<?php }elseif($_smarty_tpl->tpl_vars['entry']->value->whenstatus=="upcoming"){?>
					<span style="color: orange"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('event_upcoming');?>

				<?php }elseif($_smarty_tpl->tpl_vars['entry']->value->whenstatus=="past"){?>
					<span style="color: blue"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('event_past');?>

				<?php }else{ ?>
					<span style="color: green"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('event_inprogress');?>

					
				
				<?php }?>
			</td>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->start_datetime;?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->end_datetime;?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->toggleregistationlink;?>
</td>
			<td style="color: <?php if (($_smarty_tpl->tpl_vars['entry']->value->get_regusers_number()==$_smarty_tpl->tpl_vars['entry']->value->capacity)&&($_smarty_tpl->tpl_vars['entry']->value->capacity!=0)){?>red<?php }else{ ?>green<?php }?>; font-weight: bold">
				<?php echo $_smarty_tpl->tpl_vars['entry']->value->get_regusers_number();?>
 / <?php if ($_smarty_tpl->tpl_vars['entry']->value->capacity==0){?><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('unlimited');?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['entry']->value->capacity;?>
<?php }?>
			</td>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->viewreguserslink;?>
</td>
			<td><?php if ($_smarty_tpl->tpl_vars['entry']->value->get_regusers_number()>0){?>
				<?php echo $_smarty_tpl->tpl_vars['entry']->value->exportreguserslink;?>

				<?php if (isset($_smarty_tpl->tpl_vars['entry']->value->exportreguserslink_nms)){?>
					&nbsp;<?php echo $_smarty_tpl->tpl_vars['entry']->value->exportreguserslink_nms;?>

				<?php }?>
			<?php }?></td>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->copylink;?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->editlink;?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['entry']->value->deletelink;?>
</td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<?php }?>

<div class="pageoptions"><p class="pageoptions"><?php echo $_smarty_tpl->tpl_vars['addlink']->value;?>
 <?php if (count($_smarty_tpl->tpl_vars['items']->value)>0){?><?php echo $_smarty_tpl->tpl_vars['reg_addlink']->value;?>
<?php }?></p></div><?php }} ?>