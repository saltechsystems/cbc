<?php /* Smarty version Smarty-3.1.14, created on 2013-10-23 23:01:36
         compiled from "content:content_en" */ ?>
<?php /*%%SmartyHeaderCode:113910259752689ba027e642-51029693%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '62e936251e4799749e89fa9828a0ee7332eb5816' => 
    array (
      0 => 'content:content_en',
      1 => 1382046490,
      2 => 'content',
    ),
  ),
  'nocache_hash' => '113910259752689ba027e642-51029693',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_52689ba0284007_40479426',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52689ba0284007_40479426')) {function content_52689ba0284007_40479426($_smarty_tpl) {?><div style="width: 65%; float: left;">
<h1>Our Mission</h1>
<p style="font: italic 20px times,sans-serif; color: #484844;">The ISU Crop Bioengineering Consortium (CBC) seeks to address the urgent, grand challenge to provide sufficient food, feed, biofuels and biorenewable chemicals for the world’s burgeoning population, through basic and applied research to enable the bioengineering of valuable traits in a variety of crops.</p>
<p><span>Together, members of the CBC will work together to deploy innovative, transformative genome engineering technologies that identify, validate, and rapidly but precisely integrate strategically important traits and underlying genes into key crop plants. Importantly, the novel technologies employed can produce bioengineered crops that contain no transgenes and thus may face less stringent regulation than classic genetically modified organisms (GMOs).</span></p>
</div>
<div style="width: 35%; float: right; margin-right: -27px;"><img src="uploads/images/CBClogo.png" alt="" width="280" height="280" /></div>
<br />
<h1 class="Default">About Us</h1>
<p class="Default">We face an urgent, grand challenge to provide sufficient food, feed, fiber, biofuels and biorenewable chemicals for the world’s burgeoning population. Yield increases have succeeded historically through a combination of improved management and intensive breeding, but we have begun to fall short of demand. Paradoxically, current crop varieties leverage only a fraction of the plausible genetic architectures available through natural and bioengineered alleles. Moreover, the discovery rate of potentially useful genes now clearly outstrips our crop testing capabilities. Therefore, technologies that complement traditional management and breeding but dramatically accelerate the production and testing of improved crops are in critical demand. Current transgenic crop improvement efforts face public and regulatory challenges, dramatically increasing both the time-to-market and the costs associated with obtaining approval.</p>
<p>The Crop Bioengineering Consortium (CBC) intends to address this grand challenge through development and utilization of novel crop engineering technologies. The CBC will emphasize various innovative and transformative genome engineering technology including TALEN (Transcription Activator-Like Effector Nuclease) and CRISPR (Clustered Regularly Interspaced Short Palindromic Repeats) technology to build a platform for the identification and validation of strategically important plant genes/traits and for the subsequent rapid and precise integration of promising traits into important crop plants. Importantly, the CBC will incorporate valuable traits into crops via novel technology that generates bioengineered crops containing no transgenes and thus minimizes regulatory hurdles for bioengineered germplasm.<!-- Add code here that should appear in the content block of all new pages --></p>
<p> </p>
<p style="text-align: center;"><img src="uploads/images/CBCplatformAbout.jpg" alt="" width="500" height="529" /></p><?php }} ?>