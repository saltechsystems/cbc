<?php /* Smarty version Smarty-3.1.14, created on 2013-10-24 14:06:53
         compiled from "module_file_tpl:EventsManager;admin_tab_preferences.tpl" */ ?>
<?php /*%%SmartyHeaderCode:103789250452696fcd5d26e3-64929184%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f11aec325d3a3e9f85f93275010abb6230846e14' => 
    array (
      0 => 'module_file_tpl:EventsManager;admin_tab_preferences.tpl',
      1 => 1382022850,
      2 => 'module_file_tpl',
    ),
  ),
  'nocache_hash' => '103789250452696fcd5d26e3-64929184',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'form_start' => 0,
    'mod' => 0,
    'label_menuname' => 0,
    'input_menuname' => 0,
    'label_showtabs' => 0,
    'input_showtabs' => 0,
    'label_urlprefix' => 0,
    'input_urlprefix' => 0,
    'label_registerurlprefix' => 0,
    'input_registerurlprefix' => 0,
    'label_dflt_detailpage' => 0,
    'input_dflt_detailpage' => 0,
    'label_dflt_registrationpage' => 0,
    'input_dflt_registrationpage' => 0,
    'label_overquota_behaviour' => 0,
    'input_overquota_behaviour' => 0,
    'input_displayed_feu_properties' => 0,
    'label_displayed_feu_properties' => 0,
    'label_exportencoding' => 0,
    'input_exportencoding' => 0,
    'input_export_cancelled' => 0,
    'label_export_cancelled' => 0,
    'label_dflt_feuloginpage' => 0,
    'input_dflt_feuloginpage' => 0,
    'submit' => 0,
    'cancel' => 0,
    'form_end' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_52696fcd6fc807_91108441',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52696fcd6fc807_91108441')) {function content_52696fcd6fc807_91108441($_smarty_tpl) {?><?php echo $_smarty_tpl->tpl_vars['form_start']->value;?>


<fieldset>
	<legend><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('generaloptions');?>
</legend>
	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['label_menuname']->value;?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_menuname']->value;?>
</p>

		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['label_showtabs']->value;?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_showtabs']->value;?>
</p>
	</div>
</fieldset>
<fieldset>
	<legend><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('urloptions');?>
</legend>
	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['label_urlprefix']->value;?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_urlprefix']->value;?>
</p>
	
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['label_registerurlprefix']->value;?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_registerurlprefix']->value;?>
</p>
	</div>
</fieldset>
<fieldset>
	<legend><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('eventdetailoptions');?>
</legend>
	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['label_dflt_detailpage']->value;?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_dflt_detailpage']->value;?>
</p>
	</div>
</fieldset>
<fieldset>
	<legend><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('eventregistrationoptions');?>
</legend>
	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['label_dflt_registrationpage']->value;?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_dflt_registrationpage']->value;?>
</p>

		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['label_overquota_behaviour']->value;?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_overquota_behaviour']->value;?>
</p>
	</div>
</fieldset>
<fieldset>
	<legend><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('reguserslistoptions');?>
</legend>
	<div class="pageoverflow">
	<?php if ($_smarty_tpl->tpl_vars['input_displayed_feu_properties']->value){?>
			<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['label_displayed_feu_properties']->value;?>
:</p>
			<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_displayed_feu_properties']->value;?>
</p>
	<?php }?>
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['label_exportencoding']->value;?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_exportencoding']->value;?>
</p>

		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('title_export_cancelled');?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_export_cancelled']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['label_export_cancelled']->value;?>
</p>
	</div>
</fieldset>
<fieldset>
	<legend><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('feuloginoptions');?>
</legend>
	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['label_dflt_feuloginpage']->value;?>
:</p>
		<p class="pageinput">
			<?php echo $_smarty_tpl->tpl_vars['input_dflt_feuloginpage']->value;?>
<br />
			<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('dflt_feuloginpage_help');?>

		</p>
	</div>
</fieldset>

<div class="pageoverflow">
	<p class="pagetext">&nbsp;</p>
	<p class="pageinput">
		<?php echo $_smarty_tpl->tpl_vars['submit']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['cancel']->value;?>

	</p>
</div>

<?php echo $_smarty_tpl->tpl_vars['form_end']->value;?>
<?php }} ?>