<?php /* Smarty version Smarty-3.1.14, created on 2013-10-24 14:07:25
         compiled from "module_file_tpl:FrontEndUsers;propertiesform.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16833088052696fed3ddb25-04112811%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e8f5caa73e087164930316fb48847689a5aa2503' => 
    array (
      0 => 'module_file_tpl:FrontEndUsers;propertiesform.tpl',
      1 => 1382022836,
      2 => 'module_file_tpl',
    ),
  ),
  'nocache_hash' => '16833088052696fed3ddb25-04112811',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'title' => 0,
    'message' => 0,
    'error' => 0,
    'propcount' => 0,
    'propsfound' => 0,
    'nametext' => 0,
    'prompttext' => 0,
    'typetext' => 0,
    'lengthtext' => 0,
    'mod' => 0,
    'props' => 0,
    'prope' => 0,
    'addlink' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_52696fed5691a4_64584786',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52696fed5691a4_64584786')) {function content_52696fed5691a4_64584786($_smarty_tpl) {?><?php echo $_smarty_tpl->tpl_vars['title']->value;?>

<?php if (isset($_smarty_tpl->tpl_vars['message']->value)){?>
  <?php if (isset($_smarty_tpl->tpl_vars['error']->value)){?>
    <p><font color="red"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</font></p>
  <?php }else{ ?>
    <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
  <?php }?>
<?php }?>
<div class="pageoverflow">
<p><?php echo $_smarty_tpl->tpl_vars['propcount']->value;?>
&nbsp;<?php echo $_smarty_tpl->tpl_vars['propsfound']->value;?>
</p>
<?php if ($_smarty_tpl->tpl_vars['propcount']->value>0){?>
<br/>
<table cellspacing="0" class="pagetable cms_sortable tablesorter">
	<thead>
		<tr>
			<th><?php echo $_smarty_tpl->tpl_vars['nametext']->value;?>
</th>
			<th><?php echo $_smarty_tpl->tpl_vars['prompttext']->value;?>
</th>
			<th><?php echo $_smarty_tpl->tpl_vars['typetext']->value;?>
</th>
			<th><?php echo $_smarty_tpl->tpl_vars['lengthtext']->value;?>
</th>
			<th><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('unique');?>
</th>
			<th class="pageicon {sorter: false}">&nbsp;</th>
			<th class="pageicon {sorter: false}">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php  $_smarty_tpl->tpl_vars['prope'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['prope']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['props']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['prope']->key => $_smarty_tpl->tpl_vars['prope']->value){
$_smarty_tpl->tpl_vars['prope']->_loop = true;
?>
		<tr class="<?php echo $_smarty_tpl->tpl_vars['prope']->value->rowclass;?>
">
			<td><?php echo $_smarty_tpl->tpl_vars['prope']->value->name;?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['prope']->value->prompt;?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['prope']->value->type;?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['prope']->value->length;?>
</td>
                        <td><?php if ($_smarty_tpl->tpl_vars['prope']->value->force_unique){?><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('yes');?>
<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('no');?>
<?php }?></td>
			<td><?php echo (($tmp = @$_smarty_tpl->tpl_vars['prope']->value->editlink)===null||$tmp==='' ? '' : $tmp);?>
</td>
			<td><?php if (isset($_smarty_tpl->tpl_vars['prope']->value->deletelink)){?><?php echo $_smarty_tpl->tpl_vars['prope']->value->deletelink;?>
<?php }?></td>
		</tr>
<?php } ?>
	</tbody>
</table>
<?php }?>
</div>
<br/>
<p><?php echo $_smarty_tpl->tpl_vars['addlink']->value;?>
</p>
<?php }} ?>