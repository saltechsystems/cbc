<?php /* Smarty version Smarty-3.1.14, created on 2013-10-24 14:11:01
         compiled from "module_file_tpl:SelfRegistration;prefs.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1057371567526970c54267c6-80373682%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2833757f100ea488c75ab29a21540251f50961f1' => 
    array (
      0 => 'module_file_tpl:SelfRegistration;prefs.tpl',
      1 => 1382136530,
      2 => 'module_file_tpl',
    ),
  ),
  'nocache_hash' => '1057371567526970c54267c6-80373682',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'message' => 0,
    'startform' => 0,
    'mod' => 0,
    'prompt_inline' => 0,
    'input_inline' => 0,
    'actionid' => 0,
    'selectpkgopts' => 0,
    'allowselectpkg' => 0,
    'prompt_email_confirmation' => 0,
    'input_email_confirmation' => 0,
    'prompt_force_email_twice' => 0,
    'input_force_email_twice' => 0,
    'prompt_login_afterverify' => 0,
    'input_login_afterverify' => 0,
    'prompt_skip_final_msg' => 0,
    'input_skip_final_msg' => 0,
    'prompt_redirect_afterregister' => 0,
    'input_redirect_afterregister' => 0,
    'prompt_redirect_afterverify' => 0,
    'input_redirect_afterverify' => 0,
    'input_allowpaidregistration' => 0,
    'cartitem_summary_tpl' => 0,
    'redirect_paidpkg' => 0,
    'input_reg_additionalgroups' => 0,
    'matchfield_options' => 0,
    'additionalgroups_matchfields' => 0,
    'prompt_notify' => 0,
    'input_notify' => 0,
    'prompt_confirmmail_to' => 0,
    'input_confirmmail_to' => 0,
    'prompt_enable_whitelist' => 0,
    'input_enable_whitelist' => 0,
    'input_noregister' => 0,
    'prompt_whitelist' => 0,
    'input_whitelist' => 0,
    'prompt_whitelist_trigger_message' => 0,
    'input_whitelist_trigger_message' => 0,
    'hidden' => 0,
    'submit' => 0,
    'prompt_numresetrecords' => 0,
    'data_numresetrecords' => 0,
    'input_remove1week' => 0,
    'input_remove1month' => 0,
    'input_remove1day' => 0,
    'input_remove1all' => 0,
    'input_removeall' => 0,
    'input_list1day' => 0,
    'endform' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_526970c5784085_10460777',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_526970c5784085_10460777')) {function content_526970c5784085_10460777($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include '/var/www/html/lib/smarty/plugins/function.html_options.php';
?><?php if (isset($_smarty_tpl->tpl_vars['message']->value)&&$_smarty_tpl->tpl_vars['message']->value!=''){?><p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
<?php echo $_smarty_tpl->tpl_vars['startform']->value;?>

<table width="100%">
  <tr>
    <td width="50%">
    <fieldset>
        <legend><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_general_settings');?>
</legend>
	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_inline']->value;?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_inline']->value;?>
</p>
	</div>

    </fieldset>

    <fieldset>
        <legend><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_registration_settings');?>
</legend>
	<div class="pageoverflow">
	  <p class="pagetext"><label for="allowselectpkg"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_allow_select_pkg');?>
</label>:</p>
	  <p class="pageinput">
            <select id="allowselectpkg" name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
allowselectpkg">
            <?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['selectpkgopts']->value,'selected'=>$_smarty_tpl->tpl_vars['allowselectpkg']->value),$_smarty_tpl);?>

            </select>
            <br/><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('info_allowselectpkg');?>
;
          </p>
	</div>


	<div class="pageoverflow">
  	  <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_email_confirmation']->value;?>
:</p>
	  <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_email_confirmation']->value;?>

            <br/>
            <?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('info_email_confirmation');?>

          </p>
	</div>

	<div class="pageoverflow">
  	  <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_force_email_twice']->value;?>
:</p>
	  <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_force_email_twice']->value;?>

            <br/><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('info_force_email_twice');?>

          </p>
	</div>

	<div class="pageoverflow">
  	  <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_login_afterverify']->value;?>
:</p>
	  <p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_login_afterverify']->value;?>

            <br/>
            <?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('info_login_afterverify');?>

          </p>
	</div>

	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_skip_final_msg']->value;?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_skip_final_msg']->value;?>

                  <br/><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('info_skip_final_msg');?>

                </p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_redirect_afterregister']->value;?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_redirect_afterregister']->value;?>
</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_redirect_afterverify']->value;?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_redirect_afterverify']->value;?>
</p>
	</div>
    </fieldset>

    <?php if (isset($_smarty_tpl->tpl_vars['input_allowpaidregistration']->value)){?>
    <fieldset>
        <legend><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('paid_registration');?>
:</legend>
        <div class="pageoverflow">
          <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_allow_paid_registration');?>
:</p>
          <p class="pageinput">
            <?php echo $_smarty_tpl->tpl_vars['input_allowpaidregistration']->value;?>

            <br/>
            <?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('info_allow_paid_registration');?>

          </p>
        </div>
         
        <div class="pageoverflow">
          <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_cartitem_summary_tpl');?>
:</p>
          <p class="pageinput">
            <input type="text" name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
cartitem_summary_tpl" size="80" value="<?php echo $_smarty_tpl->tpl_vars['cartitem_summary_tpl']->value;?>
"/>
            <br/>
            <?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('info_cartitem_summary_tpl');?>

          </p>
        </div>

	<div class="pageoverflow">
	  <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_redirect_paidpkg');?>
:</p>
	  <p class="pageinput">
            <input type="text" name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
redirect_paidpkg" size="80" maxlength="255" value="<?php echo $_smarty_tpl->tpl_vars['redirect_paidpkg']->value;?>
"/>
            <br/>
            <?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('info_redirect_paidpkg');?>

          </p>
	</div>

	<div class="pageoverflow">
	  <p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('setup_cart_events');?>
:</p>
	  <p class="pageinput">
            <input type="submit" name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
setup_cart_events" value="<?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('setup');?>
"/>
            <br/>
            <?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('info_setup_cart_events');?>

          </p>
	</div>
   
    </fieldset>
    <?php }?>

    <fieldset>
        <legend><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_additionalgroups_settings');?>
:</legend>
	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_reg_additionalgroups');?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_reg_additionalgroups']->value;?>
</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_additionalgroups_matchfields');?>
:</p>
		<p class="pageinput">
                <select name="<?php echo $_smarty_tpl->tpl_vars['actionid']->value;?>
input_additionalgroups_matchfields[]" multiple="multiple" size="5">
                <?php echo smarty_function_html_options(array('options'=>$_smarty_tpl->tpl_vars['matchfield_options']->value,'selected'=>$_smarty_tpl->tpl_vars['additionalgroups_matchfields']->value),$_smarty_tpl);?>

                </select>
                <br/>
                <?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('info_additionalgroups_matchfields');?>
</p>
	</div>
    </fieldset>

    <fieldset>
        <legend><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('notifications');?>
:</legend>
	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_notify']->value;?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_notify']->value;?>
</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_confirmmail_to']->value;?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_confirmmail_to']->value;?>
</p>
	</div>
    </fieldset>

    <fieldset>
        <legend><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_security_settings');?>
</legend>
	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_enable_whitelist']->value;?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_enable_whitelist']->value;?>
</p>
	</div>

	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['mod']->value->Lang('prompt_noregister');?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_noregister']->value;?>
</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_whitelist']->value;?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_whitelist']->value;?>
</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext"><?php echo $_smarty_tpl->tpl_vars['prompt_whitelist_trigger_message']->value;?>
:</p>
		<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_whitelist_trigger_message']->value;?>
</p>
	</div>
    </fieldset>
    <div class="pageoverflow">
	<p class="pagetext">&nbsp;</p>
	<p class="pageinput"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['hidden']->value)===null||$tmp==='' ? '' : $tmp);?>
<?php echo $_smarty_tpl->tpl_vars['submit']->value;?>
</p>
    </div>
   </td>
   
   <td width="50%" valign="top">
   <?php if (isset($_smarty_tpl->tpl_vars['prompt_numresetrecords']->value)){?>
      <div class="pageoverflow">
	<p class="pagetext"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['prompt_numresetrecords']->value)===null||$tmp==='' ? '' : $tmp);?>
</p>
	<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['data_numresetrecords']->value;?>
</p>
      </div>
   <?php }?>
   <?php if (isset($_smarty_tpl->tpl_vars['input_remove1week']->value)){?>
      <div class="pageoverflow">
	<p class="pagetext">&nbsp;</p>
	<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_remove1week']->value;?>
</p>
      </div>
   <?php }?>
   <?php if (isset($_smarty_tpl->tpl_vars['input_remove1month']->value)){?>
      <div class="pageoverflow">
	<p class="pagetext">&nbsp;</p>
	<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_remove1month']->value;?>
</p>
      </div>
   <?php }?>
   <?php if (isset($_smarty_tpl->tpl_vars['input_remove1day']->value)){?>
      <div class="pageoverflow">
	<p class="pagetext">&nbsp;</p>
	<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_remove1day']->value;?>
</p>
      </div>
   <?php }?>
   <?php if (isset($_smarty_tpl->tpl_vars['input_remove1all']->value)){?>
      <div class="pageoverflow">
	<p class="pagetext">&nbsp;</p>
	<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_removeall']->value;?>
</p>
      </div>
   <?php }?>
   <?php if (isset($_smarty_tpl->tpl_vars['input_list1day']->value)){?>
      <div class="pageoverflow">
	<p class="pagetext">&nbsp;</p>
	<p class="pageinput"><?php echo $_smarty_tpl->tpl_vars['input_list1day']->value;?>
</p>
      </div>
   <?php }?>
   </td>
  </tr>
</table>
<?php echo $_smarty_tpl->tpl_vars['endform']->value;?>

<?php }} ?>