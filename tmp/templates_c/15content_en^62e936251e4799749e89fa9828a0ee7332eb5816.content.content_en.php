<?php /* Smarty version Smarty-3.1.14, created on 2013-10-23 18:56:54
         compiled from "content:content_en" */ ?>
<?php /*%%SmartyHeaderCode:201497293452686246342324-25907290%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '62e936251e4799749e89fa9828a0ee7332eb5816' => 
    array (
      0 => 'content:content_en',
      1 => 1382112137,
      2 => 'content',
    ),
  ),
  'nocache_hash' => '201497293452686246342324-25907290',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_526862463534e5_58511305',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_526862463534e5_58511305')) {function content_526862463534e5_58511305($_smarty_tpl) {?><div id="header"><img style="margin-top: 24px; margin-left: -1%;" src="uploads/images/CBClogo.png" alt="" />
<div id="showtime_slideshow" style="width: 69%; height: 335px; float: right; position: relative;"><?php echo Showtime::function_plugin(array('show'=>'1'),$_smarty_tpl);?>
</div>
</div>
<div style="width: 66%; float: left;">
<div style="width: 1px; height: 565px; background-color: #cccccc; float: right; margin-left: 10px;"> </div>
<div>
<h1>Our Mission</h1>
<p style="font: italic 20px times,sans-serif; color: #484844;">The ISU Crop Bioengineering Consortium (CBC) seeks to address the urgent, grand challenge to provide sufficient food, feed, biofuels and biorenewable chemicals for the world’s burgeoning population, through basic and applied research to enable the bioengineering of valuable traits in a variety of crops.</p>
<p><span>Together, members of the CBC will work together to deploy innovative, transformative genome engineering technologies that identify, validate, and rapidly but precisely integrate strategically important traits and underlying genes into key crop plants. Importantly, the novel technologies employed can produce bioengineered crops that contain no transgenes and thus may face less stringent regulation than classic genetically modified organisms (GMOs).</span></p>
</div>
<h1>Activities and Events</h1>
<div style="width: 48%; float: left;">
<div style="float: left; width: 100%;"><img src="uploads/images/activity1image.jpg" alt="" width="195" height="105" />
<div style="float: right; width: 28%;">+ <a href="http://www.extension.iastate.edu/registration/events/Conferences/cbcworkshop/index.html" target="_blank">About The Workshop</a>
<p> </p>
<p>+ <a href="http://www.extension.iastate.edu/registration/events/Conferences/cbcworkshop/register.html" target="_blank">Registration</a></p>
</div>
</div>
<div style="float: left; margin-top: 5px;"><strong>Science and Opportunities in Using Site-Directed Mutagenesis for Plant and Animal Improvement:</strong> November 4 and 5, 2013. Through this workshop we seek to communicate a clearer understanding of the technological opportunities for both plant and animal improvement afforded by tools for site-directed mutagenesis as they interface with issues of regulation, technology access, and public acceptance. <a href="http://www.extension.iastate.edu/registration/events/Conferences/cbcworkshop/index.html" target="_blank">Read More</a></div>
</div>
<div style="width: 48%; float: right;">
<div style="float: left; width: 100%;"><img src="uploads/images/activity2image.jpg" alt="" width="175" height="105" />
<div style="float: right; width: 36%;">+ <a href="mailto:cropbioengineering@iastate.edu">IT Specialist</a><br />
<p> </p>
<p>+ <a href="mailto:cropbioengineering@iastate.edu">Project Coordinator</a></p>
</div>
</div>
<div style="float: left; width: 96%; margin-top: 5px;"><strong>We're hiring!:</strong> If you are interested in working with us as a project coordinator or IT specialist, please contact us via email at <a href="mailto:cropbioengineering@iastate.edu">cropbioengineering@iastate.edu</a>.</div>
</div>
</div>
<div style="float: left; width: 34%;">
<div style="padding-left: 10px;">
<h1>Scientists</h1>
<div style="margin-bottom: 5px; float: left;">
<div style="float: left;"><img src="uploads/images/smSpaldingImage.jpg" alt="" width="59" height="60" /></div>
<div style="float: right; width: 79%;"><span style="font-size: small;"><strong><em>Dr. Martin H. Spalding</em></strong></span>
<p><strong><span style="font-size: 11px;">Principle Investigator<br /></span></strong><span style="font-size: 11px;">Department of Genetics, Development, <br />and Cell Biology<br /><strong>Role:</strong> Genome editing technology and trait discovery. <a href="http://www.cropbioengineeringconsortium.org/leadership/pi-co-pi.html">Read More</a><br /></span></p>
</div>
</div>
<div style="margin-bottom: 5px; float: left;">
<div style="float: left;"><img src="uploads/images/smWangImage.jpg" alt="" width="59" height="60" /></div>
<div style="float: right; width: 79%;"><span style="font-size: small;"><strong><em>Dr. Kan Wang<br /></em></strong></span>
<p><strong><span style="font-size: 11px;">Co-Principle Investigator<br /></span></strong><span style="font-size: 11px;">Department of Agronomy<br /><strong>Role:</strong> Genetic transformation and genome editing technology development. <a href="http://www.cropbioengineeringconsortium.org/leadership/pi-co-pi.html">Read More</a><br /></span></p>
</div>
</div>
<div style="margin-bottom: 5px; float: left;">
<div style="float: left;"><img src="uploads/images/smYangImage.jpg" alt="" width="59" height="60" /></div>
<div style="float: right; width: 79%;"><span style="font-size: small;"><strong><em>Dr. Bing Yang<br /></em></strong></span>
<p><strong><span style="font-size: 11px;">Co-Principle Investigator<br /></span></strong><span style="font-size: 11px;">Department of Genetics, Development, <br />and Cell Biology<br /><strong>Role:</strong> Genetic editing technology and trait discovery. <a href="http://www.cropbioengineeringconsortium.org/leadership/pi-co-pi.html">Read More</a><br /></span></p>
</div>
</div>
</div>
</div>
<div style="float: left; width: 34%;">
<div style="padding-left: 10px;">
<h1>Latest News</h1>
<div style="margin-bottom: 5px; float: left;">
<div style="float: left; width: 59px; height: 60px; background-color: #949480; border: 1px solid #ad1414; text-align: center;"><strong><span style="color: #ffffff; font-size: 28pt; font-family: times new roman,times;">13</span></strong></div>
<div style="float: right; width: 78%;"><span style="text-decoration: underline;"><span style="font-size: small;"><strong>September, 2013<em><br /></em></strong></span></span>
<p><span style="font-size: 11px;">We had our first full-member meeting. Project planning and coordination is in full swing and research endeavors are already underway.</span></p>
</div>
</div>
<div style="margin-bottom: 5px; float: left;">
<div style="float: left; width: 59px; height: 60px; background-color: #949480; border: 1px solid #ad1414; text-align: center;"><strong><span style="color: #ffffff; font-size: 28pt; font-family: times new roman,times;">10</span></strong></div>
<div style="float: right; width: 78%;"><span style="text-decoration: underline; font-size: small;"><strong>June, 2013<em><br /></em></strong></span>
<p><span style="font-size: 11px;">Seven ISU <a href="http://www.news.iastate.edu/news/2013/06/06/researchawards"><span style="text-decoration: underline;">Presidential Initiative for Interdisciplinary Research</span></a> awards were announced!</span></p>
</div>
</div>
</div>
</div><?php }} ?>