<?php /* Smarty version Smarty-3.1.14, created on 2013-10-24 12:49:41
         compiled from "module_db_tpl:EventsManager;registration_Simple" */ ?>
<?php /*%%SmartyHeaderCode:105705857852686217b28db8-24094809%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '36ab759ba36f6b4c9a79a222962d5ac07dc4829f' => 
    array (
      0 => 'module_db_tpl:EventsManager;registration_Simple',
      1 => 1382635981,
      2 => 'module_db_tpl',
    ),
  ),
  'nocache_hash' => '105705857852686217b28db8-24094809',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_52686217d196d2_08470270',
  'variables' => 
  array (
    'errors' => 0,
    'error' => 0,
    'messages' => 0,
    'message' => 0,
    'event' => 0,
    'registration' => 0,
    'display_event_registration_form' => 0,
    'overquotanb' => 0,
    'overquotatotal' => 0,
    'form_start' => 0,
    'hidden' => 0,
    'nb_persons_label' => 0,
    'nb_persons_input' => 0,
    'submit' => 0,
    'cancel' => 0,
    'form_end' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52686217d196d2_08470270')) {function content_52686217d196d2_08470270($_smarty_tpl) {?><?php if (!is_callable('smarty_cms_modifier_cms_date_format')) include '/var/www/html/plugins/modifier.cms_date_format.php';
?>
<?php if (count($_smarty_tpl->tpl_vars['errors']->value)>0){?>
	<?php  $_smarty_tpl->tpl_vars['error'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['error']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['errors']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['error']->key => $_smarty_tpl->tpl_vars['error']->value){
$_smarty_tpl->tpl_vars['error']->_loop = true;
?>
		<p class='error_message'><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p>
	<?php } ?>
<?php }?>
<?php if (count($_smarty_tpl->tpl_vars['messages']->value)>0){?>
	<?php  $_smarty_tpl->tpl_vars['message'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['message']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['messages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['message']->key => $_smarty_tpl->tpl_vars['message']->value){
$_smarty_tpl->tpl_vars['message']->_loop = true;
?>
		<p class='message'><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
	<?php } ?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['event']->value->allow_registration){?>
	<?php if ($_smarty_tpl->tpl_vars['event']->value->limited_reg_period==1){?>
		<p>Registrations for this event runs from <?php echo smarty_cms_modifier_cms_date_format($_smarty_tpl->tpl_vars['event']->value->reg_start_datetime);?>
 to <?php echo smarty_cms_modifier_cms_date_format($_smarty_tpl->tpl_vars['event']->value->reg_end_datetime);?>
</p>
	<?php }?>

	<?php if ($_smarty_tpl->tpl_vars['registration']->value->status=='registered'){?>
		<p>You're registered for <?php echo $_smarty_tpl->tpl_vars['registration']->value->nb_persons;?>
 place(s)</p>
	<?php }elseif($_smarty_tpl->tpl_vars['registration']->value->status=='cancelled'){?>
		<p>You've cancelled your registration on <?php echo smarty_cms_modifier_cms_date_format($_smarty_tpl->tpl_vars['registration']->value->modify_datetime);?>
</p>
	<?php }?>

	<?php if ($_smarty_tpl->tpl_vars['display_event_registration_form']->value){?>
		<?php if (isset($_smarty_tpl->tpl_vars['overquotanb']->value)&&isset($_smarty_tpl->tpl_vars['overquotatotal']->value)){?>
			<div class="overquota_message">
				<p style="color: red">You cannot register <strong><?php echo $_smarty_tpl->tpl_vars['overquotanb']->value;?>
</strong> places, because this exceed the total number of available places. You can get <strong><?php echo $_smarty_tpl->tpl_vars['overquotatotal']->value;?>
</strong> places. Please modify:</p>
			</div>
		<?php }elseif(isset($_smarty_tpl->tpl_vars['overquotanb']->value)&&($_smarty_tpl->tpl_vars['overquotanb']->value>0)){?>
			<div class="overquota_message">
				<p style="color: red">You cannot register <strong><?php echo $_smarty_tpl->tpl_vars['overquotanb']->value;?>
</strong> places, because this event is limited to <strong><?php echo $_smarty_tpl->tpl_vars['event']->value->capacity_per_feu;?>
</strong> places per user. Please modify:</p>
			</div>
		<?php }?>
		<?php echo $_smarty_tpl->tpl_vars['form_start']->value;?>

			<?php echo $_smarty_tpl->tpl_vars['hidden']->value;?>


			
			
			

			<?php if ((($_smarty_tpl->tpl_vars['event']->value->capacity_per_feu>1)||($_smarty_tpl->tpl_vars['event']->value->capacity_per_feu==0))&&$_smarty_tpl->tpl_vars['registration']->value->nb_persons==0){?>
				<?php echo $_smarty_tpl->tpl_vars['nb_persons_label']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['nb_persons_input']->value;?>

			<?php }?>

			<?php if ($_smarty_tpl->tpl_vars['registration']->value->nb_persons==0){?>
				<?php echo $_smarty_tpl->tpl_vars['submit']->value;?>

			<?php }else{ ?>
				<?php echo $_smarty_tpl->tpl_vars['cancel']->value;?>

			<?php }?>

		<?php echo $_smarty_tpl->tpl_vars['form_end']->value;?>

	<?php }?>
<?php }?><?php }} ?>