{* login form template *}
{* this is a sample template, feel free to customize it *}
{if isset($alt_loginform)}
  <div id="altlogin">{$alt_loginform}</div><br/>
{/if}
<fielset id="loginform">
  <legend>{$FrontEndUsers->Lang('prompt_normallogin')}:</legend>
  {$startform}
  {if $error}{$error}<br>{/if}
  <p><label for="{$feuactionid}feu_input_username">{$prompt_username}:</label>&nbsp;{$input_username}<br/></p>
  <p><label for="{$feuactionid}feu_input_password">{$prompt_password}:</label>&nbsp;{$input_password}</p>
  {if isset($captcha)}<p>
    <label for="{$feuactionid}input_captcha">{$captcha_title}:</label>&nbsp;{$input_captcha}<br/>{$captcha}</p>
  {/if}
  {if isset($input_rememberme)}<p>{$input_rememberme}&nbsp;<label for="{$feuactionid}feu_rememberme">{$prompt_rememberme}</input></p>{/if}
  <p><input type="submit" name="{$feuactionid}submit" value="{$FrontEndUsers->Lang('login')}"/></p>
  <p><a href="{$url_forgot}" title="{$FrontEndUsers->Lang('info_forgotpw')}">{$FrontEndUsers->Lang('forgotpw')}</a>&nbsp;
  <a href="{$url_lostun}" title="{$FrontEndUsers->Lang('info_lostun')}">{$FrontEndUsers->Lang('lostusername')}</a></p>
  {$endform}
</fieldset>
