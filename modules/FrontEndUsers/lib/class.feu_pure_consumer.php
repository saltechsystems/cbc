<?php

class feu_pure_consumer implements feu_auth_consumer
{
  public function is_authenticated() {
    return FALSE;
  }

  final public function has_capability()
  {
    $flag = func_get_args();
    if( !is_array($flag) ) $flag = array($flag);
    foreach( $flag as $one ) {
      if( in_array($one,$this->get_capabilities()) ) return TRUE;
    }
    return FALSE;
  }

  public function get_capabilities()
  {
    return array();
  }

  public function get_login_display($id,$returnid,$params)
  {
    return;
  }

  public function get_logout_display($id,$returnid,$params)
  {
    return;
  }

  public function get_changesettings_display($id,$returnid,$params)
  {
    return;
  }

  public function get_user_info()
  {
    return;
  }

  public function get_connecting_property_name()
  {
    return;
  }

  public function get_unique_identifier()
  {
    return;
  }

  public function get_group_list($with_count = FALSE)
  {
    return;
  }

  public function get_group_membership($userid)
  {
    return;
  }

  public function get_default_groups()
  {
    return;
  }

  public function get_username($uid = null)
  {
    return;
  }

  public function get_username_prompt()
  {
    $feu = cms_utils::get_module('FrontEndUsers');
    return $feu->Lang('prompt_username');
  }

  public function validate_username($username,$check_email_addr = FALSE,$uid = -1)
  {
    return TRUE;
  }
} // end of class

?>