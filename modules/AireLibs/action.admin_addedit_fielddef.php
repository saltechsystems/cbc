<?php
if (!isset($gCms)) exit;

// One field def edition / creation

/* Prepare the display */
$errors = array();
$messages = array();

$FieldsManager = new AireFieldsManager();

// Add or edit ?
$formodule = (isset($params['formodule'])) ? $params['formodule'] : '';
$key1 = (isset($params['key1'])) ? $params['key1'] : '';
$key2 = (isset($params['key2'])) ? $params['key2'] : '';
$key3 = (isset($params['key3'])) ? $params['key3'] : '';

if (isset($params['id_item']))
{
	$mode = 'edit';
	$id_item=$params['id_item'];
	$item = $FieldsManager->get_field_def($id_item);

	$formodule = $item->module;
	$key1 = $item->key1;
	$key2 = $item->key2;
	$key3 = $item->key3;
}
else
{
	$mode = 'add';
	$id_item=false;

	if (isset($params['type']))
	{
		$thetype = 'AireFieldDef_'.$params['type'];
		$item = new $thetype();
	}
	else
	{
		// Default : text
		$item = new AireFieldDef_text();
	}
}

/******************************************************************************
// Submit */
$item_saved = false;
if (isset($params['submit']))
{
	$item->load_from_params($params);

	// Check if the name and alias are unique for the module and depending on the keys
	if (!empty($item->name) && !empty($item->alias))
	{
		if ($FieldsManager->is_unique_name_alias($params['name'], $params['alias'], $id_item, $formodule, $key1, $key2, $key3))
		{
			// Save the field and return to the fields list
			if ($item->addedit())
				$item_saved = true;
		}
		else
			$errors[] = $this->Lang('error_duplicate_alias_name');
	}
	else
		$errors[] = $this->Lang('error_noname_noalias');
}

if (isset($params['cancel']) || $item_saved)
{
	// Item saved or cancelled ! Now go back to the module
	$orig_module = cms_utils::get_module($formodule);
	$orig_module->Redirect($id, 'defaultadmin', '', array('active_tab'=>$params['returntotab']));
}

/******************************************************************************
// Assign to smarty */

/* Form general vars */
$smarty->assign('form_start', $this->CreateFormStart($id, 'admin_addedit_fielddef', $returnid));
$smarty->assign('submit', $this->CreateInputSubmit($id, 'submit', lang('submit')));
$smarty->assign('cancel', $this->CreateInputSubmit($id, 'cancel', lang('cancel')));

$params_hidden = array('formodule', 'key1', 'key2', 'key3');
$hidden = '';
foreach ($params_hidden as $oneparam)
	if ($$oneparam)
		$hidden .= $this->CreateInputHidden($id, $oneparam, $$oneparam);

if ($mode == 'edit')
	$hidden .= $this->CreateInputHidden($id, 'id_item', $item->id);
$hidden .= $this->CreateInputHidden($id, 'returntotab', $params['returntotab']);

$smarty->assign('hidden', $hidden);
$smarty->assign('form_end', $this->CreateFormEnd());

/* General field infos */
# Name
$smarty->assign('name_input', $this->CreateInputText($id, 'name', $item->name, '40'));
$smarty->assign('name_label', $this->CreateLabelForInput($id, 'type', $this->Lang('field_name')));

# Alias
$smarty->assign('alias_input', $this->CreateInputText($id, 'alias', $item->alias, '40'));
$smarty->assign('alias_label', $this->CreateLabelForInput($id, 'alias', $this->Lang('field_alias')));

# Prompt
$smarty->assign('prompt_input', $this->CreateInputText($id, 'prompt', $item->prompt, '60'));
$smarty->assign('prompt_label', $this->CreateLabelForInput($id, 'prompt', $this->Lang('field_prompt')));

# Public ?
$smarty->assign('public_input', $this->CreateInputCheckbox($id, 'public', '1', ($item->public) ? '1':''));
$smarty->assign('public_label', $this->CreateLabelForInput($id, 'public', $this->Lang('field_public')));

# Helptext wysiwyg
$smarty->assign('help_input', $this->CreateTextArea(1, $id, $item->helptext, 'helptext', '', '', '', '', '', 5));
$smarty->assign('help_label', $this->CreateLabelForInput($id, 'helptext', $this->Lang('field_help')));

/* Specific field infos */
// Field type ONLY FOR CREATION
if ($mode == 'add')
{
	$type_items = $FieldsManager->get_fields_types_dropdown();
	$smarty->assign('type_input', $this->CreateInputDropdown($id, 'type', $type_items, '', 'text'));
	$smarty->assign('type_label', $this->CreateLabelForInput($id, 'type', $this->Lang('field_type')));
}
else
{
	$smarty->assign('type_label', $this->Lang('fielddef_name_' . $item->type));
}

// Field form template
$first_field_template = $item->get_admin_form($this, $id);
$smarty->assign('field_form', $first_field_template);

// Page title
if ($mode == 'add')
	$smarty->assign('title', $this->Lang('add_field'));
else
	$smarty->assign('title', $this->Lang('modify_field'));

$smarty->assign('actionid', $id);
$smarty->assign('mod', $this);
$smarty->assign('mode', $mode);

// Errors
foreach ($errors as $error)
	echo $this->ShowErrors($error);

// Ajax form change - we need the link to be called
$ajax_link = explode('?', $this->CreateLink($id, 'admin_ajaxform_fielddef', '', '', array('disable_theme'=>'true', 'type'=>'replacetypeajax'), '', 1));
$smarty->assign('ajax_url', $ajax_link[0]);
$smarty->assign('ajax_data', $ajax_link[1]);

echo $this->ProcessTemplate('admin_addedit_fielddef.tpl');

?>