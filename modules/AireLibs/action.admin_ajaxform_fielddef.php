<?php
if (!isset($gCms)) exit;

// Returns the form for a specific fielddef type in ajax
if (isset($params['type']))
{
	$thetype = 'AireFieldDef_'.$params['type'];
	$item = new $thetype();

	echo $item->get_admin_form(&$this, $id);
}

?>