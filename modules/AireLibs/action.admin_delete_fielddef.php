<?php
if (!isset($gCms)) exit;

$errors = array();
$messages = array();

if (isset($params['id_item']))
{
	// Delete the fielddef and associated data
	$item = new AireFieldDef($params['id_item']);
	$orig_module = cms_utils::get_module($item->module);
	$item->delete();

	// Return to the module
	$orig_module->Redirect($id, 'defaultadmin', '', array('active_tab'=>$params['returntotab']));
}

?>