<?php
if (!isset($gCms)) exit;

// Drop tables
$db = cmsms()->GetDb();
$dict = NewDataDictionary($db);

$tables = array('aire_fielddefs', 'aire_fieldvals');

foreach ($tables as $table)
{
	$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_" . $table);
	$dict->ExecuteSQLArray($sqlarray);
}

// Preferences, permissions and templates
#$this->DeleteTemplate();
$this->RemovePreference();


?>