<?php
if (!isset($gCms)) exit;

// Database operations
$db = cmsms()->GetDb();

$dict = NewDataDictionary($db);
$taboptarray = array('mysql' => 'TYPE=MyISAM');

// table : module_aire_fielddefs **********************************
$flds = "
	id I KEY AUTO,
	module C(120),
	key1 C(255),
	key2 C(255),
	key3 C(255),
	type C(150),
	name C(255),
	alias C(255),
	prompt C(255),
	helptext X,
	public I,
	position I,
	attrs X,
	create_datetime " . CMS_ADODB_DT . ",
	modify_datetime " .CMS_ADODB_DT;
$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_aire_fielddefs", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);

// table : module_aire_fieldvals **********************************
$flds = "
	id I KEY AUTO,
	id_fielddef I,
	id_entry I,
	value X,
	create_datetime " . CMS_ADODB_DT . ",
	modify_datetime " .CMS_ADODB_DT;
$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_aire_fieldvals", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);

?>