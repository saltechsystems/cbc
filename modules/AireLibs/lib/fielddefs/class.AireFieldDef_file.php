<?php

class AireFieldDef_file extends AireFieldDef
{
	protected $field_infos = array(
		'max_weight'=>'4096',
		'extensions'=>'pdf,odt,ods,doc,xls,zip',
		'path'=>'module_files/item_{$entry_id}/{$file_name}.{$file_extension}'
	);

	// This type - in php 5.3, this could be deleted
	public function get_field_type() { return 'file'; }

	// Text specific admin form for fielddef creation
	public function get_admin_form_fields(&$mod, $actionid)
	{
		$fields = array();

		// Max weight
		$fields[0]['input'] = $mod->CreateInputText($actionid, 'max_weight', $this->max_weight);
		$fields[0]['label'] = $mod->CreateLabelForInput($actionid, 'max_weight', $mod->Lang('max_weight_kb'));

		// Extensions
		$fields[1]['input'] = $mod->CreateInputText($actionid, 'extensions', $this->extensions);
		$fields[1]['label'] = $mod->CreateLabelForInput($actionid, 'extensions', $mod->Lang('extensions'));

		// Path : where to store the file
		$config = cmsms()->GetConfig();
		$fields[2]['input'] = $config['root_url'] . '/uploads/' . $mod->CreateInputText($actionid, 'path', $this->path, 70);
		$fields[2]['label'] = $mod->CreateLabelForInput($actionid, 'path', $mod->Lang('path_to_file'));
		$fields[2]['help'] = $mod->Lang('help_path_to_image');
	
		return $fields;
	}

	// Renders the item edition form field
	public function get_edit_form(&$mod, $actionid, $value, $smarty)
	{
		$fields = array();
		
		$fields[0]['label'] = $mod->CreateLabelForInput($actionid, 'afv_'.$this->id.'_file', (!empty($this->prompt) ? $this->prompt : $this->name));
		$fields[0]['input'] = $mod->CreateInputFile($actionid, 'afv_'.$this->id.'_file') . $mod->Lang('authorized_extensions') . ' : <strong>' . $this->extensions . '</strong> - ' . $mod->Lang('max_weight') . ' : <strong>' . $this->max_weight . '</strong> ' . $mod->Lang('kb');
		$fields[0]['help'] = $this->helptext;

		$smarty->assign('fields', $fields);
		
		// Current file
		if (!empty($value))
		{
			$config = cmsms()->GetConfig();
			$smarty->assign('current_file_url', $config['uploads_url'] . '/'. $value);
			$file_name = pathinfo($value);
			$smarty->assign('current_file_name', $file_name['basename']);

			// Delete checkbox
			$smarty->assign('delete_checkbox_label', $mod->CreateLabelForInput($actionid, 'afv_'.$this->id.'_delete', $mod->Lang('delete_file')));
			$smarty->assign('delete_checkbox_input', $mod->CreateInputCheckbox($actionid, 'afv_'.$this->id.'_delete', 1,0, 'id="'.$actionid.'afv_'.$this->id.'_delete"'));

			// Choose another file text
			$smarty->assign('choose_other_file', $mod->Lang('choose_other_file'));
		}
		else
			$smarty->assign('current_file', false);
		
		return $mod->ProcessTemplate('fieldvals_form_file.tpl');
	}

	// Upload the file and move it
	public function validate_value($newvalue, $id_item, $current_value=false)
	{
		$thefile = $newvalue['file'];
		$config = cmsms()->GetConfig();

		// Delete the file when no new file and no current file ?
		if (isset($newvalue['delete']) && empty($thefile['tmp_name']) && !empty($current_value))
		{
			// Delete the file
			$file = $config['uploads_path'].'/'.$current_value;
			unlink($file);
			
			// Return an empty value cause there's no file anymore
			return '';
		}

		// if there is a file
		if (!empty($thefile['tmp_name']))
		{
			$filename = $thefile['name'];
			$infos = pathinfo($filename);
			$extension=$infos['extension'];
			$basename=$infos['filename'];

			// Clear the spaces and special chars
			$basename = str_replace(' ', '-', $basename);
			$basename = preg_replace("/[&'£* ]/",'-',$basename);

			// Verify the extension and the size
			$auth_ext = explode(',',$this->extensions);
			$size = filesize($thefile['tmp_name'])/1024;

			if ( (in_array($extension, $auth_ext)) && ($size <= $this->max_weight))
			{
				// Move the file
				$final_path = $this->path;
				if ($id_item)
					$final_path = str_replace('{$entry_id}', $id_item, $final_path);
				$final_path = str_replace('{$file_name}', $basename, $final_path);
				$final_path = str_replace('{$file_extension}', $extension, $final_path);

				$dest = cms_join_path($config['uploads_path'],$final_path);

				// Create the path if necessary
				$infos = pathinfo($dest);

				if (!is_dir($infos['dirname']))
					mkdir($infos['dirname'],0777, true);

				// Delete the old file if exists
				if (!empty($current_value))
				{
					// Delete the file
					$file = $config['uploads_path'].'/'.$current_value;
					unlink($file);
				}

				// Now move the file
				if (cms_move_uploaded_file($thefile['tmp_name'], $dest))
					return $final_path;
			}
		}
		return false;
	}

	// Get extra infos for the display in smarty
	// Here we add some paths, filesize, etc..
	public function add_extra_data($fieldval_obj, &$smarty_obj)
	{
		$config = cmsms()->GetConfig();
		$extra_infos = array();
		$filepath = cms_join_path($config['uploads_path'], $fieldval_obj->value);

		// Urls
		$smarty_obj->file_url = $config['uploads_url'] . '/' . $fieldval_obj->value;

		// Paths
		$file_infos = pathinfo($filepath);
		$smarty_obj->file_name = $file_infos['basename'];
		if (isset($file_infos['extensions']))
			$smarty_obj->file_extension = $file_infos['extension'];
		$smarty_obj->file_name_without_ext = $file_infos['filename'];
		$smarty_obj->file_complete_path = $filepath;
		$smarty_obj->file_complete_dir = $file_infos['dirname'] . '/';

		// Size
		$smarty_obj->file_size_kb = filesize($filepath)/1024;
		$smarty_obj->file_size_mb = $smarty_obj->file_size_kb/1024;

		return true;
	}

	// We have to delete the file, if there is one
	// TODO : delete the directory / directories
	public function delete_extra_operations(&$fieldval_obj)
	{
		if (!empty($fieldval_obj->value))
		{
			// Delete the file
			$config = cmsms()->GetConfig();
			$file = $config['uploads_path'].'/'.$fieldval_obj->value;

			return unlink($file);
		}
		return true;
	}
}

?>