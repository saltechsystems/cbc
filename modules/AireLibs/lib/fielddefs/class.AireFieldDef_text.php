<?php

class AireFieldDef_text extends AireFieldDef
{
	protected $field_infos = array(
		'size'=>'40',
		'max_length'=>'255'
	);

	// This type - in php 5.3, this could be deleted
	public function get_field_type() { return 'text'; }

	// Text specific admin form for fielddef creation
	public function get_admin_form_fields(&$mod, $actionid)
	{
		$fields = array();

		// Size
		$fields[0]['input'] = $mod->CreateInputText($actionid, 'size', $this->size);
		$fields[0]['label'] = $mod->CreateLabelForInput($actionid, 'size', $mod->Lang('size'));

		// Max length
		$fields[1]['input'] = $mod->CreateInputText($actionid, 'max_length', $this->max_length);
		$fields[1]['label'] = $mod->CreateLabelForInput($actionid, 'max_length', $mod->Lang('max_length'));
	
		return $fields;
	}

	// Renders the item edition form field
	public function get_edit_form(&$mod, $actionid, $value, $smarty)
	{
		$fields = array();
		$fields[0]['label'] = $mod->CreateLabelForInput($actionid, 'afv_'.$this->id, (!empty($this->prompt) ? $this->prompt : $this->name));
		$fields[0]['input'] = $mod->CreateInputText($actionid, 'afv_'.$this->id, $value, $this->size, $this->max_length);
		$fields[0]['help'] = $this->helptext;

		$smarty->assign('fields', $fields);
		return $mod->ProcessTemplate('fieldvals_form_default.tpl');
	}
}

?>