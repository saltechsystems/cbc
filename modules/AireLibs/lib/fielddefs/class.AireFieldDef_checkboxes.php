<?php

// Note : the values are stored in the db like a text. Each checked value is stored with a separator
class AireFieldDef_checkboxes extends AireFieldDef
{
	protected $field_infos = array(
		'options'=>''
		
	);

	// Separator for the fieldval values
	private $sep='||';

	// This type - in php 5.3, this could be deleted
	public function get_field_type() { return 'checkboxes'; }

	// Text specific admin form for fielddef creation
	public function get_admin_form_fields(&$mod, $actionid)
	{
		$fields = array();

		// Options
		$fields[0]['input'] = $mod->CreateTextarea(0,$actionid, $this->options, 'options', '', '', '', '', 80, 7);
		$fields[0]['label'] = $mod->CreateLabelForInput($actionid, 'options', $mod->Lang('options_checkboxes'));
		$fields[0]['help'] = $mod->Lang('help_fielddef_checkboxes');
	
		return $fields;
	}

	// Renders the item edition form field
	public function get_edit_form(&$mod, $actionid, $value, $smarty)
	{
		// Get the total options
		$options = $this->options_to_array();
		// Get the checkboxes checked
		$values = $this->values_to_array($value);

		$fields = array();
		$count=0;
		foreach ($options as $onevalue=>$label)
		{
			$fields[$count]['label'] = $mod->CreateLabelForInput($actionid, 'afv_'.$this->id.'['.$onevalue.']', $label);
			$fields[$count]['input'] = $mod->CreateInputCheckbox($actionid, 'afv_'.$this->id.'['.$onevalue.']', 1, ((in_array($onevalue, $values)) ? 1 : 0),'id="'.$actionid.'afv_'.$this->id.'['.$onevalue.']"');

			// We add an hidden input because when the checkbox is not checked, nothing is returned, and the database doesn't update the value
			$fields[$count]['hidden'] = $mod->CreateInputHidden($actionid, 'afv_'.$this->id.'['.$onevalue.']', "0");

			$count++;
		}

		$smarty->assign('fields', $fields);
		$smarty->assign('checkboxes_label', (!empty($this->prompt) ? $this->prompt : $this->name));
		$smarty->assign('checkboxes_help', $this->helptext);
		
		return $mod->ProcessTemplate('fieldvals_form_checkboxes.tpl');
	}

	// Convert the options text field to an array - Returns the array
	public function options_to_array()
	{
		$res = array();
		$options = str_replace("\n\r", "\n", $this->options);
		$options = str_replace("\r", "\n", $options);
		$options = explode("\n", $options);

		foreach($options as $oneopt)
		{
			if (!empty($oneopt))
			{
				$tmp = explode('=',$oneopt);
				$res[$tmp[1]] = $tmp[0];
			}
		}
		return $res;
		
	}

	// Returns an array with the values checked
	public function values_to_array($value)
	{
		return explode($this->sep, $value);
	}

	// Validate the value - we return a string with the values separated by the separator
	public function validate_value($newvalue, $id_item, $current_value=false)
	{
		$res = array();
		foreach ($newvalue as $key=>$val)
		{
			if ($val == 1)
				$res[] = $key;
		}
		
		return implode($this->sep, $res);
	}

	// Get extra infos
	public function add_extra_data($fieldval_obj, &$smarty_obj)
	{
		// Change the default value with an array
		$values_smarty = array();
		$values=$this->values_to_array($fieldval_obj->value);
		$options = $this->options_to_array();

		foreach ($values as $oneval)
			$values_smarty[$oneval] = $options[$oneval];

		$smarty_obj->value = $values_smarty;
		
		return true;
	}
}

?>