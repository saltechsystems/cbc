<?php

class AireFieldDef_textarea extends AireFieldDef
{
	protected $field_infos = array(
		'wysiwyg'=>0
	);

	// This type - in php 5.3, this could be deleted
	public function get_field_type() { return 'textarea'; }

	public function get_admin_form_fields(&$mod, $actionid)
	{
		$fields = array();

		// Wysiwyg
		$values = array($mod->Lang('yes')=>1, $mod->Lang('no')=>0);
		$fields[0]['input'] = $mod->CreateInputDropdown($actionid, 'wysiwyg', $values, '', $this->wysiwyg);
		$fields[0]['label'] = $mod->CreateLabelForInput($actionid, 'wysiwyg', $mod->Lang('use_wysiwyg'));

		return $fields;
	}

	// Renders the item edition form field
	public function get_edit_form(&$mod, $actionid, $value, $smarty)
	{
		$fields = array();
		$fields[0]['label'] = $mod->CreateLabelForInput($actionid, 'afv_'.$this->id, (!empty($this->prompt) ? $this->prompt : $this->name));
		$fields[0]['input'] = $mod->CreateTextArea($this->wysiwyg, $actionid, $value, 'afv_'.$this->id);
		$fields[0]['help'] = $this->helptext;

		$smarty->assign('fields', $fields);
		return $mod->ProcessTemplate('fieldvals_form_textarea.tpl');
	}
}

?>