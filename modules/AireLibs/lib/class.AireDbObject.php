<?php

class AireDbObject extends AireObject
{
	protected $table = '';
	public $id = false;

	// Default fields for all the object types
	// key is the var name
	// value is the var default value
	protected $fields = array(
		'create_datetime'=>'',
		'modify_datetime'=>'');

	// An array that objects can complete to define custom fields
	protected $custom_fields = array();

	// Constructor
	// id : id of the item
	// load_fields : if false, only load the id and not the other fields values (useful just to delete an item)
	public function __construct($id=false, $load_fields=true)
	{
		// Merge fields and custom fields
		foreach ($this->custom_fields as $fieldname=>$fieldval)
			$this->fields[$fieldname] = $fieldval;

		if ($id!=false)
		{
			$this->id = $id;

			if ($load_fields)
			{
				$db = cmsms()->GetDb();
				$table = $this->get_table_name();
				$req = "SELECT * FROM ". $table ." WHERE id=?";
				$res = $db->GetRow($req, array($id));
				if ($res)
				{
					$this->id = $id;

					// Insert the field values in the object
					foreach ($this->fields as $fieldname=>$fieldvar)
						$this->$fieldname = $res[$fieldname];
				}
			}
		}
		else
		{
			// Default values
			foreach ($this->fields as $fieldname=>$fieldvar)
				$this->$fieldname = $fieldvar;
		}
	}

	// Add or edit an item
	// 'onlyfield' : give a field name, and only that field will be updated in edit mode (useful to reduce db queries - Should only be used when updating an item - The ID has to be defined in order to the update script to work
	public function addedit($onlyfieldname=false)
	{
		$db = cmsms()->GetDb();
		$this->modify_datetime = AireTools::GetDateTime();

		if ($this->id == false)
		{
			$mode = 'add';
			$query = 'INSERT INTO';
			$this->create_datetime = AireTools::GetDateTime();
		}
		else
		{
			$mode = 'edit';
			$query = 'UPDATE';
		}

		$query .= " " .cms_db_prefix()."module_".$this->table." SET ";

		// We test if we have to manage all the fieds, or only some of them
		if ($onlyfieldname == false OR ($onlyfieldname AND $mode=='add'))
		{
			// We take the values of the fields array to create the query
			// We use a second array and imlode to correctly handle the comma between the values
			foreach ($this->fields as $fieldname=>$fieldval)
			{
				// In edit mode, do not update the create_datetime
				if ($mode == 'add' OR ($fieldname != 'create_datetime'))
				{
					$fields_for_query[] = "`" . $fieldname . "`=?";
					$qparams[] = $this->$fieldname;
				}
			}
		}
		else
		{
			// Only update one field
			$fields_for_query[] = "`" . $onlyfieldname . "`=?";
			$qparams[] = $this->$onlyfieldname;
		}

		$query .= implode(', ', $fields_for_query);

		if ($mode == 'edit')
		{
			$query .= " WHERE id=?";
			$qparams[] = $this->id;
		}
		$res = $db->Execute($query, $qparams);

		//echo "QUERY: ".$db->sql."<br/>";
		//echo "ERROR: ".$db->ErrorMsg()."<br/>";
		
		if ($res)
		{
			// Set the id
			if ($mode == 'add')
				$this->id = $db->Insert_Id();

			return true;
		}

		return false;
	}

	// Delete a category in db
	public function delete()
	{
		if ($this->id)
		{
			$db = cmsms()->GetDb();
			$query = "DELETE FROM ".cms_db_prefix()."module_" . $this->table . " WHERE id=?";
			$qparams = array($this->id);

			return ($db->Execute($query, $qparams));
		}
		// Return true cause we have nothink to do when there's no id
		return true;
	}

	// Loads an empty object with array values (useful to prevent a lot of DB queries while retrieving a list of items
	public function load_from_array(array $data)
	{
		if (array_key_exists('id', $data))
			$this->id=$data['id'];

		foreach ($data as $key=>$val)
		{
			// Check if the current line is a valid field for this object
			if (array_key_exists($key, $this->fields))
				$this->$key = $val;
		}
	}

	// Returns a stdClass object for smarty
	// Useful to get a "clean" object without internal vars unused in smarty
	public function get_smarty_object()
	{
		$obj = new stdClass();
		$obj->id = $this->id;

		// Load the fields
		foreach ($this->fields as $fieldname=>$val)
		{
			$obj->$fieldname = $this->$fieldname;
		}

		return $obj;
	}

	/* Simply return the complete table name in DB */
	public function get_table_name()
	{
		$res = cms_db_prefix() . "module_" . $this->table;
		return $res;
	}
}

?>