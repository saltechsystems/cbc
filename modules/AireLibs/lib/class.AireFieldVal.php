<?php

/* Initial class for a custom Field value */
class AireFieldVal extends AireDbObject
{
	protected $table = 'aire_fieldvals';
	protected $custom_fields = array(
		'id_fielddef'=>0,
		'id_entry'=>0,
		'value'=>''
	);

	// the future FieldDef object
	public $fielddef = null;

	// Constructor with the ability to create a new object based on the two ids
	public function __construct($id=false, $load_fields=true, $id_fielddef=false, $id_entry=false)
	{
		if (!$id && ($id_fielddef && $id_entry))
		{
			// Search if there is an entry with the ids
			$query = "SELECT id FROM ".cms_db_prefix()."module_".$this->table." WHERE id_fielddef=? AND id_entry=?";
			$qparams = array($id_fielddef, $id_entry);
			$id_obj = cmsms()->GetDb()->GetOne($query, $qparams);

			if ($id_obj)
				$id = $id_obj;
			else
			{
				// values if the ids
				$this->custom_fields['id_fielddef'] = $id_fielddef;
				$this->custom_fields['id_entry'] = $id_entry;
			}
		}

		parent::__construct($id, $load_fields);
	}

	// function to render the edit form for that fieldval
	public function render_edit_form($actionid)
	{
		if ($this->fielddef)
		{
			$field_form = $this->fielddef->render_edit_form($actionid, $this->value);

			return $field_form;
		}
		else
			return false;
	}

	// Load the fieldef
	public function load_fielddef()
	{
		if ((!$this->fielddef) && !empty($this->id_fielddef))
		{
			$FieldsManager = new AireFieldsManager();
			$this->fielddef = $FieldsManager->get_field_def($this->id_fielddef);

			return true;
		}
		elseif ($this->fielddef)
			return true;
		else
			return false;
	}

	// Get a value and test it before inclusion
	public function process_new_value ($newvalue, $id_item)
	{
		$this->load_fielddef();
		$value = $this->fielddef->validate_value($newvalue, $id_item, $this->value);

		if ($value !== false)
		{
			$this->value = $value;
			return true;
		}
		return false;
	}

	// Returns the data value obj for smarty
	public function get_data_smarty()
	{
		$this->load_fielddef();

		$smarty_obj = new stdClass();
		$smarty_obj->value = $this->value;
		$smarty_obj->type = $this->fielddef->type;
		$smarty_obj->name = $this->fielddef->name;

		// Pass by reference
		$this->fielddef->add_extra_data($this, $smarty_obj);

		return $smarty_obj;
	}

	/* Delete - have to call the specific delete function for each fielddef (ex : files to delete ? */
	public function delete()
	{
		// We have to make the test because it's possible that the current object has no id and no database entry
		// If no info, we simply execute the main delete operation (which tests itself with the id info)
		if ($this->load_fielddef())
		{
			if ($this->fielddef->delete_extra_operations($this))
				return parent::delete();
			else
				return false;
		}
		return parent::delete();
	}
}

?>