<?php

/* Fields manager with useful fields functions */
class AireFieldsManager
{
	protected $table;
	
	public function __construct()
	{
		$this->table_defs = cms_db_prefix().'module_aire_fielddefs';
		$this->table_vals = cms_db_prefix().'module_aire_fieldvals';
	}

	public function get_main_module()
	{
		return cms_utils::get_module('AireLibs');
	}
	
	// Get all the field defs corresponding to parameters
	// module : name of the module to which the field have been assigned - optional
	// key1 : first key - optional
	// key2 : second key - optional
	// key3 : third key - optional
	// sort : sort order
	public function get_field_defs($module=false, $key1=false, $key2=false, $key3=false, $sort='position')
	{
		$query = 'SELECT * FROM '.$this->table_defs;
		$qparams = array();
		$where_used=0;
		
		if (!empty($module))
		{
			$query .= ' WHERE module=?';
			$qparams[] = $module;
			$where_used=1;
		}

		if ($key1)
		{
			$query .= ($where_used) ? " AND " : " WHERE ";
			$query .= 'key1=?';
			$qparams[] = $key1;
			$where_used=1;
		}

		if ($key2)
		{
			$query .= ($where_used) ? " AND " : " WHERE ";
			$query .= 'key2=?';
			$qparams[] = $key2;
			$where_used=1;
		}

		if ($key3)
		{
			$query .= ($where_used) ? " AND " : " WHERE ";
			$query .= 'key3=?';
			$qparams[] = $key3;
			$where_used=1;
		}

		$query . ' ORDER BY "' . $sort . '" ASC';

		$db = cmsms()->GetDb();
		$res = $db->GetArray($query, $qparams);

		if ($res)
		{
			// We have some fields
			// We load them in a fields objects array
			$fields = array();
			foreach ($res as $onefield)
			{
				$field = $this->get_field_def($onefield['id'], $onefield);
				$fields[] = $field;
			}

			return $fields;
		}
		
		return false;
	}

	// Returns a specific Field def object
	// This function has to return a complete and field specific object in a minimum number of db queries
	// id : id of the field - REQUIRED
	// attrs : an array of attributes (one line in db), useful after db queries to not create a new query
	public function get_field_def ($id=false, $attrs=false)
	{
		if ($id)
		{
			if (!$attrs['type'])
			{
				// We don't have the type, so lets get it from db
				$query = 'SELECT * FROM '.$this->table_defs.' WHERE id=?';
				$qparams[] = $id;

				$db = cmsms()->GetDb();
				$attrs = $db->GetRow($query, $qparams);
			}

			// Ok, we can create the right field object
			$class = 'AireFieldDef_' . $attrs['type'];
			$obj = new $class();
			$obj->load_from_array($attrs);

			return $obj;
		}
		return false;
	}

	// Display a list of field defs for a module admin
	// Module : name of the module calling
	// keys : more info keys to divide the fields.
	// For instance : key1='product' - key2='product_type_1' - etc..
	// returntotab : name of the tab the fields manager must return to
	public function display_field_defs_list($module=false, $key1=false, $key2=false, $key3=false, $returntotab=false)
	{
		// Search the fields list according to the parameters
		$fielddefs = self::get_field_defs($module, $key1, $key2, $key3);

		$mod = $this->get_main_module();
		$smarty = cmsms()->GetSmarty();
		$gCms = cmsms();
		$id = 'm1_';

		if ($fielddefs)
		{
			$fielddefs_smarty = array();
			$rowclass = 'row1';
			
			foreach ($fielddefs as $onefielddef)
			{
				$onefielddef_smarty = $onefielddef->get_smarty_object();
				$onefielddef_smarty->rowclass = $rowclass;

				// Public image
				$onefielddef_smarty->public_image = cmsms()->variables['admintheme']->DisplayImage('icons/system/' . (($onefielddef_smarty->public==1) ? 'true' : 'false') . '.gif');

				// Editlink
				$edit_params = array();
				if ($returntotab)
					$edit_params['returntotab'] = $returntotab;
				$edit_params['id_item'] = $onefielddef->id;

				$onefielddef_smarty->editlink = $mod->CreateLink($id, 'admin_addedit_fielddef', '', $gCms->variables['admintheme']->DisplayImage('icons/system/edit.gif', $mod->Lang('edit'), '', '', 'systemicon'), $edit_params);

				$onefielddef_smarty->editurl = $mod->CreateLink($id, 'admin_addedit_fielddef', '', $mod->Lang('edit'), $edit_params, '', 1);

				// Delete link
				$onefielddef_smarty->deletelink = $mod->CreateLink($id, 'admin_delete_fielddef', '', $gCms->variables['admintheme']->DisplayImage('icons/system/delete.gif', $mod->Lang('delete'), '', '', 'systemicon'), $edit_params, $mod->Lang('areyousure_field_delete'));
				
				$fielddefs_smarty[] = $onefielddef_smarty;
				($rowclass=="row1" ? $rowclass="row2" : $rowclass="row1");
			}
			$smarty->assign('fielddefs', $fielddefs_smarty);
		}
		$smarty->assign('ALmod', $mod);

		// Add link
		$add_params = array();
		if ($returntotab)
			$add_params['returntotab'] = $returntotab;
		if ($module)
			$add_params['formodule'] = $module;
		if ($key1)
			$add_params['key1'] = $key1;
		if ($key2)
			$add_params['key2'] = $key2;
		if ($key3)
			$add_params['key3'] = $key3;
		$smarty->assign('addlink', $mod->CreateLink($id, 'admin_addedit_fielddef', '', $gCms->variables['admintheme']->DisplayImage('icons/system/newobject.gif', $mod->Lang('addfielddef'), '', '', 'systemicon') . $mod->Lang('addfielddef'), $add_params));
		
		echo $mod->ProcessTemplate('module_admin_list_fielddefs.tpl');
	}

	// Returns all the fields types in an array for a dropdown list
	public function get_fields_types_dropdown()
	{
		// We have to check loaded classes
		$classes = get_declared_classes();
		$fields_classes = array();

		foreach ($classes as $class)
		{
			if (strpos($class, 'AireFieldDef_') !== FALSE)
			{
				// This is a field class
				// Get the friendly language name and the alias name

				/* Php 5.2 get_called_class limitation - we have to create an empty object and get the type */
				$tmp = new $class();

				/*
				// Note : see : http://at-byte.com/comment/2534
				// had an error : T_PAAMAYIM_NEKUDOTAYIM in php 5.2
				//$value = $class::get_field_type();
				$value = call_user_func(array($class, 'get_field_type'));
				//$key = $class::get_friendly_name();
				$key = call_user_func(array($class, 'get_friendly_name'));
				*/
				//$fields_classes[$key] = $value;
				$fields_classes[$tmp->get_friendly_name()] = $tmp->get_field_type();
			}
		}

		return $fields_classes;
	}

	// Verify if name and alias are unique for a given module and group of keys
	public function is_unique_name_alias($name, $alias, $exclude_id_item, $formodule='', $key1='', $key2='', $key3='')
	{
		$db = cmsms()->GetDb();

		$query = 'SELECT COUNT(*) FROM '.$this->table_defs.' WHERE
			(`name`=? OR `alias`=?)';
		$qparams[] = $name;
		$qparams[] = $alias;

		if ($exclude_id_item !== false)
		{
			$query .= ' AND `id` !=?';
			$qparams[] = $exclude_id_item;
		}

		if (!empty($formodule))
		{
			$query .= ' AND `module`=?';
			$qparams[] = $formodule;
		}
		if (!empty($key1))
		{
			$query .= ' AND `key1`=?';
			$qparams[] = $key1;
		}
		if (!empty($key2))
		{
			$query .= ' AND `key2`=?';
			$qparams[] = $key2;
		}
		if (!empty($key3))
		{
			$query .= ' AND `key3`=?';
			$qparams[] = $key3;
		}

		$res = $db->GetOne($query, $qparams);
		if ($res > 0)
			return false;

		return true;
	}

	/* Returns a form for an item edition in backoffice */
	public function get_edit_form($actionid, $formodule, $id_item, $key1='', $key2='', $key3='')
	{
		$edit_form = '';

		$field_defsvals = $this->get_field_defsvals($formodule, $id_item, $key1, $key2, $key3);
		if (is_array($field_defsvals))
		{
			foreach ($field_defsvals as $onefield)
			{
				$edit_form .= $onefield->render_edit_form($actionid);
			}
		}
		
		return $edit_form;
	}

	/* Returns fields defs and vals in only on db query
		useful for an item edition in backoffice */
	public function get_field_defsvals($formodule, $id_item, $key1='', $key2='', $key3='')
	{
		$db = cmsms()->GetDb();
		$req = "SELECT fd.id as 'fd_id', fv.id as 'fv_id',
					fd.create_datetime as 'fd_create_datetime', fd.modify_datetime as 'fd_modify_datetime',
					fv.create_datetime as 'fv_create_datetime', fv.modify_datetime as 'fv_modify_datetime',
					fd.*, fv.* ";
		$req .= "FROM ".$this->table_defs." fd LEFT JOIN ".$this->table_vals." fv ON ( (fd.id=fv.id_fielddef) AND ";

		if ($id_item)
		{
			$req .= "( (fv.id_entry=?) OR (fv.id_entry IS NULL) )";
			$qparams[] = $id_item;
		}
		else
			$req .= "(fv.id_entry IS NULL)";
		
		$req .= ") WHERE fd.module=?";
		$qparams[] = $formodule;

		if (!empty($key1))
		{
			$req .= ' AND fd.key1=?';
			$qparams[] = $key1;
		}
		if (!empty($key2))
		{
			$req .= ' AND fd.key2=?';
			$qparams[] = $key2;
		}
		if (!empty($key3))
		{
			$req .= ' AND fd.key3=?';
			$qparams[] = $key3;
		}

		$req .= ' ORDER BY `position` ASC';

		$res = $db->GetArray($req, $qparams);
		//echo $db->sql;
		
		if ($res)
		{
			// We have the fielddefs and vals (if exists) for one item
			// Lets create the objects for the val and def, with the values of the query result
			$res_defvals = array();
			foreach ($res as $oneline)
			{
				// We MUST create a new array for the "load_from_array" function, cause this function searches for an "id" key, and there are two "id" keys in the db query (def and val)
				$array_for_def = $oneline;
				$array_for_def['id'] = $array_for_def['fd_id'];

				// Same for the create and modify datetime
				$array_for_def['create_datetime'] = $array_for_def['fd_create_datetime'];
				$array_for_def['modify_datetime'] = $array_for_def['fd_modify_datetime'];
				
				$fielddef_obj = $this->get_field_def($oneline['fd_id'], $array_for_def);

				// Now the fieldval obj
				$fieldval_obj = new AireFieldVal();
				// Load the infos only if there is an id
				// If no ID = still no value for this field - keep the default infos
				if ($oneline['fv_id'])
				{
					$array_for_val = $oneline;
					$array_for_val['id'] = $oneline['fv_id'];
					// Same for the create and modify datetime
					$array_for_val['create_datetime'] = $array_for_val['fv_create_datetime'];
					$array_for_val['modify_datetime'] = $array_for_val['fv_modify_datetime'];

					$fieldval_obj->load_from_array($array_for_val);
				}

				// Now load the fielddef inside the fieldval object
				$fieldval_obj->fielddef = $fielddef_obj;

				$res_defvals[] = $fieldval_obj;
			}

			return $res_defvals;
		}
		else
			return false;
	}

	/* Verify and store the fieldvals infos in the DB */
	// All the specific params are prefixed with "afv_" + id of the fielddef
	public function store_fields_from_params($actionid, $params, $id_item)
	{
		$params_for_fields = array();
		foreach ($params as $key=>$val)
		{
			if (strpos($key, 'afv_') !== false)
			{
				$infos = explode('_', $key);

				// if the field has the form : afv_IDFIELDDEF_OtherStuff, create a new key "OtherStuff"
				if (isset($infos[2]))
					$params_for_fields[$infos[1]][$infos[2]] = $val;
				else
					$params_for_fields[$infos[1]] = $val;
			}
		}

		// We have to check the files
		if (isset($_FILES))
		{
			foreach ($_FILES as $key=>$val)
			{
				$key = str_replace($actionid, '', $key);
				if (strpos($key, 'afv_') !== false)
				{
					$infos = explode('_', $key);

					// if the field has the form : afv_IDFIELDDEF_OtherStuff, create a new key "OtherStuff"
					if (isset($infos[2]))
						$params_for_fields[$infos[1]][$infos[2]] = $val;
					else
						$params_for_fields[$infos[1]] = $val;
				}
			}
		}
		
		// We have all the params to manage in the $params_for_fields array
		// The key is the id of the fielddef
		foreach ($params_for_fields as $id_fielddef=>$fieldval_newvalue)
		{
			$fieldval = new AireFieldVal(false, true, $id_fielddef, $id_item);

			// This must return true - if true, everything is ok and we can save
			if ($fieldval->process_new_value($fieldval_newvalue, $id_item))
				$fieldval->addedit();
		}
		
		return true;
	}

	/* Returns all the values for a specific entry - used for display */
	public function get_fieldsbyname($formodule, $id_item, $key1='', $key2='', $key3='')
	{
		$fields = $this->get_field_defsvals($formodule, $id_item, $key1='', $key2='', $key3='');

		if ($fields)
		{
			$result = array();

			foreach ($fields as $onefield)
			{
				$result[$onefield->fielddef->alias] = $onefield->get_data_smarty();
			}
		}
		else
			$result = false;

		return $result;
	}

	/* Delete all the values objects from a fielddef
			Used when a Field DEF is removed */
	public function delete_all_values($id_fielddef)
	{
		if ($id_fielddef)
		{
			$query = "SELECT id FROM ".$this->table_vals." WHERE id_fielddef=?";
			$qparams[] = $id_fielddef;
			$db = cmsms()->GetDb();
			$res = $db->GetArray($query, $qparams);

			$return = true;
			if ($res)
			{
				foreach ($res as $oneval)
				{
					// note : we have use load_fields cause we need the id_fielddef in the val obj
					$val_obj = new AireFieldVal($oneval['id']);
					if (!$val_obj->delete())
						$return =false;
				}
			}
			return $return;
		}
		return false;
	}

	/* Delete all the values objects from an item
			Used when an item is deleted in a third party module */
	public function delete_values_for_item($actionid, $formodule, $id_item, $key1='', $key2='', $key3='')
	{
		$field_vals = $this->get_field_defsvals($formodule, $id_item, $key1, $key2, $key3);

		$res = true;
		if ($field_vals)
		{
			foreach ($field_vals as $onefieldval_obj)
			{
				if (!$onefieldval_obj->delete())
					$res=false;
			}
		}

		return $res;
	}
}

?>