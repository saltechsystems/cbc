<?php

/* Initial class for a custom Field */
class AireFieldDef extends AireDbObject
{
	protected $table = 'aire_fielddefs';

	protected $field_infos = array();
	protected $custom_fields = array(
		'module'=>'',
		'key1'=>'',
		'key2'=>'',
		'key3'=>'',
		'type'=>'',
		'name'=>'',
		'alias'=>'',
		'prompt'=>'',
		'helptext'=>'',
		'public'=>1,
		'position'=>0,
		'attrs'=>''
	);

	// Constructor :
	// Set the type
	public function __construct($id=false, $load_fields=true)
	{
		// Merge custom fields and field_infos
		foreach ($this->field_infos as $fieldname=>$fieldval)
			$this->custom_fields[$fieldname] = $fieldval;
		
		parent::__construct($id, $load_fields);

		if (!$id)
		{
			// Set the type in terms of the final class name
			$this->type = $this->get_field_type();
		}

		// Field attributes in a serialized array
		$this->unserialize_attrs();
		$this->attrs_to_vars();
	}

	// Unserialize the attrs
	public function unserialize_attrs()
	{
		if (!empty($this->attrs) and is_string($this->attrs))
			$this->attrs = unserialize($this->attrs);
	}

	// Returns the type of the object
	public function get_field_type()
	{
		return false;
		
		// Set the type in terms of the final class name
		// DOESN'T WORK IN PHP 5.2 - SHOULD BE UNCOMMENTED IN CMSMS 1.11 (php 5.3)
		/*if (strpos(get_called_class(), '_'))
		{
			$type = str_replace('AireFieldDef_','',get_called_class());
			return $type;
		}
		return false;*/
	}

	// Returns the friendly type name based on the class name
	public function get_friendly_name ()
	{
		if (isset($this->type))
		{
			$mod = cms_utils::get_module('AireLibs');
			return $mod->Lang('fielddef_name_' . $this->type);
		}
		return false;
	}

	// Replace the addedit function to serialize the attrs values
	public function addedit($onlyfieldname=false)
	{
		if (!empty($this->attrs))
			$this->attrs = serialize($this->attrs);

		// We MUST unset some vars to make the addedit
		$this->unset_field_infos();

		return parent::addedit($onlyfieldname);
	}

	// A function that has to be implemented in each subclasses
	// Returns the admin form for the field def
	public function get_admin_form(&$mod, $actionid)
	{
		$fields = $this->get_admin_form_fields($mod, $actionid);

		$smarty = cms_utils::get_smarty();
		$smarty->assign('fields', $fields);
		$form = $mod->ProcessTemplate('fielddef_form.tpl');
		
		return $form;
	}

	// Load from params
	public function load_from_params($params)
	{
		// General infos
		if ($params['formodule'])
			$this->module = $params['formodule'];

		// We use a new array to filter params
		$tmp = array();
		foreach ($this->field_infos as $key=>$val)
		{
			$tmp[$key] = $params[$key];
		}
		$tmp = serialize($tmp);
		$params['attrs'] = $tmp;
		
		$this->load_from_array($params);

		// Now manage attrs infos in an array
		//$this->attrs = $this->attrs_to_array();
	}

	// Place the attrs in an array
	public function attrs_to_array()
	{
		$res = array();

		foreach ($this->field_infos as $key=>$val)
			$res[$key] = $this->$key;

		return $res;
	}

	// Unset the non-db vars
	public function unset_field_infos()
	{
		foreach ($this->field_infos as $key=>$val)
			unset($this->fields[$key]);
	}

	// Load from array has to include the serialize function
	public function load_from_array($infos)
	{
		// Just trim the alias
		if (isset($infos['alias']))
			$infos['alias'] = trim($infos['alias']);
	
		parent::load_from_array($infos);

		// Important : we have to update the attr array after the load_from_array
		//$this->attrs = $this->attrs_to_array();
		$this->unserialize_attrs();
		$this->attrs_to_vars();
	}

	/* copy the attrs infos to object info */
	public function attrs_to_vars()
	{
		// Load the field specific vars from the attrs array
		if (is_array($this->attrs))
		{
			foreach ($this->attrs as $key=>$val)
			{
				$this->$key = $val;
			}
		}
	}

	/* Create the form field for the item edition */
	public function render_edit_form($actionid, $value='')
	{
		$mod = cms_utils::get_module('AireLibs');
		$smarty = cmsms()->GetSmarty();
		return $this->get_edit_form($mod, $actionid, $value, $smarty);
	}

	/* Function to be overriden by the objects to render the item edition form */
	public function get_edit_form($mod, $actionid, $value, $smarty)
	{
		return false;
	}

	/* Validate the new value and return it */
	/* Can be overriden by the subobjects to process the value (for example for files, etc..)
	id_item = the id of the article, entity, product, etc.. in the module - useful for files names
	 */
	public function validate_value($newvalue, $id_item, $current_value=false)
	{
		return $newvalue;
	}

	/* Add extra data to the value object for smarty
	This function here does nothing, bug it can be overwritten by the other classes
	This function is called to get some extra informations for the frontend display of the fieldval
	return false or and array that will be merged with the other field infos for smarty
	*/
	public function add_extra_data($fieldval_obj)
	{
		return false;
	}

	/* Delete all the values of this fielddef */
	public function delete_all_values()
	{
		$FieldsManager = new AireFieldsManager();
		return $FieldsManager->delete_all_values($this->id);
	}

	/* Delete with all values */
	public function delete()
	{
		if ($this->delete_all_values())
			return parent::delete();
		else
			return false;
	}

	/* handle delete for specific files or other type specific stuff (ex : files) */
	/* Here returns true, but can be overriden */
	public function delete_extra_operations(&$fieldval_obj)
	{
		return true;
	}
}

?>