<?php
#-------------------------------------------------------------------------
# Module: AireLibs
# Method: Upgrade
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2008 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
#-------------------------------------------------------------------------
if (!isset($gCms)) exit;

$current_version = $oldversion;
switch($current_version)
{
	// we are now 1.0 and want to upgrade to latest
	case "1.0":
	{
		// Nothing to do
	}
}

// put mention into the admin log
$this->Audit( 0, $this->Lang('friendlyname'), $this->Lang('upgraded', $this->GetVersion()));
?>