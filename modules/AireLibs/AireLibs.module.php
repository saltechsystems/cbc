<?php
#-------------------------------------------------------------------------
# Module: AireLibs
# Author: Mathieu MUTHS (Company Aire Libre) contact@airelibre.fr
# A libs base module for CMS Made Simple
# This module gives some useful object classes for other modules
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
#-------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin 
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#-------------------------------------------------------------------------

class AireLibs extends CMSModule
{
	function GetName() { return 'AireLibs'; }
	function GetFriendlyName() { return ($this->Lang('friendlyname')); }
	function GetVersion() { return '1.3.2'; }
	function GetHelp() { return $this->Lang('help'); }
	function GetAuthor() { return 'AireLibre'; }
	function GetAuthorEmail() { return 'contact@airelibre.fr'; }
	function GetChangeLog() { return file_get_contents(__FILE__).'/doc/changelog.html'; }
	function MinimumCMSVersion() {return '1.10';}
	//function MaximumCMSVersion() {return '1.11';}

	// No admin (for the time being)
	function HasAdmin() { return false; }
	function GetAdminSection() { return 'extensions'; }
	function GetAdminDescription () { return $this->Lang('admindescription'); }

	/* Constructor */
	public function __construct()
	{
		parent::__construct();

		// Simply load the libs - Note : this is already done by CGExtensions if installed
		require_once($this->GetModulePath()."/lib/class.AireObject.php");
		require_once($this->GetModulePath()."/lib/class.AireDbObject.php");
		require_once($this->GetModulePath()."/lib/class.AireTools.php");
		require_once($this->GetModulePath()."/lib/class.AireFieldsManager.php");
		require_once($this->GetModulePath()."/lib/class.AireFieldDef.php");
		require_once($this->GetModulePath()."/lib/class.AireFieldVal.php");

		// Fields classes
		foreach (glob($this->GetModulePath()."/lib/fielddefs/*.php") as $filename)
			require_once($filename);
	}

	/* Frontend protection */
	function IsPluginModule() { return false; }
	
	function InitializeFrontend()
	{
		$this->RestrictUnknownParams();
	}
}

?>