<?php

$lang['friendlyname'] = 'Extensions AireLibs';
$lang['admindescription'] = "Module de librairies/extensions pour la gestion d'objets dans la base de données et la création de définitions de champs dans d'autres modules";

# General
$lang['edit'] = 'Modifier';
$lang['delete'] = 'Supprimer';
$lang['yes'] = 'Oui';
$lang['no'] = 'Non';
$lang['areyousure'] = 'Êtes-vous sûr de vouloir supprimer cet élément ?';
$lang['areyousure_field_delete'] = "Êtes-vous sûr de vouloir supprimer ce champ ? Toutes les données associées vont être supprimées !";
$lang['add_field'] = 'Nouveau champ extra';
$lang['modify_field'] = 'Modification du champ';
$lang['generate_alias_from_name'] = 'Générer un alias à partir du nom';

# Fielddefs
$lang['general_field_infos'] = 'Infos générales sur le champ';
$lang['field_name'] = 'Nom du champ';
$lang['field_alias'] = 'Alias / Nom de la variable Smarty';
$lang['alias_warning'] = "Merci de n'utiliser que des chiffres/lettres et le caractère '-' dans ce champ<br /><strong>Important</strong> : si vous changez cette valeur, vous devrez probablement mettre à jour vos gabarits d'affichage";
$lang['field_prompt'] = 'Intitulé de saisie à afficher aux éditeurs';
$lang['public'] = 'Public';
$lang['field_public'] = 'Champ public';
$lang['field_help'] = 'Aide';

$lang['specific_field_infos'] = 'Infos spécifiques pour le champ';
$lang['field_type'] = 'Type';

# For each fielddef
$lang['fielddef_name_text'] = 'Texte simple';
$lang['fielddef_name_textarea'] = 'Zone de texte';
$lang['fielddef_name_image'] = 'Image';
$lang['fielddef_name_file'] = 'Fichier';
$lang['fielddef_name_checkboxes'] = 'Case(s) à cocher';

# Fields labels
# Text
$lang['size'] = 'Taille';
$lang['max_length'] = 'Longueur max';
# Textarea
$lang['use_wysiwyg'] = "Utiliser l'éditeur WYSIWYG";
# Image
$lang['max_weight_kb'] = 'Poids max en ko';
$lang['max_weight'] = 'Poids max';
$lang['kb'] = 'ko';
$lang['extensions'] = "Types d'images autorisées - Liste des extensions séparées par des virgules";
$lang['authorized_extensions'] = 'Extensions autorisées';
$lang['path_to_image'] = "Chemin vers l'image";
$lang['help_path_to_image'] = 'Vous pouvez utiliser ces variables :
<br />
<strong>{$entry_id}</strong>:  l\'ID de l\'élément enregistré<br />
<strong>{$file_name}</strong>: le nom du fichier sans l\'extension<br />
<strong>{$file_extension}</strong>: l\'extension du fichier<br />
<br />Conseil : si vous avez plusieurs champs "fichier" ou "image", il est recommandé d\'ajouter un préfixe sur le nom du fichier, par exemple : <strong>logo_{$file_name}.{$file_extension}</strong>';
$lang['delete_image'] = 'Supprimer cette image';
$lang['choose_other_image'] = "Téléverser une autre image (l'image courante sera remplacée)";
# File
$lang['path_to_file'] = 'Chemin vers le fichier';
$lang['delete_file'] = 'Supprimer ce fichier';
$lang['choose_other_file'] = 'Choisir un autre fichier (le fichier courant sera remplacé)';
# Checkboxes
$lang['options_checkboxes'] = 'Case(s) à cocher';
$lang['help_fielddef_checkboxes'] = "Spécifiez les options pour la/les case(s) à cocher selon la forme : intitulé=valeur. Une case à cocher par ligne. Séparez les intitulés des valeurs avec le caractère '='. Par exemple : Ouvert le dimanche=ouvert-dimanche";

# Links
$lang['addfielddef'] = 'Ajouter un champ extra';

# Errors
$lang['error_noname_noalias'] = "Merci de saisir le nom et l'alias";
$lang['error_duplicate_alias_name'] = "Le nom et/ou l'alias existent déjà";

# Help
$lang['help'] = "
<h3>Que fait ce module ?</h3>
<p>Ce module est une collection de librairies / classes utiles pour différents modules. Il fournit une classe de base nommée <strong>AireDbObject</strong> permettant à d'autres modules de créer des objets liés à une entrée dans la base de données.</p>
<h3>Comment l'utiliser ?</h3>
<ul>
<li>Créez une nouvelle classe qui étend <strong>AireDbObject</strong> dans votre module</li>
<li>Définissez le nom de la table, les champs spécifiques (id, modify_datetime, create_datetime sont là par défaut)</li>
<li>Utilisez votre objet avec un code du type :</li>
</ul>
<pre>
	&#36;myobject = new MyObject(&#36;id_object);
	echo &#36;myobject->id;
</pre>
<p>Où <strong>MyObject</strong> est une classe qui étend <strong>AireDbObject</strong></p>

<h3>Assistance</h3>
<p>Ce module est fourni sans aucune assistance commerciale. Cependant,
vous pouvez tout de même obtenir de l'aide :</p>
<ul>
<li>Pour obtenir la dernière version de ce module, consulter la FAQ,
ou signaler un bug, rendez-vous sur la Developers Forge de CMS Made
Simple, et faites une recherche sur 'EventsManager'.</li>
<li>Pour obtenir une assistance commerciale, contactez par mail
Mathieu Muths, l'auteur de ce module : <a
href=\"mailto:mailto:contact@airelibre.fr\">contact@airelibre.fr</a></li>
<li>Les Forums CMS Made Simple sont aussi une préciseuse source
d'informations.</li>
<li>Si vous êtes francophone, n'hésitez pas à consulter le Forum CMS
Made Simple Français où l'auteur du module est régulièrement présent
(pseudo : <strong>airelibre</strong>)</li>
</ul>
<h3>Copyright et Licence</h3>
<p>Copyright © 2011, Mathieu Muths (contact@airelibre.fr) - Tous droits
réservés</p>
<p>Ce programme est un logiciel libre - Vous pouvez le redistribuer
et/ou le modifier selon les termes de la GNU GPL (General Public
Licence) telle que diffusée par la Free Software Fundation en version 2
ou supérieure.</p>
<p>De plus, et telle une exception supplémentaire de la licence GPL, ce
logiciel est un module pour CMS Made Simple. Vous ne devez donc pas
l'utiliser dans une version non conforme à la GPL de CMS Made Simple,
ou dans n'importe quelle version de CMS Made Simple qui n'indiquerait
pas clairement dans son administration que le site a été conçu avec CMS
Made Simple.</p>
<p>Ce module a été distribué dans l'espoir d'être utile, mais sans
AUCUNE GARANTIE. Il vous appartient de le tester avant toute mise en
production, que ce soit dans le cadre d'une nouvelle installation ou
d'une mise à jour du module. L'auteur du module ne pourrait être tenu
pour responsable de tout dysfonctionnement du site provenant de ce
module. Pour plus d'informations, <a
href=\"http://www.gnu.org/licenses/licenses.html#GPL\" target=\"_blank\">consultez
la licence GNU GPL</a>.</p>
<p>L'icône du module a été créée par Everaldo. Vous pouvez télécharger ses belles icônes sur : <a href='http://www.everaldo.com' target='_blank'>www.everaldo.com</a></p>";
?>