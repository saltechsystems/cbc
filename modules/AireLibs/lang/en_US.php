<?php

$lang['friendlyname'] = 'AireLibs Extensions';
$lang['admindescription'] = 'Extensions module for Db objects base and field defs in other modules';

# General
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['yes'] = 'Yes';
$lang['no'] = 'No';
$lang['areyousure'] = 'Are you sure you want to delete this item?';
$lang['areyousure_field_delete'] = 'Are you sure you want to delete this field? All the associated data will be deleted!';
$lang['add_field'] = 'New custom field';
$lang['modify_field'] = 'Field modification';
$lang['generate_alias_from_name'] = 'Generate an alias from the name';

# Fielddefs
$lang['general_field_infos'] = 'General field informations';
$lang['field_name'] = 'Field name';
$lang['field_alias'] = 'Alias / Name of the Smarty var';
$lang['alias_warning'] = 'Please use only letters, numbers and "-" in this field<br /><strong>Important</strong>: if you change this value, you may have to update all your display templates';
$lang['field_prompt'] = 'Prompt to show to the editors';
$lang['public'] = 'Public';
$lang['field_public'] = 'Public field';
$lang['field_help'] = 'Help';

$lang['specific_field_infos'] = 'Field specific informations';
$lang['field_type'] = 'Type';

# For each fielddef
$lang['fielddef_name_text'] = 'Simple text';
$lang['fielddef_name_textarea'] = 'Text area';
$lang['fielddef_name_image'] = 'Image';
$lang['fielddef_name_file'] = 'File';
$lang['fielddef_name_checkboxes'] = 'Checkbox(es)';

# Fields labels
# Text
$lang['size'] = 'Size';
$lang['max_length'] = 'Max length';
# Textarea
$lang['use_wysiwyg'] = 'Use WYSIWYG editor';
# Image
$lang['max_weight_kb'] = 'Max weight in kb';
$lang['max_weight'] = 'Max weight';
$lang['kb'] = 'kb';
$lang['extensions'] = 'Authorized image extensions (comma seperated)';
$lang['authorized_extensions'] = 'Authorized extensions';
$lang['path_to_image'] = 'Path to the image';
$lang['help_path_to_image'] = 'You can use some variables :
<br />
<strong>{$entry_id}</strong>:  the item/entry/thing ID<br />
<strong>{$file_name}</strong>: the filename without the extension<br />
<strong>{$file_extension}</strong>: the file extension without the dot<br />
<br />Advice: if you have multiple files on one item, it\'s recommended to add a prefix on the file name, like: <strong>logo_{$file_name}.{$file_extension}</strong>';
$lang['delete_image'] = 'Delete this image';
$lang['choose_other_image'] = 'Choose another image (the current image will be replaced)';
# File
$lang['path_to_file'] = 'Path to the file';
$lang['delete_file'] = 'Delete this file';
$lang['choose_other_file'] = 'Choose another file (the current file will be replaced)';
# Checkboxes
$lang['options_checkboxes'] = 'Checkboxe(s)';
$lang['help_fielddef_checkboxes'] = 'Specify the options for the checkboxes in the form: prompt=value. One line per entry. Seperate keys from values with the = character. i.e: Open on sunday=open-sunday';

# Links
$lang['addfielddef'] = 'Add a new field definition';

# Errors
$lang['error_noname_noalias'] = 'Please fill the name and alias fields';
$lang['error_duplicate_alias_name'] = 'The name and/or the alias is still existing';

# Help
$lang['help'] = "
<h3>What does this do?</h3>
<p>This is a base module for other AireLibre modules. It does mainly provide a ALDbObject class to easily handle database 'objects'.</p>
<h3>How do I use it?</h3>
<p>Todo</p>

<h3>Support</h3>
<p>This module does not include commercial support. However there are a number of resources available to help you with it:</p>
<ul>
	<li>For the latest version of this module, FAQs or to file a bug report, please visit the cms made simple Developers Forge and do a search for 'EventsManager'</li>
	<li>To obtain commercial support, please send an email to the author Mathieu Muths (french prefered but I can speak english) : <a href=\"mailto:mailto:contact@airelibre.fr\">contact@airelibre.fr</a></li>
	<li>Additional discussion of this module may also be found in the CMS Made Simple Forms.</li>
</ul>
<h3>Copyright et Licence</h3>
<p>Copyright © 2012, Mathieu Muths (contact@airelibre.fr) - All rights reserved</p>
<p>This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.</p>
<p>However, as a special exception to the GPL, this software is distributed as an addon module to CMS Made Simple. You may not use this software in any Non GPL version of CMS Made simple, or in any version of CMS Made simple that does not indicate clearly and obviously in its admin section that the site was built with CMS Made simple.</p>
<p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA Or <a
href=\"http://www.gnu.org/licenses/licenses.html#GPL\" target=\"_blank\">read it online</a></p>
<p>The module icon has be designed by Everaldo - You can download some nice icons on: <a href='http://www.everaldo.com' target='_blank'>www.everaldo.com</a></p>
";
?>