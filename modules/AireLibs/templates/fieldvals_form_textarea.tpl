{if isset($fields)}
	{foreach from=$fields item='onefield'}
		<div class="pageoverflow">
			<p class="pagetext">{$onefield.label}:</p>
			{if !empty($onefield.help)}
				<p class="pageinput" style="padding-bottom: 5px">
					{eval var=$onefield.help} :
				</p>
			{/if}
			<p class="pageinput">
				{$onefield.input}
			</p>
		</div>
	{/foreach}
{/if}