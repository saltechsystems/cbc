{if isset($fields)}
	{foreach from=$fields item='onefield'}
		<div class="pageoverflow">
			<p class="pagetext">{$onefield.label}:</p>
			<p class="pageinput">
				{$onefield.input}

				{if !empty($onefield.help)}
					<br />
					{$onefield.help}
				{/if}
			</p>
		</div>
	{/foreach}
{/if}