{if isset($fields)}
	{foreach from=$fields item='onefield'}
		<div class="pageoverflow">
			<p class="pagetext">{$onefield.label}:</p>

			{if isset($current_file_url)}
				<p class="pageinput">
					<a href="{$current_file_url}">{$current_file_name}</a>
					{$delete_checkbox_input} {$delete_checkbox_label}
				</p>

				<p class="pagetext">{$choose_other_file}:</p>
			{/if}
			<p class="pageinput">
				{$onefield.input}

				{if !empty($onefield.help)}
					<br />
					{eval var=$onefield.help}
				{/if}
			</p>
		</div>
	{/foreach}
{/if}