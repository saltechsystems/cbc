{if isset($fields)}
	{foreach from=$fields item='onefield'}
		<div class="pageoverflow">
			<p class="pagetext">{$onefield.label}:</p>

			{if $current_image}
				<p class="pageinput">
					<img src="{$current_image}" style="max-width: 200px; max-height: 80px; margin-right: 15px; vertical-align: middle" />
					{$delete_checkbox_input} {$delete_checkbox_label}
				</p>

				<p class="pagetext">{$choose_other_image}:</p>
			{/if}
			<p class="pageinput">
				{$onefield.input}

				{if !empty($onefield.help)}
					<br />
					{eval var=$onefield.help}
				{/if}
			</p>
		</div>
	{/foreach}
{/if}