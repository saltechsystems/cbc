
{if isset($fielddefs)}
<div class="pageoptions"><p class="pageoptions">{$addlink}</p></div>

<table cellspacing="0" class="pagetable">
	<thead>
		<tr>
			<th>ID</th>
			<th>{$ALmod->Lang('field_name')}</th>
			<th>{$ALmod->Lang('field_alias')}</th>
			<th>{$ALmod->Lang('field_type')}</th>
			<th>{$ALmod->Lang('public')}</th>
			<th class="pageicon">&nbsp;</th>
			<th class="pageicon">&nbsp;</th>
		</tr>
	</thead>
	<tbody>

	{foreach from=$fielddefs item=entry}
		<tr class="{$entry->rowclass}">
			<td>{$entry->id}</td>
			<td><a href="{$entry->editurl}">{$entry->name}</a></td>
			<td>{$entry->alias}</td>
			<td>{$entry->type}</td>
			<td>{$entry->public_image}</td>
			<td>{$entry->editlink}</td>
			<td>{$entry->deletelink}</td>
		</tr>
	{/foreach}
	</tbody>
</table>
{/if}

<div class="pageoptions"><p class="pageoptions">{$addlink}</p></div>