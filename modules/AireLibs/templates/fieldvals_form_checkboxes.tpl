{if isset($fields)}
	<div class="pageoverflow">
		<p class="pagetext"><label>{$checkboxes_label}</label>:</p>
		<p class="pageinput">
			{if !empty($onefield.help)}
				{eval var=$checkboxes_help}<br />
			{/if}
			{foreach from=$fields item='onefield'}
				{$onefield.hidden|replace:'id="':'id="tmp'}
				{$onefield.input}&nbsp;{$onefield.label}<br />
			{/foreach}
		</p>
	</div>
{/if}