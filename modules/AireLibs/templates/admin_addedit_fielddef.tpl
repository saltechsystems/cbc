{literal}
<script type='text/javascript'>

function generate_alias_from_name()
{
	var text = $('#NameInput input').val();
	text = $.trim(text);
	text = text.replace(/ /g, '-');
	text = text.toLowerCase();

	$('#AliasInput input').val(text);
}

$(document).ready(function($){
	$('#NameInput input').blur(function () {
		if ($('#AliasInput input').val() == '')
		{
			generate_alias_from_name();
		}
	});

	$('#TypeContainer select').change(function () {
		$('#SpecificFieldInfos').html('<img src="{/literal}{root_url}{literal}/modules/AireLibs/images/fancybox_loading.gif" />');
		val = $(this).val();
		data = '{/literal}{$ajax_data|replace:"&amp;":"&"}{literal}';
		data = data.replace('replacetypeajax', val);

		$.ajax({
			type: "POST",
			url: '{/literal}{$ajax_url}{literal}',
			async: true,
			data: data,
			success: function (retour) {
				$('#SpecificFieldInfos').html(retour);
			}
		});

		return false;
	});
});
</script>
{/literal}


<h3>{$title}</h3>

{$form_start}
	{*<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput">{$submit}{$cancel}</p>
	</div>*}
	<fieldset id="GeneralFieldInfos">
		<legend>{$mod->Lang('general_field_infos')}</legend>
		<div class="pageoverflow">
			<p class="pagetext">{$name_label}*:</p>
			<p class="pageinput" id="NameInput">{$name_input}</p>
		</div>
		<div class="pageoverflow">
			<p class="pagetext">{$alias_label}*:</p>
			<p class="pageinput" id="AliasInput">{$alias_input} <a href="javascript: generate_alias_from_name()">{$mod->Lang('generate_alias_from_name')}</a>
			<br />{$mod->Lang('alias_warning')}
			</p>
		</div>
		<div class="pageoverflow">
			<p class="pagetext">{$prompt_label}:</p>
			<p class="pageinput">{$prompt_input}</p>
		</div>
		<div class="pageoverflow">
			<p class="pagetext">{$public_label}:</p>
			<p class="pageinput">{$public_input}</p>
		</div>
		<div class="pageoverflow">
			<p class="pagetext">{$help_label}:</p>
			<p class="pageinput">{$help_input}</p>
		</div>
	</fieldset>
	<fieldset>
		<legend>{$mod->Lang('specific_field_infos')}</legend>
		{if $mode eq 'add'}
			<div class="pageoverflow">
				<p class="pagetext">{$type_label}:</p>
				<p class="pageinput" id="TypeContainer">{$type_input}</p>
			</div>
		{else}
			<div class="pageoverflow">
				<p class="pageinput">Type : <strong>{$type_label}</strong></p>
			</div>
		{/if}
		<div id="SpecificFieldInfos">
			{$field_form}
		</div>
	</fieldset>

	<div class="pageoverflow" style="clear: both">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput">{$submit}{$cancel}</p>
	</div>
{$hidden}
{$form_end}