<?php

/**
 * A base class notification message object.  This is used to convey information that can be formatted and sent out via various channels
 *
 * This object is used to send data to various twitter, facebook, mail, and other modules.  some channels may not use all of the fields
 * or may aggregate the data in various ways.
 *
 * fields:
 *   subject:     string - the message subject
 *   body:        string - the message body
 *      Some channels may strip html out of the body, and/or shorten URLS or do other processing to make the text compliant with 
 *      channel requirements. 
 *   module:      string - The name of the originating module
 *   priority:    integer/const - A message priority (1 = high, 2 = normal, 3 = low)
 *   to:          integer - A user identifer
 *   lat:         float - Latitude
 *   long:        float - Longitude
 *   html:        boolean - May indicate that the message is an HTML message
 *   ischeckin:   boolean - May indicate that the message is a user checkin
 *   link:        string - URL to a link to attach to the message
 *   linkname:    string - A name for the link
 *   caption:     string - A caption for the link
 *   description: string - A description for the link
 *   picture:     string - URL to an image to attach to the message.
 *   shorten:     boolean - May indicate that URLS in the message body (and possibly the link) can be shortened.
 */
class notification_message 
{
  const PRIORITY_HIGH = 1;
  const PRIORITY_NORMAL = 2;
  const PRIORITY_LOW = 3;

  private static $_keys = array('subject','body','module','priority','to','lat','long','html','ischeckin','link','linkname',
				'caption','description','picture','shorten');
  private $_data = array();

  public function __get($okey)
  {
    // key translation
    if( $okey == 'message' || $okey == 'msg' ) $okey = 'body';

    $key = strtolower($okey);
    if( !in_array($key,self::$_keys) ) throw new Exception('Attempt to retrieve invalid key '.$okey.' from message object');
    if( isset($this->_data[$key]) ) return $this->_data[$key];
    
    switch($key) {
    case 'priority':
      return self::PRIORITY_NORMAL;

    case 'to':
      return 0;

    case 'html':
      return 0;

    case 'module':
      return -1;

    case 'shorten':
      return 0;
    }
  }

  public function __set($key,$value)
  {
    if( $key == 'message' || $key == 'msg' ) $key = 'body';

    $key = strtolower($key);
    if( !in_array($key,self::$_keys) ) throw new Exception('Attempt to store invalid data into message object');

    $this->_data[$key] = $value;
  }

  public function __isset($key)
  {
    if( !in_array($key,self::$_keys) ) throw new Exception('Attempt to retrieve invalid key '.$okey.' from message object');
    return isset($this->_data[$key]);
  }

  public function valid_key($key)
  {
    if( $key == 'message' || $key == 'msg' ) $key = 'body';
    return in_array($key,self::$_keys);
  }
} // end of class

#
# EOF
#
?>