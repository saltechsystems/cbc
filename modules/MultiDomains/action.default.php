<?php
/*======================================================================================
Module: MultiDomains
======================================================================================*/

// Check authorisation
if(!is_object(cmsms())) exit;

// Case if page id or parameter was given
if (isset($params['pageid']) || isset($params['pagealias'])) {
	// Get parameters
	$pageid = isset($params['pageid']) ? (int)$params['pageid'] : 0;
	$pagealias = isset($params['pagealias']) ? $params['pagealias'] : '';
	$linkvalue = isset($params['linkvalue']) ? $params['linkvalue'] : '';
	if (!$pageid && $pagealias) $pageid = $db->GetOne('SELECT content_id FROM '.cms_db_prefix().'content WHERE content_alias = ?', array($pagealias));
	if (!$pageid) return '';
	// Get URL
	echo $this->GetURL($pageid,$linkvalue);
} else {
	// Check if module was already used
	if (!$this->mdused) {
		// Set the module used
		$this->mdused = true;
		// Set content id
		$this->mdpageid = (int)cmsms()->variables['content_id'];
		// Log visit
		$log = $this->GetPreference('stats_use') ? $this->PrepareLogVisit() : array();
		// Look for redirect domains
		$this->RedirectDomain($log);
		// Assign site to smarty template vars
		$smarty->assign('mdsite',$this->mddomain);
		$smarty->assign('mddevsite',$this->mddev ? $this->mddevdomain : '');
	}
}
return '';

// EOF