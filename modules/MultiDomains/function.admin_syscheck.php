<?php
/*======================================================================================
Module: MultiDomains
======================================================================================*/

// Check authorisation
if(!is_object(cmsms())) exit;
if ( !$this->CheckPermission('Manage MultiDomains') ) exit;

// Set variables
$tabto = 'syscheck';
$myconfig = cmsms()->GetConfig();
$cversion = 2.1;

// Check .htaccess
$htsuccess = array();
$hterror = array();
// Ist .htaccess vorhanden?
if (!is_file($myconfig['root_path'].'/.htaccess')) {
	$hterror[] = $this->Lang('htaccess_missing');
} else {
	// Set arrays
	$htkeys = array('rewriteengine','rewritebase','rewritecond','rewriterule');
	$hterror = array(
		'RewriteEngine' => 'On',
		'RewriteBase' => '/',
		'RewriteCond-1' => '%{REQUEST_URI} !/$',
		'RewriteCond-2' => '%{REQUEST_URI} !\.',
		'RewriteCond-3' => '%{REQUEST_METHOD} !POST$',
		'RewriteRule-1' => '^(.*) %{REQUEST_URI}/ [NE,R=301,L]',
		'RewriteCond-4' => '%{REQUEST_FILENAME} !-f',
		'RewriteCond-5' => '%{REQUEST_FILENAME} !-d',
		'RewriteRule-2' => '^(.+)$ index.php?page=$1 [QSA]'
	);
	// Datei in ein Array einlesen
	$htaccess = @file($myconfig['root_path'].'/.htaccess');
	// Array durchlaufen
	foreach($htaccess as $linenumber=>$data) {
		// Zeile aufteilen
		$line = explode(' ',trim(preg_replace("/\t/",' ',strtolower($data))));
		$key = array_shift($line);
		$value = str_replace('\'','\\\'',trim(implode(' ',$line)));
		if (in_array($key,$htkeys)) { // Pr�fen, ob Eintrag relevant ist
			// hterror durchlaufen
			foreach ($hterror as $k=>$v) {
				$rkey = preg_replace('/-\d/','',strtolower($k));
				// Wenn Zeile noch in $hterrer drin ist, nach $htsuccess verschieben
				if ($key==$rkey && strpos($value,strtolower($v))!==false) {
					$htsuccess[] = $data;
					unset($hterror[$k]);
				}
			}
		}
	}
	// Fehler-Array formatieren
	foreach ($hterror as $k=>$v) {
		$hterror[$k] = preg_replace('/-\d/','',$k).' '.$v;
	}
}

// Check config.php
$verror = $this->Lang('config_not_modified');
$csuccess = array();
$cerror = array();
$ctest = array('url_rewriting','process_whole_template','root_url','root_path');
// Datei in ein Array einlesen
$configphp = @file($myconfig['root_path'].'/config.php');
// Array durchlaufen
$botstop = false;
foreach($configphp as $linenumber=>$data) {
	// Test, ob Bot eine Pause machen soll ;)
	if (strpos($data,'bot-command:')!==false) {
		// Test, ob Bot eine Pause machen soll ;)
		if (strpos($data,'bot-sleep: start')!==false) $botstop = true;
		if (strpos($data,'bot-sleep: stop')!==false) $botstop = false;
		if ($botstop) continue;
		// Kommentar-Zeichen entfernen
		$data = preg_replace('/[\/\*]/','',strtolower($data));
		// Zeile aufteilen
		$line = explode(':',trim($data));
		$key = trim(array_shift($line));
		if (substr($key,0,11)=='bot-command') {
			$value = trim(implode(':',$line));
			if (strpos($value,'=')!==false) {
				$l = explode('=',$value);
				$k = trim(array_shift($l));
				$v = trim(implode('=',$l));
				if ($k=='version') {
					$v = (float)str_replace('\'','',$v);
					if ($v!=$cversion) $cerror[] = $this->Lang('config_wrong_version');
					$verror = '';
				}
			}
		}
	}
}
if ($verror) $cerror[] = $verror;
$cbutton = '';
if ($cerror) {
	$cwriteable = is_writable($myconfig['root_path'].'/config.php');
	$cbutton = $this->CreateFormStart($id,'admin_syscheck',$returnid,'post','multipart/form-data',true,array());
	$cbutton.= $this->CreateInputHidden($id,'tabto',$tabto);
	$cbutton.= $this->CreateInputHidden($id,'config_todo',$cwriteable ? 'modify' : 'download');
	$cbutton.= $this->CreateInputSubmit($id,'submit',$cwriteable ? $this->Lang('config_modify') : $this->Lang('config_download'));
	$cbutton.= $this->CreateFormEnd();
}

// Assign Smarty vars
$smarty->assign('desc_htsuccess',$this->Lang('desc_htsuccess'));
$smarty->assign('htsuccess',$htsuccess ? implode("<br />\n",$htsuccess) : '');
$smarty->assign('desc_hterror',$this->Lang('desc_hterror'));
$smarty->assign('hterror',$hterror ? implode('<br />',$hterror) : '');
$smarty->assign('htaccess_ok',$this->Lang('htaccess_ok'));
$smarty->assign('desc_csuccess',$this->Lang('desc_csuccess'));
$smarty->assign('csuccess',$csuccess ? implode("<br />\n",$csuccess) : '');
$smarty->assign('desc_cerror',$this->Lang('desc_cerror'));
$smarty->assign('cerror',$cerror ? implode('<br />',$cerror) : '');
$smarty->assign('config_ok',$this->Lang('config_ok'));
$smarty->assign('cbutton',$cbutton);
$smarty->assign('morehelp',$this->Lang('morehelp'));

// Display Template
echo $this->ProcessTemplate('admin_syscheck.tpl');

// EOF