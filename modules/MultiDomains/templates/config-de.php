<?php

// CMS made simple Konfigurations Datei
// Version 2.1 (ab CMSms 1.11.x)
// Deutsche, verbesserte Version
// Änderungen von: Andi Petzoldt, https://plus.google.com/113580733838250075244

// Hinweis: bot-sleep und bot-command sind Anweisungen für Scripte - bitte nicht löschen oder verändern
/* bot-command: version='2.1' */

// Einstellungen für Live-Server
$config['dbms'] = 'mysqli'; // mysql oder mysqli
$config['db_hostname'] = 'localhost'; // MySQL Hostname
$config['db_username'] = 'root'; // MySQL Username
$config['db_password'] = 'root'; // MySQL Passwort
$config['db_name'] = 'cms'; // MySQL Datenbank-Name
$config['db_port'] = '3306'; // MySQL Port, meist 3306, bei manchen Providern wird für MySQL 5 Datenbanken auch 3307 verwendet
$config['db_prefix'] = 'cms_'; // Standard ist cms_ - bei DB-Benutzung durch verschiedene CMS bzw. Applikationen kann der DB Prefix geändert werden.
$config['subdir'] = ''; // Standard ist kein Unterverzeichnis (Variable leer lassen).
$config['server_prefix'] = ''; // Wenn Sie einen Prefix (z.B. www.) vor dem Servernamen ($_SERVER['SERVER_NAME']) verwenden wollen (ist z.B. aus SEO-Gründen oft nützlich)
$config['http_port'] = ''; // Falls der Webserver nicht über den Standardport erreichbar ist, tragen Sie den Port hier ein (z.B. 8080)
$config['ssl_url'] = ''; // Wenn Sie eine seperate URL für SSL verwenden (z.B. bei SSL-Proxies), können Sie diese hier angeben (ohne abschließenden Slash). Sonst lassen Sie den Parameter leer.
$config['ssl_port'] = ''; // Wenn Sie einen Nicht-Standard-Port für SSL verwenden, können Sie hier den separaten SSL-Port. Sonst lassen Sie den Parameter leer.
$config['dev'] = false; // Verwenden Sie diesen Parameter, um in Ihren Erweiterungen/Anpassungen zwischen Entwicklungs- und Live-Server zu unterscheiden.

// Einstellungen für lokalen Entwicklungsserver - Kommentare entfernen und Daten eintragen
/* bot-sleep: start */
#if ($_SERVER['SERVER_ADDR']=='127.0.0.1') {
#	$config['dbms'] = 'mysqli'; // mysql oder mysqli
#	$config['db_hostname'] = 'localhost'; // MySQL Hostname
#	$config['db_username'] = 'mysql_username'; // MySQL Username
#	$config['db_password'] = 'mysql_passwort'; // MySQL Passwort
#	$config['db_name'] = 'mysql_db_name'; // MySQL Datenbank-Name
#	$config['db_port'] = '3306'; // MySQL Port, meist 3306
#	$config['db_prefix'] = 'cms_'; // Standard ist cms_ - bei DB-Benutzung durch verschiedene CMS bzw. Applikationen kann der DB Prefix geändert werden.
#	$config['subdir'] = ''; // Standard ist kein Unterverzeichnis (Variable leer lassen).
#	$config['server_prefix'] = ''; // Wenn Sie einen Prefix (z.B. www.) vor dem Servernamen ($_SERVER['SERVER_NAME']) verwenden wollen (ist z.B. aus SEO-Gründen oft nützlich)
#	$config['http_port'] = ''; // Falls der Webserver nicht über den Standardport erreichbar ist, tragen Sie den Port hier ein (z.B. 8080)
#	$config['ssl_url'] = ''; // Wenn Sie eine seperate URL für SSL verwenden (z.B. bei SSL-Proxies), können Sie diese hier angeben (ohne abschließenden Slash). Sonst lassen Sie den Parameter leer.
#	$config['ssl_port'] = ''; // Wenn Sie eine seperate URL für SSL verwenden (z.B. bei SSL-Proxies), können Sie diese hier angeben (ohne abschließenden Slash). Sonst lassen Sie den Parameter leer.
#	$config['dev'] = true; // Verwenden Sie diesen Parameter, um in Ihren Erweiterungen/Anpassungen zwischen Entwicklungs- und Live-Server zu unterscheiden.
#}
/* bot-sleep: stop */

// Zeitzonen Einstellung
// PHP 5.3 erwartet eine Zeitzonen Einstellung auf dem Server
// Diese Variable kann verwendet werden, um die Zeitzone einzustellen
// Hilfe und mögliche Werte können unter folgender URL gefunden werden:
// http://www.php.net/manual/en/timezones.php
// Wenn dieses Feld leer ist, wird keine Zeitzone gesetzt
$config['timezone'] = 'Europe/Berlin';

// PHP Memory Limit
// Wenn es Ihr Server erlaubt, können Sie hier das PHP Memory Limit anpassen
$config['php_memory_limit'] = '';

// Ausgabe Komprimierung
// Hier können Sie die CMSms Ausgabe-Komprimierung aktivieren.
// Dies ist nur sinnvoll, wenn Ihr Webserver nicht bereits den mod_deflate Parameter
// aktiviert hat.
$config['output_compression'] = false;

// Permanente Datenbank-Verbindung
// Wenn Sie die permamente Datenbank-Verbindung aktivieren, können Sie eine höhere
// Datenbank-Zugriffsgeschwindigkeit erzielen.
// Jedoch erlauben nicht alle Datenbank-Hosts diese Einstellung, zudem ist oft die
// maximale Anzahl an gleichzeitigen Datenbankverbindungen beschränkt, so dass es
// bei Aktivierung auch hier zu Problemen kommen kann.
$config['persistent_db_conn'] = true;

// Maximale Größe für Uploads
// Geben Sie an, wie groß Uploads maximal sein dürfen (in Bytes).
// Beachten Sie, dass dieser Wert auch durch die Einstellung in der php.ini begrenzt ist.
$config['max_upload_size'] = 32000000;

// Rechte für Uploads
// Geben Sie hier an, mit welchen Rechten Uploads gespeichert werden sollen.
$config['default_upload_permission'] = '664';

/* bot-sleep: start */

// Document Root
// Die "Root"-URL vom Webserver aus gesehen.
// Beachten Sie, dass die URL ohne endenen Slash (Schrägstrich) angegeben wedren muss.
// Beispiel: http://www.meine-domain.de
// Beispiel: http://www.meine-domain.de/unterverzeichnis
// Im folgenden Fall wird die Einstellung automatisch ermittelt. Verändern Sie diese Einstellung
// nur, wenn Sie wissen was Sie tun.

#if ($_SERVER['SERVER_PORT']==443 && $config['https_url']) { $config['root_url'] = $config['https_url']; } // Seperate SSL-URL benutzen
#if ($config['ssl_url']) { $config['https_url'] = $config['ssl_url']; }
#else {
// Normale URL- bzw. SSL-URL zusammenbauen
	$http_port = $config['http_port'] ? ':'.preg_replace('/[^0-9]/','',$config['http_port']) : '';
	$config['root_url'] = 'http';
	#if($_SERVER['SERVER_PORT']==443 || ($config['ssl_port'] && $_SERVER['SERVER_PORT']==$config['ssl_port'])) { // Abfrage, ob eine HTTPS-Verbindung angefragt wurde
	$https = isset($_SERVER['HTTPS']) ? strtolower($_SERVER['HTTPS']) : '';
	if ($https == 'on') { // Abfrage, ob eine HTTPS-Verbindung angefragt wurde
		$config['root_url'].= 's';
		$http_port = $config['ssl_port'] ? $config['ssl_port'] : '';
	}
	if ($config['server_prefix'] && substr($_SERVER['SERVER_NAME'],0,strlen($config['server_prefix']))==$config['server_prefix']) { $config['server_prefix'] = ''; }
	if (trim($config['subdir'])) {
		$config['subdir'] = str_replace ('\\','/',trim($config['subdir']));
		while (substr($config['subdir'],0,1) == '/') { $config['subdir'] = substr($config['subdir'],1); }
		while (substr($config['subdir'],-1) == '/') { $config['subdir'] = substr($config['subdir'],0,-1); }
		while (strpos($config['subdir'],'..')) { $config['subdir'] = str_replace('..','.',$config['subdir']); }
	}
	$http_host = $http_port ? $config['server_prefix'].$_SERVER['SERVER_NAME'].':'.$http_port : $config['server_prefix'].$_SERVER['SERVER_NAME'];
	$config['root_url'].= $config['subdir'] ? '://'.$http_host.'/'.$config['subdir'] : '://'.$http_host;
	if ($config['ssl_url'] && $https == 'on') { $config['root_url'] = $config['subdir'] ? $config['ssl_url'].'/'.$config['subdir'] : $config['ssl_url']; }
	if (!$config['ssl_url'] && $config['ssl_port']) {
		$config['ssl_url'] = 'https://'.$config['server_prefix'].$_SERVER['SERVER_NAME'].':'.$config['ssl_port'];
		if ($config['subdir']) $config['ssl_url'].= '/'.$config['subdir'];
	}
#}

// Pfadangabe
// Die absolute Pfadangabe, wo CMSms auf dem Server installiert wurde.
// Beachten Sie, dass der Pfad ohne endenen Slash (Schrägstrich) angegeben werden muss.
// Beispiel: /var/www/localhost
// Im folgenden Fall wird die Einstellung automatisch ermittelt. Verändern Sie diese Einstellung
// nur, wenn Sie wissen was Sie tun.
$config['root_path'] = $config['subdir'] ? $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.$config['subdir'] : $_SERVER['DOCUMENT_ROOT'];

// Verzeichnis für Cache
// Hier können Sie angeben, wo Cache-Dateien gespeichert werden sollen.
$config['previews_path'] = $config['root_path'].DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'cache';

// Verzeichnis für Uploads
// Hier können Sie angeben, wo Upload-Dateien gespeichert werden sollen.
$config['uploads_path'] = $config['root_path'].DIRECTORY_SEPARATOR.'uploads';

// URL für Uploads
// Geben Sie hier die URL an, unter welcher die Uploads von außen erreichbar sind.
$config['uploads_url'] = $config['root_url'].'/uploads';

// Verzeichnis für Bilder-Uploads
// Hier können Sie angeben, wo Bilder-Upload-Dateien gespeichert werden sollen.
$config['image_uploads_path'] = $config['root_path'].DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'images';

// URL für Bilder-Uploads
// Geben Sie hier die URL an, unter welcher die Bilder-Uploads von außen erreichbar sind.
$config['image_uploads_url'] = $config['root_url'].'/uploads/images';

// Debug Modus
// Hier können Sie den CMSms Debug-Modus einschalten, um PHP Warningen, Fehler,
// etc zu sehen. CSS Caching wird deaktiviert und es werden noch eine Reihe an
// Zusatz-Informationen ausgegeben.
if (isset($_GET['debug']) and $_GET['debug'] && $_SESSION['cms_admin_username']) { $config['debug'] = true; } else { $config['debug'] = false; }
#$config['debug'] = true;

/* bot-sleep: stop */

// Backend-Verzeichnis
// Name des Backend-Verzeichnis.
// Standard ist admin
$config['admin_dir'] = 'admin';

// Automatischer Alias
// Wenn dieser Parameter aktiviert ist, werden Seiten Aliase automatisch aus dem Seitentitel erzeugt.
$config['auto_alias_content'] = true;

// Sprechende, suchmaschinenfreundliche URLs (Pretty URLs)
// Aktivieren Sie diesen Parameter, um suchmaschinenfreundliche (sprechende) URLs zu benutzen.
// Dieser Parameter bietet 3 Einstellungen:
// 'none' - Pretty URLs sind deaktiviert
// 'mod_rewrite' - Pretty URLs sind aktiviert über das Apache Modul mod_rewrite (Datei .htaccess wird im CMSms Root Verzeichnis benötigt, weiterhin muss der Platzhalter {metadata} in den Seitentemplates vorhanden sein)
// 'internal' - Pretty URLs sind aktiviert über die CMSms-eigene Methode (funktioniert nicht mit IIS und einigen CGI Konfigurationen)
// Beachten Sie, dass Sie dann ebenfalls 'use_hierarchy' aktivieren müssen, damit Module weiter funktionieren.
// Weiterführende Informationen gibt es hier: http://wiki.cmsmadesimple.org/index.php/FAQ/Installation/Pretty_URLs#Pretty_URL.27s
$config['url_rewriting'] = 'mod_rewrite';

// Sollen sprechende URL's im Menü angezeigt werden?
// Funktioniert nur, wenn use_hierarchy auf true steht ...
$config['assume_mod_rewrite'] = true;

// Aktivieren, wenn CMSms (und nicht der Webserver/.htaccess) die sprechenden URLs handeln soll
// Standard ist false
$config['internal_pretty_urls'] = false;

// URL Hierarchie benutzen
// Wenn Sie suchmaschinenfreundliche (sprechende) URLs aktiviert haben, sollten Sie
// diesen Parameter ebenfalls aktivieren, um die Hierarchie in der URL anzuzeigen.
// Beispiel Hierarchie-URL: http://www.meine-seite.de/elternseite/elternseite/unterseite
$config['use_hierarchy'] = true;

// Datei-Erweiterung für mod_rewrite
// Mit diesem Parameter können Sie eine Dateiendung für sprechende URLs aktivieren,
// z.B. '.html'. Dann wird aus http://meine-seite.de/unterseite
// http://www.meine-seite.de/unterseite.html
$config['page_extension'] = '/';

// URL Query Variable
// Wenn Sie keine sprechenden URLs verwenden, können Sie hier den Variablen-Namen
// für die URL Query festlegen.
// (z.B. ttp://www.meine-seite.de/index.php?page=irgendeineseite
// Standard ist page
$config['query_var'] = 'page';

// Programm für Bildbearbeitung
// Legen Sie hier fest, welches Programm zur Thumbnail-Erzeugung und Bildbearbeitung
// verwendet werden soll.
// Siehe http://wiki.cmsmadesimple.org/index.php/User_Handbook/Admin_Panel/Content/Image_Manager für weitere Informationen.
$config['image_manipulation_prog'] = 'GD';
$config['image_transform_lib_path'] = '/usr/bin/ImageMagick/';

// Spracheinstellung
// Hier können Sie die Spacheinstellung vornehmen.
// Lassen Sie diese Variable leer, wenn Sie die Einstellung des Webservers übernehmen möchten.
// Standard ist ''.
// Für Deutsch eignet sich meist 'de_DE.UTF8'
$config['locale'] = 'de_DE.UTF8';

// Zeichenkodierung
// Hier können Sie die Zeichenkodierung angeben.
// Lassen Sie diese Variable leer, wird utf-8 verwendet.
// Wenn Sie hier Änderungen vornehmen, sollten Sie stets beide Parameter ändern.
$config['default_encoding'] = 'utf-8';
$config['admin_encoding'] = 'utf-8';

// Datenbank als UTF-8 behandeln
$config['set_names'] = true;

// Kompatibilitäts-Modus aktivieren, um CMSms kompatibel für Module zu machen, die vor CMSms Version 1.0 entwickelt sind.
// Achtung: Verbraucht sehr viel Speicher und die Kompatibilität ist nicht garantiert!
// Standard ist false
$config['backwards_compatible'] = false;

// Wird nicht mehr benötigt
// Standard ist false
$config['disable_htmlarea_translation'] = false;

// Wird nicht mehr benötigt
// Standard ist true
$config['use_Indite'] = true;

// Wenn Du die "alte" Stylesheet Logik benötigst, kannst Du diesen Parameter auf true setzen.
// Könnte auch von älteren Modulen benötigt werden.
// Besser ist es aber, diesen Parameter auf false zu lassen ;)
// Standard ist false
$config['old_stylesheet'] = false;

// URL der Backend-Hilfe im Benutzer-Handbuch
$config['wiki_url'] = 'http://wiki.cmsmadesimple.org/index.php/User_Handbook/Admin_Panel';

// EOF