<?php

// CMS made simple Config File
// Version 2.1 (from CMSms 1.11.x)
// English, improved Version
// Modified by: Andi Petzoldt, https://plus.google.com/113580733838250075244

// Hinweis: bot-sleep und bot-command sind Anweisungen für Scripte - bitte nicht löschen oder verändern
/* bot-command: version='2.1' */

// Settings for Live-Server
$config['dbms'] = 'mysqli'; // mysql oder mysqli
$config['db_hostname'] = 'localhost'; // MySQL Hostname
$config['db_username'] = 'root'; // MySQL Username
$config['db_password'] = 'root'; // MySQL Password
$config['db_name'] = 'cms'; // MySQL Database-Name
$config['db_port'] = '3306'; // MySQL Port, mostly 3306, some Provider are using 3307 for MySQL 5 Databases
$config['db_prefix'] = 'cms_'; // Default value is cms_ - change it if you use more than one CMSms installations within one database
$config['subdir'] = ''; // Default is no subdirectory (leave empty)
$config['server_prefix'] = ''; // If you want to use a prefix (e.g. www.)
$config['http_port'] = ''; // If your Webserver uses a different port than 80, enter it here (e.g. 8080)
$config['ssl_url'] = ''; // If you use a different URL for SSL (e.g. for a SSL-Proxy), enter this URL here
$config['ssl_port'] = ''; // If you use a Non-Standard-Port for SSL, you can enter the port number here
$config['dev'] = false; // This parameter can be used from developers

// Settings for local development server - remove the comments (#) and fill in your data
#if ($_SERVER['SERVER_ADDR']=='127.0.0.1') {
#	$config['dbms'] = 'mysqli'; // mysql oder mysqli
#	$config['db_hostname'] = 'localhost'; // MySQL Hostname
#	$config['db_username'] = 'mysql_username'; // MySQL Username
#	$config['db_password'] = 'mysql_passwort'; // MySQL Password
#	$config['db_name'] = 'mysql_db_name'; // MySQL Database-Name
#	$config['db_port'] = '3306'; // MySQL Port, mostly 3306, some Provider are using 3307 for MySQL 5 Databases
#	$config['db_prefix'] = 'cms_'; // Default value is cms_ - change it if you use more than one CMSms installations within one database
#	$config['subdir'] = ''; // Default is no subdirectory (leave empty)
#	$config['server_prefix'] = ''; // If you want to use a prefix (e.g. www.)
#	$config['http_port'] = ''; // If your Webserver uses a different port than 80, enter it here (e.g. 8080)
#	$config['ssl_url'] = ''; // If you use a different URL for SSL (e.g. for a SSL-Proxy), enter this URL here
#	$config['ssl_port'] = ''; // If you use a Non-Standard-Port for SSL, you can enter the port number here
#	$config['dev'] = false; // This parameter can be used from developers
#}

// Timezone Setting
// PHP 5.3 expects a timezone setting for the server
// This var can be used to set the timezone
// Find help and possible values here:
// http://www.php.net/manual/en/timezones.php
// Leave this field blank to use no timezone
$config['timezone'] = 'Europe/Berlin';

// If you are experiencing propblems with php memory limit errors, then you may
// want to try enabling and/or adjusting this setting.
// Note: Your server may not allow the application to override memory limits.
$config['php_memory_limit'] = '';

// Output compression?
// Turn this on to allow CMS to do output compression
// this is not needed for apache servers that have mod_deflate enabled
// and possibly other servers.  But may provide significant performance
// increases on some sites.  Use caution when using this as there have
// been reports of incompatibilities with some browsers.
$config['output_compression'] = false;

// Use persistent connections?  They're generally faster, but not all hosts
// allow them.
$config['persistent_db_conn'] = true;

// Maxium upload size (in bytes)?
// Notice: This value could be limited in your php.ini
$config['max_upload_size'] = 32000000;

// Permissions for uploaded files.  This only really needs changing if your
// host has a weird permissions scheme.
$config['default_upload_permission'] = '664';

/* bot-sleep: start */

// Document Root
// The "Root"-URL seen of the point of the webserver.
// Note, that the URL has been written here without the Slash at the end.
// Example: http://www.meine-domain.de
// Example: http://www.meine-domain.de/unterverzeichnis
// In the following case the settings are analyzed automatically. Do not change this settings
// if you not know what you do!

#if ($_SERVER['SERVER_PORT']==443 && $config['https_url']) { $config['root_url'] = $config['https_url']; } // Seperate SSL-URL benutzen
#if ($config['ssl_url']) { $config['https_url'] = $config['ssl_url']; }
#else {
// Normal build of URL- or SSL-URL
	$http_port = $config['http_port'] ? ':'.preg_replace('/[^0-9]/','',$config['http_port']) : '';
	$config['root_url'] = 'http';
	#if($_SERVER['SERVER_PORT']==443 || ($config['ssl_port'] && $_SERVER['SERVER_PORT']==$config['ssl_port'])) { // Abfrage, ob eine HTTPS-Verbindung angefragt wurde
	$https = isset($_SERVER['HTTPS']) ? strtolower($_SERVER['HTTPS']) : '';
	if ($https == 'on') { // Abfrage, ob eine HTTPS-Verbindung angefragt wurde
		$config['root_url'].= 's';
		$http_port = $config['ssl_port'] ? $config['ssl_port'] : '';
	}
	if ($config['server_prefix'] && substr($_SERVER['SERVER_NAME'],0,strlen($config['server_prefix']))==$config['server_prefix']) { $config['server_prefix'] = ''; }
	if (trim($config['subdir'])) {
		$config['subdir'] = str_replace ('\\','/',trim($config['subdir']));
		while (substr($config['subdir'],0,1) == '/') { $config['subdir'] = substr($config['subdir'],1); }
		while (substr($config['subdir'],-1) == '/') { $config['subdir'] = substr($config['subdir'],0,-1); }
		while (strpos($config['subdir'],'..')) { $config['subdir'] = str_replace('..','.',$config['subdir']); }
	}
	$http_host = $http_port ? $config['server_prefix'].$_SERVER['SERVER_NAME'].':'.$http_port : $config['server_prefix'].$_SERVER['SERVER_NAME'];
	$config['root_url'].= $config['subdir'] ? '://'.$http_host.'/'.$config['subdir'] : '://'.$http_host;
	if ($config['ssl_url'] && $https == 'on') { $config['root_url'] = $config['subdir'] ? $config['ssl_url'].'/'.$config['subdir'] : $config['ssl_url']; }
	if (!$config['ssl_url'] && $config['ssl_port']) {
		$config['ssl_url'] = 'https://'.$config['server_prefix'].$_SERVER['SERVER_NAME'].':'.$config['ssl_port'];
		if ($config['subdir']) $config['ssl_url'].= '/'.$config['subdir'];
	}
#}

// Path to document root. This should be the directory this file is in.
// e.g. /var/www/localhost
// In the following line we try to get your settings automatically.
// Do only change if you know, what you do!
$config['root_path'] = $config['subdir'] ? $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.$config['subdir'] : $_SERVER['DOCUMENT_ROOT'];

// Where do previews get stored temporarily?  It defaults to tmp/cache.
$config['previews_path'] = $config['root_path'].DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'cache';

// Where are uploaded files put?  This defaults to uploads.
$config['uploads_path'] = $config['root_path'].DIRECTORY_SEPARATOR.'uploads';

// Where is the url to this uploads directory?
$config['uploads_url'] = $config['root_url'].'/uploads';

// Default path and URL for uploaded images in the image manager
$config['image_uploads_path'] = $config['root_path'].DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'images';
$config['image_uploads_url'] = $config['root_url'].'/uploads/images';

// CMSMS Debug Mode?  Turn it on to get a better error when you
// see {nocache} errors, or to allow seeing php notices, warnings, and errors in the html output.
// This setting will also disable browser css caching.
if (isset($_GET['debug']) and $_GET['debug'] && $_SESSION['cms_admin_username']) { $config['debug'] = true; } else { $config['debug'] = false; }
#$config['debug'] = true;

/* bot-sleep: stop */

// Name of the admin directory
// Default is admin
$config['admin_dir'] = 'admin';

// Automatically assign alias based on page title?
$config['auto_alias_content'] = true;

// User-friendly URLs (Pretty URLs)
// Activate this parameter to use user-friendly URLs.
// This parameter has 3 setting possibilities:
// 'none' - Pretty URLs are de-activated
// 'mod_rewrite' - Pretty URLs are activatet using the Apache Module mod_rewrite (file .htaccess is needed in CMSms Root Directory, additionally the placeholder {metadata} is needed in the page templates).
// 'internal' - Pretty URLs are activated using the CMSms-internal function (will not work with IIS and some CGI configurations)
// Find more information here: http://wiki.cmsmadesimple.org/index.php/FAQ/Installation/Pretty_URLs#Pretty_URL.27s
$config['url_rewriting'] = 'mod_rewrite';

// Show mod_rewrite URLs in the menu? You must enable 'use_hierarchy' for this to work for modules
$config['assume_mod_rewrite'] = true;

// If you don't use mod_rewrite, then would you like to use the built-in
// pretty url mechanism? This will not work with IIS and the {metadata} tag
// should be in all of your templates before enabling.
// Standard is false
$config['internal_pretty_urls'] = false;

// If you're using the internal pretty url mechanism or mod_rewrite, would you like to
// show urls in their hierarchy?  (ex. http://www.mysite.com/parent/parent/childpage)
$config['use_hierarchy'] = true;

// Extension to use if you're using mod_rewrite for pretty URLs.
$config['page_extension'] = '/';

// If using none of the above options, what should we be using for the query string
// variable?  (ex. http://www.mysite.com/index.php?page=somecontent)
$config['query_var'] = 'page';

// Which program should be used for handling thumbnails in the image manager.
// See http://wiki.cmsmadesimple.org/index.php/User_Handbook/Admin_Panel/Content/Image_Manager for more
// info on what this all means
$config['image_manipulation_prog'] = 'GD';
$config['image_transform_lib_path'] = '/usr/bin/ImageMagick/';

// Locale to use for various default date handling functions, etc.  Leaving
// this blank will use the server's default.  This might not be good if the
// site is hosted in a different country than it's intended audience.
// Default is ''.
// For German you should use 'de_DE.UTF8'
$config['locale'] = 'de_DE.UTF8';

// In almost all cases, default_encoding should be empty (which defaults to utf-8)
// and admin_encoding should be utf-8.  If you'd like this to be different, change
// both.  Keep in mind, however, that the admin interface translations are all in
// utf-8, and will be converted on the fly to match the admin_encoding.  This
// could seriously slow down the admin interfaces for users.
$config['default_encoding'] = 'utf-8';
$config['admin_encoding'] = 'utf-8';

// Use Database as UTF-8
$config['set_names'] = true;

// Enable backwards compatibility mode? This basically will allow some
// modules written before 1.0 was released to work. Keep in mind that this
// will use a lot more memory and isn't guaranteed to fix the problem.
// Standard ist false
$config['backwards_compatible'] = false;

// Not used anymore
// Standard is false
$config['disable_htmlarea_translation'] = false;

// Wird nicht mehr benötigt
// Standard is true
$config['use_Indite'] = true;

// Use the old stylesheet logic? It's much slower, but it works with older
// versions of CMSMS. You'll also need this set to true if there is a module
// that uses a stylesheet callback. Leave it as false instead you really
// need it.
// Standard ist false
$config['old_stylesheet'] = false;

// URL of the Admin Panel section of the User Handbook
$config['wiki_url'] = 'http://wiki.cmsmadesimple.org/index.php/User_Handbook/Admin_Panel';

// EOF