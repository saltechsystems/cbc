{$formstart}
{$tabto}
<div class="pageoverflow">
	<p class="pagetext">{$prompt_dont_use_event_handler}:</p>
	<p class="pageinput">{$input_dont_use_event_handler}<br />{$help_dont_use_event_handler}</p>
</div>
<div class="pageoverflow">
	<p class="pagetext">{$prompt_clear_cache}:</p>
	<p class="pageinput">{$input_clear_cache}<br />{$help_clear_cache}</p>
</div>
<div class="pageoverflow">
	<p class="pagetext">{$prompt_system_check}:</p>
	<p class="pageinput">{$input_system_check}</p>
</div>
<div class="pageoverflow">
	<p class="pagetext">{$prompt_debug_use}:</p>
	<p class="pageinput">{$input_debug_use}</p>
</div>
<div class="pageoverflow">
	<p class="pagetext">{$prompt_stats_use}:</p>
	<p class="pageinput">{$input_stats_use}</p>
</div>
<div class="pageoverflow">
	<p class="pagetext">{$prompt_stats_daily}:</p>
	<p class="pageinput">{$input_stats_daily}</p>
</div>
<div class="pageoverflow">
	<p class="pagetext">{$prompt_stats_email}:</p>
	<p class="pageinput">{$input_stats_email}<br />{$help_stats_email}</p>
</div>
<div class="pageoverflow">
	<p class="pagetext">{$prompt_devdomain}:</p>
	<p class="pageinput">{$input_devdomain}</p>
</div>
<div class="pageoverflow">
	<p class="pagetext">{$prompt_devip}:</p>
	<p class="pageinput">{$input_devip}</p>
</div>
<div class="pageoverflow">
    <p class="pagetext">&nbsp;</p>
    <p class="pageinput">{$submit}</p>
</div>
{$formend}