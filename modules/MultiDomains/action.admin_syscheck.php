<?php
/*======================================================================================
Module: MultiDomains
======================================================================================*/

// Check authorisation
if(!is_object(cmsms())) exit;
if ( !$this->CheckPermission('Manage MultiDomains') ) exit;

// Set variables
$tabto = preg_replace('/[^0-9a-zA-Z_]/','',$params['tabto']);
$ctodo = preg_replace('/[^0-9a-zA-Z_]/','',$params['config_todo']);
$myconfig = cmsms()->GetConfig();
$cfile = strpos($myconfig['locale'],'de')!==false ? 'config-de.php' : 'config-en.php';
$cnotchange = array('url_rewriting','process_whole_template','https_url','root_url','root_path','previews_path','uploads_path','uploads_url','image_uploads_path','image_uploads_url','debug');
$message = '';

// config.php Vorlage und Original in Arrays einlesen
$ctemplate = @file($myconfig['root_path'].'/modules/MultiDomains/templates/'.$cfile);
$cfile = @file($myconfig['root_path'].'/config.php');

// Variablen aus original config.php auslesen
$ovalues = array();
$botstop = false;
foreach($cfile as $linenumber=>$data) {
	// Test, ob Bot eine Pause machen soll ;)
	if (strpos($data,'bot-sleep: start')!==false) $botstop = true;
	if (strpos($data,'bot-sleep: stop')!==false) $botstop = false;
	if ($botstop) continue;
	// Zeile aufteilen
	$line = explode('=',trim($data));
	$key = '';
	$k = trim(array_shift($line));
	if (substr($k,0,7)=='$config') $key = trim(preg_replace('/[\[\]\'\"]/','',substr($k,7)));
	if ($key && !in_array($key,$cnotchange)) {
		$v = trim(implode('=',$line));
		$temp = explode(';',$v);
		$value = trim($temp[0]);
		$ovalues[$key] = $value;
	}
}

// Variablen in der Vorlage anpassen
$botstop = false;
foreach($ctemplate as $linenumber=>$data) {
	// Zeilenumbruch l�schen
	$data = preg_replace('/[\n\r]/','',$data);
	$ctemplate[$linenumber] = $data;
	// Test, ob Bot eine Pause machen soll ;)
	if (strpos($data,'bot-sleep: start')!==false) $botstop = true;
	if (strpos($data,'bot-sleep: stop')!==false) $botstop = false;
	if ($botstop) continue;
	// Zeile aufteilen
	$line = explode('=',trim($data));
	$key = '';
	$k = trim(array_shift($line));
	if (substr($k,0,7)=='$config') $key = trim(preg_replace('/[\[\]\'\"]/','',substr($k,7)));
	if ($key && !in_array($key,$cnotchange) && array_key_exists($key,$ovalues)) {
		$l = explode(';',implode('=',$line));
		array_shift($l);
		$ctemplate[$linenumber] = '$config[\''.$key.'\'] = '.$ovalues[$key].';'.implode(';',$l);
	}
}

// Write modified config.php
if ($ctodo == 'modify') {
	// Backup der alten config.php machen
	$backup = 'config_backup.'.time().'.php';
	if (!copy($myconfig['root_path'].'/config.php', $myconfig['root_path'].'/'.$backup)) {
		$message.= $this->Lang('config_backup_fail');
		$ctodo == 'download';
	// Neue config.php schreiben
	} else {
		$newconfig = trim(implode("\n",$ctemplate));
		if (file_put_contents($myconfig['root_path'].'/config.php', $newconfig)) {
			$message.= $this->Lang('config_written',$backup);
		} else {
			$message.= $this->Lang('config_written_fail');
			$ctodo == 'download';
		}
	}
}

// Download modified config.php
if ($ctodo == 'download') {
	$test = ob_get_flush();
	ob_end_clean();
	$newconfig = trim(implode("\n",$ctemplate));
	if(ini_get('zlib.output_compression')) ini_set('zlib.output_compression', 'Off');
	header('Pragma: public');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Cache-Control: private',false);
	header('Content-Type: application/force-download');
	header('Content-Disposition: attachment; filename="config.php";');
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: '.strlen($newconfig));
	header('Content-Description: File Transfer');
	echo $newconfig;
	exit;
}

// Redirect
$this->Redirect($id, 'defaultadmin', $returnid, array( 'tabto'=>$tabto, 'message'=>$message ));

// EOF