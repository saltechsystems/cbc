<?php
/*======================================================================================
Module: MultiDomains
======================================================================================*/

// Check authorisation
if(!is_object(cmsms())) exit;
if ( !$this->CheckPermission('Manage MultiDomains') ) exit;

// Get POST data
$tabto = preg_replace('/[^0-9a-zA-Z_]/','',$params['tabto']);

// Get db instance
$db = cmsms()->GetDb();
// Delete the debug
$sql = 'DELETE FROM '.cms_db_prefix().'module_multidomains_debug';
$res = $db->Execute($sql,array());

// Redirect
$this->Redirect($id, 'defaultadmin', $returnid, array( 'tabto'=>$tabto ));

// EOF