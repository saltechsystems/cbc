<?php
/*======================================================================================
Module: MultiDomains
======================================================================================*/

// Check authorisation
if(!is_object(cmsms())) exit;

// Get DB instance and make DB settings
$db = cmsms()->GetDb();
$dict = NewDataDictionary($db);

// Delete db tables
$sqlarray = $dict->DropTableSQL( cms_db_prefix().'module_multidomains' );
$dict->ExecuteSQLArray($sqlarray);
$sqlarray = $dict->DropTableSQL( cms_db_prefix().'module_multidomains_extradomains' );
$dict->ExecuteSQLArray($sqlarray);
$sqlarray = $dict->DropTableSQL( cms_db_prefix().'module_multidomains_log' );
$dict->ExecuteSQLArray($sqlarray);
$sqlarray = $dict->DropTableSQL( cms_db_prefix().'module_multidomains_logsum' );
$dict->ExecuteSQLArray($sqlarray);
$sqlarray = $dict->DropTableSQL( cms_db_prefix().'module_multidomains_debug' );
$dict->ExecuteSQLArray($sqlarray);

// Remove module permissions
$this->RemovePermission('Manage MultiDomains');

// Remove Events
$this->RemoveEventHandler('Core','ContentPostRender');

// Remove module preferences
$this->RemovePreference();

// EOF