<?php
if (!isset($gCms)) exit;

/* Display an event details */

// Some default values
$inline = 0;
if ( (isset($params['inline'])) AND ($params['inline'] != '') )
	$inline = $params['inline'];
	
// Detail page ID ?
$detailpage = '';
if (isset($params['detailpage']) and !empty($params['detailpage']))
	$detailpage = $params['detailpage'];
else
{
	$detailpage = $this->GetPreference('dflt_detailpage');
	if ($detailpage == '-1')
		$detailpage = $returnid;
}

// Registration page ID ?
$registrationpage = '';
if (isset($params['registrationpage']) and !empty($params['registrationpage']))
	$registrationpage = $params['registrationpage'];
else
{
	$registrationpage = $this->GetPreference('dflt_registrationpage', '-1');
	if ($registrationpage == '-1')
		$registrationpage = $returnid;
}
	
// Manage the event
$event = new EventsManager_event($params['event_id']);
$event_smarty = $event->get_smarty_object();

// Add the registration link
if ($event->allow_registration)
{
	$aliased_title = munge_string_to_url($event->name);
	
	$urlprefix = $this->GetPrefix('events');
	$registrationurlprefix = $this->GetPrefix('register');

	$prettyurl = $urlprefix.'/'.$registrationurlprefix.'/'.$event->id.'/'.$registrationpage.'/'.$aliased_title;
	
	$event_smarty->registration_url = $this->CreateFrontendLink($id, $registrationpage, 'register', '', array('event_id'=>$event->id), '', 1, $inline, '', '', $prettyurl);
}

// Add custom fields
$FieldsManager = new AireFieldsManager();
$tmp = $FieldsManager->get_fieldsbyname('EventsManager', $event->id, '', '', '');
if ($tmp)
	$event_smarty->fields = $tmp;

/******************************************************************************
// Display */

// Smarty
$smarty->assign('entry', $event_smarty);

// Display an overquota message ?
if (isset($params['overquotanb']))
{
	$smarty->assign('overquotanb', $params['overquotanb']);
}

if (isset($params['overquotatotal']))
{
	// Case where the overquota is because of the total number of reg users, and not the feu limit
	$smarty->assign('overquotatotal', $params['overquotatotal']);
}

// Display
$template = $this->GetPreference('dflt_detail_template');
if  (!empty($params['detailtemplate']))
	$template = $params['detailtemplate'];
echo $this->ProcessTemplateFromDatabase('detail_' . $template);
//echo $this->ProcessTemplate('orig_detailtemplate.tpl');
?>