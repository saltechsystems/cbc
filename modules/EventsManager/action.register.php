<?php
if (!isset($gCms)) exit;

/* Registration form */
$errors = array();
$messages = array();

// A variable to see if we do display the registration form
$display_event_registration_form = false;

// Some default values
$inline = 0;
if ( (isset($params['inline'])) AND ($params['inline'] != '') )
	$inline = $params['inline'];

$allow_notconnected=0;
if (isset($params['allow_notconnected']) && ($params['allow_notconnected'] == 1))
	$allow_notconnected=1;

// if (!isset($params['redirectdetail']))
// 	$smarty->assign('overquotanb', 0);

// Load the event and see if registration is allowed
if (isset($params['event_id']))
{
	// Load the event and registration
	$event = new EventsManager_event($params['event_id']);

	$feu_module = cms_utils::get_module('FrontEndUsers');
	$id_user = $feu_module->LoggedInId();
	$registration = new EventsManager_registration(false, true, $event->id, $id_user);
	
	// Submit ?
	if (isset($params['submit']) OR isset($params['cancel']))
	{
		$continue_registration=true;
		$overquota = false;
		
		if (isset($params['submit']))
		{
			// Check the number of persons asked - If the param is not provided, we consider only 1 person
			if (isset($params['nb_persons']))
				$nb_persons = $params['nb_persons'];
			else
				$nb_persons = 1;
			
			// Nb max ? 
			if ( ($event->capacity_per_feu != 0) AND ($nb_persons > $event->capacity_per_feu) )
			{
				// We ask for an over quota - we have to see what to do (see the overquota_behaviour pref)
				if ($this->GetPreference('overquota_behaviour') == 'reg_with_limit')
					$nb_persons = $event->capacity_per_feu;
				else
				{
					// Show a message and the input form again to ask confirmation
					$continue_registration=false;
					$overquota = true;
				}
			}
		}
		else
		{
			// Cancel registration
			$nb_persons = 0;
		}

		// We have to check the case where the limit per FEU is ok, but the total number of registrations asked is higher than the remaining places
		if ($continue_registration and $nb_persons != 0)
		{
			if (!$event->registration_allowed($nb_persons, $registration->nb_persons))
			{

				$regusers = $event->get_regusers_number();
				$overquotatotal = $event->capacity - $regusers;
				
				$smarty->assign('overquotanb', $nb_persons);
				$smarty->assign('overquotatotal', $overquotatotal);

				if (isset($params['redirectdetail']) && ($params['redirectdetail']))
				{
					// We go back to the orig detail url
					$this->RedirectForFrontEnd($id, $returnid, 'detail', array('event_id'=>$params['event_id'], 'overquotanb'=>$nb_persons, 'redirectdetail'=>1, 'overquotatotal'=>$overquotatotal), 0);
				}
			}
		}

		// id_user if there to verify the user is connected - if not, go to login page
		if ($continue_registration && $id_user && ($event->registration_allowed($nb_persons, $registration->nb_persons)))
		{
			$registration->nb_persons = $nb_persons;
			
			// Ready - Just have to store the actual id to see if it's an addition or update
			$id_before_addedit = $registration->id;
			
			if ($registration->addedit())
			{
				$messages[] = $this->Lang('message_registration_ok');
				
				// Send events
				$parms['registration_id'] = $registration->id;
				
				if ($id_before_addedit)
					$this->SendEvent('EventsManagerRegistrationEdited', $parms);
				else
					$this->SendEvent('EventsManagerRegistrationAdded', $parms);

				// Send e-mails if necessary
				$registration->send_mail_alerts();
				
				// Redirect to originating url
				if (isset($params['redirectdetail']))
				{
					$this->RedirectForFrontEnd($id, $returnid, 'detail', array('event_id'=>$params['event_id'], 'redirectdetail'=>1), 0);
				}
				else
				{
					$url = html_entity_decode($params['orig_url']);
					redirect($url);
				}
			}
			else
				$errors[] = $this->Lang('error_db');
		}
		elseif ($overquota)
		{
			// We ask for a number of places wich is over quota per user
			// If we are here, it means that we have to display a message
			// The message is handled by smarty to keep customization
			$smarty->assign('overquotanb', $nb_persons);

			if (isset($params['redirectdetail']) && ($params['redirectdetail']))
			{
				// We go back to the orig detail url
				$this->RedirectForFrontEnd($id, $returnid, 'detail', array('event_id'=>$params['event_id'], 'overquotanb'=>$nb_persons, 'redirectdetail'=>1), 0);
			}
		}
		elseif (!$id_user)
		{
			// Save the registration in session // $nb_persons
			$sess = new cge_session($this->GetName());
			$sess->put('event_registration', $event->id);
			$sess->put('nb_persons', $nb_persons);
			
			// Redirect to feu login page
			$loginpage = $this->GetPreference('dflt_feuloginpage');
			if ($loginpage == -1)
				$loginpage=$returnid;
			$this->RedirectContent($loginpage);
		}
		else
		{
			$errors[] = $this->Lang('error_registration_notallowed');
		}
	}
	
	
	// Registration form / Only if we can register to this event OR if the allow_notconnected param value is 1
	if ( $event->registration_allowed() || $allow_notconnected)
	{
		// We allow the registration ONLY if the limited_reg_period is OK
		$reg_period_ok = true;
		if ( ($event->limited_reg_period == 1))
		{
			// We have to check the dates
			$reg_start = new DateTime($event->reg_start_datetime);
			$reg_end = new DateTime($event->reg_end_datetime);
			$now = new DateTime();

			if (($reg_start >= $now) || ($now >= $reg_end))
				$reg_period_ok = false;
		}
		
		if ($reg_period_ok)
		{
			$display_event_registration_form = true;

			// Over quota infos
			if (isset($overquota))
				$smarty->assign('overquota', $overquota);

			// The form
			$smarty->assign('form_start', $this->CreateFormStart($id, 'register', $returnid, 'POST', '', $inline));
			$smarty->assign('form_end', $this->CreateFormEnd());

			$tmp = '';
			if (isset($params['redirectdetail']) && ($params['redirectdetail']))
				$tmp = $this->CreateInputHidden($id, 'redirectdetail', 1);
				
			$smarty->assign('hidden', $this->CreateInputHidden($id, 'event_id', $params['event_id']) . $this->CreateInputHidden($id, 'orig_url', cge_url::current_url()).$tmp);

			// Create a "register" of "modify" button according to the nb of persons of the current registration
			if ($registration->nb_persons > 0)
				$smarty->assign('submit', $this->CreateInputSubmit($id, 'submit', $this->Lang('modify_registration')));
			else
				$smarty->assign('submit', $this->CreateInputSubmit($id, 'submit', $this->Lang('register')));

			// Cancel
			$smarty->assign('cancel', $this->CreateInputSubmit($id, 'cancel', $this->Lang('cancelregister')));

			// Quantity - If we are in overquota, we display the feu max number
			$dflt_nb = 1;
			if (isset($overquotatotal) && $overquotatotal)
				$dflt_nb = $overquotatotal;
			elseif ($smarty->get_template_vars('overquotatotal'))
				$dflt_nb = $smarty->get_template_vars('overquotatotal');
			elseif (isset($overquota) && $overquota)
				$dflt_nb = $event->capacity_per_feu;
			elseif ($smarty->get_template_vars('overquotanb') > 0)
				$dflt_nb = $event->capacity_per_feu;
			elseif ($registration->nb_persons > 0)
				$dflt_nb = $registration->nb_persons;
			$smarty->assign('nb_persons_input', $this->CreateInputText($id, 'nb_persons', $dflt_nb, 4));
			$smarty->assign('nb_persons_label', $this->CreateLabelForInput($id, 'nb_persons', $this->Lang('nb_persons')));
		}
		else
			$errors[] = $this->Lang('error_registration_notallowed_badperiod');
	}
	else
		$errors[] = $this->Lang('error_registration_notallowed');
}
else
	$errors[] = $this->Lang('error_paramsmissing');

/******************************************************************************
// Display */

// Smarty
$smarty->assign('errors', $errors);
$smarty->assign('messages', $messages);
$smarty->assign('display_event_registration_form', $display_event_registration_form);

$event_smarty = $event->get_smarty_object();
$smarty->assign('event', $event_smarty);

$registration_smarty = $registration->get_smarty_object();
$smarty->assign('registration', $registration_smarty);

// Display
$template = $this->GetPreference('dflt_registration_template');
if (!empty($params['registrationtemplate']))
	$template = $params['registrationtemplate'];

echo $this->ProcessTemplateFromDatabase('registration_' . $template);
//echo $this->ProcessTemplate('orig_registertemplate.tpl');
?>