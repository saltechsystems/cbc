<?php
if (!isset($gCms)) exit;
if (!$this->HasPermission()) exit;

if (isset($params['id_event']))
{
	$event = new EventsManager_event($params['id_event']);
	
	$registrations = $event->get_registrations();
	$feu_module = cms_utils::get_module('FrontEndUsers');
	
	$result = '"ID","' . $this->Lang('regdate') . '","' . $this->Lang('feuname') . '","' . $this->Lang('nb_persons') . '"';

	// Get the properties titles
	$pref_displayed_feu_properties = $this->GetPreference('displayed_feu_properties');
	if ($pref_displayed_feu_properties)
		$displayed_feu_properties = explode(',', $pref_displayed_feu_properties);
	else
		$displayed_feu_properties = '';

	// Get the FEU properties to display
	$groups = $feu_module->GetGroupList();

	// For each group, search the properties and remove double properties
	$props_to_display = array();
	foreach ($groups as $group=>$id_group)
	{
		// Property in the group
		$tmp = $feu_module->GetGroupPropertyRelations($id_group);

		foreach ($tmp as $oneproparray)
		{
			// test if the value :
			// is OK for display according to the preferences
			// is NOT in the actual props_to_display res array
			if (in_array($oneproparray['name'], $displayed_feu_properties) && (!in_array($oneproparray['name'], $props_to_display)))
				$props_to_display[] = $oneproparray['name'];
		}
	}
	$count_props = count($props_to_display);
	
	// Feu informations - first line
	foreach($props_to_display as $feuprop)
		$result .= ',"' . $feuprop . '"';
	
	// A registration per line
	foreach ($registrations as &$registration)
	{
		// Test if the nb persons is 0, and if in this case we have to export it
		if (($registration->nb_persons > 0) || ($this->GetPreference('export_cancelled') == 1))
		{
			$registration->username = $feu_module->GetUserName($registration->id_user);
			$tmp_feu_props = $feu_module->GetUserProperties($registration->id_user);

			$res_props = array();
			$invert_props = array_flip($props_to_display);
			foreach ($tmp_feu_props as $oneproparray)
			{
				if (in_array($oneproparray['title'], $props_to_display))
					$res_props[$invert_props[$oneproparray['title']]] = $oneproparray['data'];
			}
			$registration_smarty->feu_props = $res_props;

			$result .= "\n";
			$result .= $registration->id . ',"' . $registration->modify_datetime . '","' . $registration->username . '","' . $registration->nb_persons . '"';

			for ($i=0; $i<=$count_props; $i++)
			{
				$result .= ',"' . $registration_smarty->feu_props[$i] . '"';
			}
		}
	}
	
	$aliased_title = munge_string_to_url($event->name);
	$file_name = date('Y-m-d') . '_' . $aliased_title . '_registrations.csv';
	
	@ob_clean();
	@ob_clean();
	header('Pragma: public');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Cache-Control: private',false);
	header('Content-Description: File Transfer');
	header('Content-Type: text/csv; charset='.$this->GetPreference('exportencoding','utf-8'));
	header('Content-Length: ' . strlen($result));
	header('Content-Disposition: attachment; filename=' . $file_name);
	
	echo $result;
	
	exit;
}

?>