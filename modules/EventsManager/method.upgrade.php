<?php
#-------------------------------------------------------------------------
# Module: EventsManager
# Method: Upgrade
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2008 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
#-------------------------------------------------------------------------
if (!isset($gCms)) exit;

$current_version = $oldversion;
$db = cmsms()->GetDb();

switch($current_version)
{
	// we are now 0.1 and want to upgrade to latest
	case "0.1":
	{
		$this->CreateEvent('EventsManagerEventAdded');
		$this->CreateEvent('EventsManagerEventEdited');
		$this->CreateEvent('EventsManagerEventDeleted');
		
		$this->CreateEvent('EventsManagerCategoryAdded');
		$this->CreateEvent('EventsManagerCategoryEdited');
		$this->CreateEvent('EventsManagerCategoryDeleted');
		
		$this->CreateEvent('EventsManagerRegistrationAdded');
		$this->CreateEvent('EventsManagerRegistrationEdited');
	}
	
	case "0.1.1":
	case "0.1.2":
	case "0.1.3":
	case "0.1.4":
	case "0.1.5":
	case "0.1.6":
	case "0.1.7":
	case "0.1.8":
	case "0.1.9":
	{
		$fn = cms_join_path(dirname(__FILE__),'templates','orig_registrationslisttemplate.tpl');
		if( file_exists( $fn ) )
		{
			$template = file_get_contents( $fn );
			$this->SetPreference('orig_registrationslist_template',$template);
			$this->SetTemplate('registrationslist_Sample',$template);
			$this->SetPreference('dflt_registrationslist_template','Sample');
		}
	}

	case "0.2":
	case "0.2.1":
	case "0.2.2":
	{
		// Add the mails templates and preferences

		// New registration template
		$fn = cms_join_path(dirname(__FILE__),'templates','orig_newregistrationmailtemplate.tpl');
		if( file_exists( $fn ) )
		{
			$template = file_get_contents( $fn );
			$this->SetTemplate('newregistrationmail_Sample',$template);
		}

		$this->SetPreference('newregistration_mail_sendtouser', 0);
		$this->SetPreference('newregistration_mail_sendtoother', 0);
		$this->SetPreference('newregistration_mail_sendtoother_mails', '');
		// Mail subject
		$this->SetPreference('newregistration_mail_subject', 'New registration to the event {$event->name}');

		// Cancel registration template
		$fn = cms_join_path(dirname(__FILE__),'templates','orig_cancelregistrationmailtemplate.tpl');
		if( file_exists( $fn ) )
		{
			$template = file_get_contents( $fn );
			$this->SetTemplate('cancelregistrationmail_Sample',$template);
		}

		$this->SetPreference('cancelregistration_mail_sendtouser', 0);
		$this->SetPreference('cancelregistration_mail_sendtoother', 0);
		$this->SetPreference('cancelregistration_mail_sendtoother_mails', '');
		// Mail subject
		$this->SetPreference('cancelregistration_mail_subject', 'Registration cancelled for the event {$event->name}');
	}

	case "0.3":
	{
		// Add new infos for start and cutoff dates for registrations in DB
		$dict = NewDataDictionary($db);
		$sqlarray = $dict->AddColumnSQL(cms_db_prefix()."module_events", "limited_reg_period I1 DEFAULT '0'");
		$dict->ExecuteSQLArray($sqlarray);
		$sqlarray = $dict->AddColumnSQL(cms_db_prefix()."module_events", "reg_start_datetime ".CMS_ADODB_DT);
		$dict->ExecuteSQLArray($sqlarray);
		$sqlarray = $dict->AddColumnSQL(cms_db_prefix()."module_events", "reg_end_datetime ".CMS_ADODB_DT);
		$dict->ExecuteSQLArray($sqlarray);

		// Feu default login page
		$this->SetPreference('dflt_feuloginpage', '-1');

		// Handle the registration after FEU login
		$this->AddEventHandler('FrontEndUsers', 'OnLogin', true);
	}

	case "1.0":
	{
		// By default, do the registration when a user enters an over-quota number of registrations
		$this->SetPreference('overquota_behaviour', 'reg_with_limit');
		// Export cancelled users in CSV
		$this->SetPreference('export_cancelled', 1);
		// Show tabs in event edit window
		$this->SetPreference('showtabs', 1);

		// Add the new "cancelled" value
		$dict = NewDataDictionary($db);
		$sqlarray = $dict->AddColumnSQL(cms_db_prefix()."module_events", "cancelled I DEFAULT '0'");
		$dict->ExecuteSQLArray($sqlarray);
	}
}

// put mention into the admin log
$this->Audit( 0, $this->Lang('friendlyname'), $this->Lang('upgraded', $this->GetVersion()));
?>