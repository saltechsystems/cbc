<?php
if (!isset($gCms)) exit;
if (!$this->HasPermission()) exit;

if (isset($params['cancel_mails']))
	$this->RedirectToTab($id, 'mails');
elseif (isset($params['submit_mails']))
{
	// Save

	// New registration
	$this->SetPreference('newregistration_mail_sendtouser', (isset($params['newregistration_mail_sendtouser']) ? 1 : 0));
	$this->SetPreference('newregistration_mail_sendtoother', (isset($params['newregistration_mail_sendtoother']) ? 1 : 0));
	$this->SetPreference('newregistration_mail_sendtoother_mails', $params['newregistration_mail_sendtoother_mails']);
	$this->SetPreference('newregistration_mail_subject', $params['newregistration_mail_subject']);

	// Template
	$this->SetTemplate('newregistrationmail_Sample', $params['newregistration_mail_template']);

	// Cancel registration
	$this->SetPreference('cancelregistration_mail_sendtouser', (isset($params['cancelregistration_mail_sendtouser']) ? 1 : 0));
	$this->SetPreference('cancelregistration_mail_sendtoother', (isset($params['cancelregistration_mail_sendtoother']) ? 1 : 0));
	$this->SetPreference('cancelregistration_mail_sendtoother_mails', $params['cancelregistration_mail_sendtoother_mails']);
	$this->SetPreference('cancelregistration_mail_subject', $params['cancelregistration_mail_subject']);

	// Template
	$this->SetTemplate('cancelregistrationmail_Sample', $params['cancelregistration_mail_template']);

	$this->RedirectToTab($id, 'mails');
}

$smarty = cmsms()->GetSmarty();

// Form
$smarty->assign('form_start', $this->CreateFormStart($id, 'defaultadmin'));
$smarty->assign('form_end', $this->CreateFormEnd());
$smarty->assign('submit', $this->CreateInputSubmit($id, 'submit_mails', lang('submit')));
$smarty->assign('cancel', $this->CreateInputSubmit($id, 'cancel_mails', lang('cancel')));

/* Mail on new registration **************************************************/
/* Mail for the user */
$smarty->assign('newregistration_mail_sendtouser_input', $this->CreateInputCheckbox($id, 'newregistration_mail_sendtouser', 1, $this->GetPreference('newregistration_mail_sendtouser', 0), 'id="'.$id.'newregistration_mail_sendtouser"'));
$smarty->assign('newregistration_mail_sendtouser_label', $this->CreateLabelForInput($id, 'newregistration_mail_sendtouser', $this->Lang('mail_sendtouser_label')));

/* Mail for the admin or other */
$smarty->assign('newregistration_mail_sendtoother_input', $this->CreateInputCheckbox($id, 'newregistration_mail_sendtoother', 1, $this->GetPreference('newregistration_mail_sendtoother', 0), 'id="'.$id.'newregistration_mail_sendtoother"'));
$smarty->assign('newregistration_mail_sendtoother_label', $this->CreateLabelForInput($id, 'newregistration_mail_sendtoother', $this->Lang('mail_sendtoother_label')));
$smarty->assign('newregistration_mail_sendtoother_mails_input', $this->CreateInputText($id, 'newregistration_mail_sendtoother_mails', $this->GetPreference('newregistration_mail_sendtoother_mails'), 50));

/* Template */
$smarty->assign('mail_newregistration_template_label', $this->CreateLabelForInput($id, 'newregistration_mail_template', $this->Lang('mail_newregistration_template')));
$smarty->assign('mail_newregistration_template_input', $this->CreateSyntaxArea($id, $this->GetTemplate('newregistrationmail_Sample'), 'newregistration_mail_template', '', '', '', '', 80, 7));
$smarty->assign('mail_newregistration_template_help', $this->Lang('mail_template_help'));

// Subject
$smarty->assign('mail_newregistration_subject_input', $this->CreateInputText($id, 'newregistration_mail_subject', $this->GetPreference('newregistration_mail_subject'), 70));
$smarty->assign('mail_newregistration_subject_label', $this->CreateLabelForInput($id, 'newregistration_mail_subject', $this->Lang('mail_subject')));

/* Mail on cancel registration **************************************************/
/* Mail for the user */
$smarty->assign('cancelregistration_mail_sendtouser_input', $this->CreateInputCheckbox($id, 'cancelregistration_mail_sendtouser', 1, $this->GetPreference('cancelregistration_mail_sendtouser', 0), 'id="'.$id.'cancelregistration_mail_sendtouser"'));
$smarty->assign('cancelregistration_mail_sendtouser_label', $this->CreateLabelForInput($id, 'cancelregistration_mail_sendtouser', $this->Lang('mail_sendtouser_label')));

/* Mail for the admin or other */
$smarty->assign('cancelregistration_mail_sendtoother_input', $this->CreateInputCheckbox($id, 'cancelregistration_mail_sendtoother', 1, $this->GetPreference('cancelregistration_mail_sendtoother', 0), 'id="'.$id.'cancelregistration_mail_sendtoother"'));
$smarty->assign('cancelregistration_mail_sendtoother_label', $this->CreateLabelForInput($id, 'cancelregistration_mail_sendtoother', $this->Lang('mail_sendtoother_label')));
$smarty->assign('cancelregistration_mail_sendtoother_mails_input', $this->CreateInputText($id, 'cancelregistration_mail_sendtoother_mails', $this->GetPreference('cancelregistration_mail_sendtoother_mails'), 50));

/* Template */
$smarty->assign('mail_cancelregistration_template_label', $this->CreateLabelForInput($id, 'cancelregistration_mail_template', $this->Lang('mail_cancelregistration_template')));
$smarty->assign('mail_cancelregistration_template_input', $this->CreateSyntaxArea($id, $this->GetTemplate('cancelregistrationmail_Sample'), 'cancelregistration_mail_template', '', '', '', '', 80, 7));
$smarty->assign('mail_cancelregistration_template_help', $this->Lang('mail_template_help'));

// Subject
$smarty->assign('mail_cancelregistration_subject_input', $this->CreateInputText($id, 'cancelregistration_mail_subject', $this->GetPreference('cancelregistration_mail_subject'), 70));
$smarty->assign('mail_cancelregistration_subject_label', $this->CreateLabelForInput($id, 'cancelregistration_mail_subject', $this->Lang('mail_subject')));


/* Process smarty */
echo $this->ProcessTemplate('admin_tab_mails.tpl');
?>