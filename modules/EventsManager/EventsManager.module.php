<?php
#-------------------------------------------------------------------------
# Module: Events
# Author: Mathieu MUTHS (Company Aire Libre) contact@airelibre.fr
# An addon module for CMS Made Simple to allow users to create and manage 
# Events like lessons, shows, training, etc. - Frontend users will be able to
# register to Events
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
#-------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin 
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#-------------------------------------------------------------------------

$cgextensions = cms_join_path($gCms->config['root_path'],'modules', 'CGExtensions','CGExtensions.module.php');
if( !is_readable( $cgextensions ) )
{
	echo '<h1><font color="red">ERROR: The CGExtensions module could not be found.</font></h1>';
	return;
}
require_once($cgextensions);

class EventsManager extends CGExtensions
{
	function GetName() { return 'EventsManager'; }
	function GetFriendlyName() 
	{
		$name = $this->GetPreference('menuname');
		if (empty($name))
			$name = $this->Lang('friendlyname');
		return $name;
	}
	function GetVersion() { return '1.2.7'; }
	function GetHelp() {
		$main_help = $this->Lang('help');
		$examples_help = $this->Lang('help_examples');

		$tabs_top = $this->StartTabHeaders();
		$tabs_top .= $this->SetTabHeader('main_help', $this->Lang('main_help_tab'));
		$tabs_top .= $this->SetTabHeader('examples_help', $this->Lang('examples_help_tab'));
		$tabs_top .= $this->EndTabHeaders();

		$total_help = $tabs_top;
		$total_help .= $this->StartTabContent() . $this->StartTab('main_help') . $main_help . $this->EndTab() . $this->StartTab('examples_help') . $examples_help . $this->EndTab() . $this->EndTabContent();
		return $total_help;
	}
	function GetAuthor() { return 'AireLibre'; }
	function GetAuthorEmail() { return 'contact@airelibre.fr'; }
	function GetChangeLog() { return file_get_contents(__FILE__).'/doc/changelog.html'; }
	function MinimumCMSVersion() {return '1.10';}
	//function MaximumCMSVersion() {return '1.11';}
	
	function InstallPostMessage() { return $this->Lang('postinstall'); }
	function UninstallPreMessage() {return $this->Lang('preuninstall'); }
	function UninstallPostMessage() { return $this->Lang('postuninstall'); }
	
	function IsPluginModule() { return true; }
	function HasAdmin() { return true; }
	function GetAdminSection() { return 'content'; }
	function GetAdminDescription () { return $this->Lang('admindescription'); }

	function GetEventDescription($eventname) { return $this->lang('eventdesc_'.$eventname);}
	function GetEventHelp($eventname) { return $this->lang('eventhelp_'.$eventname); }
	
	function VisibleToAdminUser() {return true;}
	function GetDependencies()
	{
		return (array('AireLibs'=>'1.3', 'CGExtensions'=>'1.24', 'FrontEndUsers'=>'1.16'));
	}
	
	// Non API function - Defined to handle the rights easier (with a default value)
	function HasPermission($perm = 'Use EventsManager')
	{
		return parent::CheckPermission($perm);
	}
	
	function InitializeFrontend()
	{
		$this->RegisterModulePlugin();
		$this->RestrictUnknownParams();
		
		/* Routes - URL Rewriting */
		// Detail : /prefix/event_id/returnid/alias
		$urlprefix = $this->GetPrefix('events');

		// With detail template
		$this->RegisterRoute('/' . $urlprefix . '\/(?P<event_id>[0-9]+)\/(?P<returnid>[0-9]+)\/(?P<junk>.*?)\/d,(?P<detailtemplate>.*?)$/', array('action'=>'detail'));
		// Classic link
		$this->RegisterRoute('/' . $urlprefix . '\/(?P<event_id>[0-9]+)\/(?P<returnid>[0-9]+)\/(?P<junk>.*?)$/', array('action'=>'detail'));
		
		// Registration : /prefix/registerurlprefix/event_id/returnid/alias
		$registerurlprefix = $this->GetPrefix('register');
		if (empty($registerurlprefix))
			$registerurlprefix = 'register';
		// With registration template
		$this->RegisterRoute('/'.$urlprefix . '\/' . $registerurlprefix .  '\/(?P<event_id>[0-9]+)\/(?P<returnid>[0-9]+)\/(?P<junk>.*?)\/d,(?P<registrationtemplate>.*?)$/', array('action'=>'register'));
		// Classic link
		$this->RegisterRoute('/'.$urlprefix . '\/' . $registerurlprefix .  '\/(?P<event_id>[0-9]+)\/(?P<returnid>[0-9]+)\/(?P<junk>.*?)$/', array('action'=>'register'));
		
		/* Params */
		$this->SetParameterType('event_id', CLEAN_INT);
		$this->SetParameterType('detailpage', CLEAN_INT);
		$this->SetParameterType('submit', CLEAN_STRING);
		$this->SetParameterType('cancel', CLEAN_STRING);
		$this->SetParameterType('nb_persons', CLEAN_INT);
		$this->SetParameterType('junk',CLEAN_STRING);
		$this->SetParameterType('orig_url',CLEAN_STRING);
		$this->SetParameterType('category',CLEAN_STRING);
		$this->SetParameterType('category_id', CLEAN_INT);
		$this->SetParameterType('status',CLEAN_STRING);
		$this->SetParameterType('show',CLEAN_STRING);
		$this->SetParameterType('start_datetime', CLEAN_STRING);
		$this->SetParameterType('end_datetime', CLEAN_STRING);
		$this->SetParameterType('myevents', CLEAN_STRING);
		$this->SetParameterType('sortby', CLEAN_STRING);
		$this->SetParameterType('sortorder', CLEAN_STRING);
		$this->SetParameterType('allow_notconnected', CLEAN_INT);
		$this->SetParameterType('redirectdetail', CLEAN_INT);
		$this->SetParameterType('overquotanb', CLEAN_INT);
		$this->SetParameterType('overquotatotal', CLEAN_INT);
		
		// Templates
		$this->SetParameterType('summarytemplate', CLEAN_STRING);
		$this->SetParameterType('detailtemplate', CLEAN_STRING);
		$this->SetParameterType('registrationtemplate', CLEAN_STRING);
		$this->SetParameterType('registrationslisttemplate', CLEAN_STRING);
	}
	
	function SetParameters()
	{
		$this->CreateParameter('action','default',$this->Lang('param_action'));
		$this->CreateParameter('event_id', '0', $this->Lang('param_event_id'));
		$this->CreateParameter('detailpage', '-1', $this->Lang('param_detailpage'));
		$this->CreateParameter('category_id', '0', $this->Lang('param_category_id'));
		$this->CreateParameter('category', '', $this->Lang('param_category'));
		$this->CreateParameter('status', 'published', $this->Lang('param_status'));
		$this->CreateParameter('show', 'all', $this->Lang('param_show'));
		$this->CreateParameter('start_datetime', '', $this->Lang('param_start_datetime'));
		$this->CreateParameter('end_datetime', '', $this->Lang('param_end_datetime'));
		$this->CreateParameter('myevents', 'false', $this->Lang('param_myevents'));
		$this->CreateParameter('sortby', 'name', $this->Lang('param_sortby'));
		$this->CreateParameter('sortorder', 'ASC', $this->Lang('param_sortorder'));
		$this->CreateParameter('allow_notconnected', '0', $this->Lang('param_allow_notconnected'));
		$this->CreateParameter('redirectdetail', '0', $this->Lang('param_redirectdetail'));
		$this->CreateParameter('overquotanb');
		$this->CreateParameter('overquotatotal');
		
		// Templates
		$this->CreateParameter('summarytemplate', '', $this->Lang('param_summarytemplate'));
		$this->CreateParameter('detailtemplate', '', $this->Lang('param_detailtemplate'));
		$this->CreateParameter('registrationtemplate', '', $this->Lang('param_registrationtemplate'));
		$this->CreateParameter('registrationslisttemplate', '', $this->Lang('param_registrationslisttemplate'));
	}
	
	function InitializeAdmin()
	{
	  $this->SetParameters();
	}
	
	function GetHeaderHTML()
	{
		$config = cmsms()->GetConfig();
		$userid = get_userid();
		$smarty = cmsms()->GetSmarty();
		$lang = get_preference($userid, 'default_cms_language');
		
		// Localization
		$isocode = 'en';
		if ($lang != 'en_US')
			$isocode = substr($lang, 0, strpos($lang, '_'));
		
		$smarty->assign('datepicker_lang', $isocode);
		
		return $this->ProcessTemplate('admin_header.tpl');
	}
	
	function SearchReindex(&$module)
	{
		$events = EventsManager_events_ops::get_events();
		
		foreach ($events as $event)
		{
			$module->AddWords($this->GetName(), $event->id, '', $event->name.' '.$event->description);
		}
	}
	
	function SearchResult ($returnid, $id, $attr='')
	{
		$result = array();
		$event = new EventsManager_event($id);
		
		$result[0] = $this->GetFriendlyName();
		$result[1] = $event->name;
		
		$detailpage = $this->GetPreference('dflt_detailpage', $returnid);
		if ($detailpage == -1)
			$detailpage = $returnid;
		$aliased_title = munge_string_to_url($event->name);
		
		$url_prefix = $this->GetPreference('urlprefix', 'events');
		if (empty($url_prefix))
			$url_prefix = 'events';
		$prettyurl = $url_prefix.'/'.$event->id.'/'.$detailpage.'/'.$aliased_title;
		$result[2] = $this->CreateFrontendLink($id, $detailpage, 'detail', '', array('event_id'=>$event->id), '', true, $inline, '', '', $prettyurl);
		
		return $result;
	}

	// Get the prefixes
	function GetPrefix($type='events')
	{
		$prefix = '';
		switch ($type)
		{
			case 'events':
				$prefix = $this->GetPreference('urlprefix', 'events');
				if (empty($prefix))
					$prefix = 'events';
				break;
			
			case 'register':
				$prefix = $this->GetPreference('registerurlprefix', 'register');
				if (empty($prefix))
					$prefix = 'register';
				break;
		}
		
		return $prefix;
	}
	
	// Preparation / Tests for the E-commerce integration
	function get_product_info($event_id)
	{
		$event = new EventsManager_event($event_id);
		
		$obj = new cg_ecomm_productinfo();
		$obj->set_product_id($event->id);
		$obj->set_name($event->name);
		$obj->set_weight(0);
		$obj->set_sku(0);
		$obj->set_price($event->price);
		$obj->set_taxable(0);
		$obj->set_type(cg_ecomm_productinfo::TYPE_PRODUCT);
		
		return $obj;
	}
}

?>