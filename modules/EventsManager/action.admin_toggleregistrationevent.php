<?php
if (!isset($gCms)) exit;
if (!$this->HasPermission()) exit;

$errors = array();
$messages = array();

$event = new EventsManager_event($params['id_event'], false);

$this->SetCurrentTab('events');

if (isset($params['allow_registration']))
{
	$event->allow_registration = $params['allow_registration'];
	
	if ($event->addedit('allow_registration'))
		$this->RedirectToTab($id);
}
else
{
	$errors[] = $this->Lang('error_db');
}

// Errors
foreach ($errors as $error)
	echo $this->ShowErrors($error);

?>