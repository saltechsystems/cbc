<?php
if (!isset($gCms)) exit;

// Display the list of events categories
$categories = EventsManager_categories_ops::get_categories();
$items = array();

$rowclass = 'row1';
foreach ($categories as $oneline)
{
	$item = new stdClass();
	$item->id = $oneline['id'];
	$item->name = $this->CreateLink($id, 'admin_addeditcategory', $returnid, $oneline['name'], array('id_category'=>$oneline['id']));
	
	$item->editlink = $this->CreateLink($id, 'admin_addeditcategory', $returnid, $gCms->variables['admintheme']->DisplayImage('icons/system/edit.gif', $this->Lang('edit'), '', '', 'systemicon'), array('id_category'=>$oneline['id']));
	$item->deletelink = $this->CreateLink($id, 'admin_deletecategory', $returnid, $gCms->variables['admintheme']->DisplayImage('icons/system/delete.gif', $this->Lang('delete'), '', '', 'systemicon'), array('id_category'=>$oneline['id']), $this->Lang('areyousure'));
	$item->rowclass = $rowclass;
	
	$items[] = $item;
	
	($rowclass=="row1" ? $rowclass="row2" : $rowclass="row1");
}

// Assign to smarty
$smarty->assign('items', $items);
$smarty->assign('addlink', $this->CreateLink($id, 'admin_addeditcategory', $returnid, $gCms->variables['admintheme']->DisplayImage('icons/system/newfolder.gif', $this->Lang('addcategory'), '', '', 'systemicon') . $this->Lang('addcategory')));

echo $this->ProcessTemplate('admin_tab_categories.tpl');
?>