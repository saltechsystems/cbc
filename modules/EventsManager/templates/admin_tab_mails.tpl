{$form_start}

<fieldset>
	<legend>{$mod->Lang('mail_newregistration_title')}</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$mod->Lang('email_to_who_newregistration')}</p>
		<p class="pageinput">
			{$newregistration_mail_sendtouser_input} {$newregistration_mail_sendtouser_label}<br />
			{$newregistration_mail_sendtoother_input} {$newregistration_mail_sendtoother_label}:
			{$newregistration_mail_sendtoother_mails_input}
		</p>
		<p class="pagetext">{$mail_newregistration_template_label}</p>
		<p class="pageinput">
			{$mail_newregistration_template_help}
		</p>
		<p class="pagetext">
			{$mail_newregistration_subject_label}: {$mail_newregistration_subject_input}<br /><br />
		</p>
		<p class="pageinput">
			{$mail_newregistration_template_input}
		</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput">{$submit} {$cancel}</p>
	</div>
</fieldset>

<fieldset>
	<legend>{$mod->Lang('mail_cancelregistration_title')}</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$mod->Lang('email_to_who_cancelregistration')}</p>
		<p class="pageinput">
			{$cancelregistration_mail_sendtouser_input} {$cancelregistration_mail_sendtouser_label}<br />
			{$cancelregistration_mail_sendtoother_input} {$cancelregistration_mail_sendtoother_label}:
			{$cancelregistration_mail_sendtoother_mails_input}
		</p>
		<p class="pagetext">{$mail_cancelregistration_template_label}</p>
		<p class="pageinput">
			{$mail_cancelregistration_template_help}</p>
		<p class="pagetext">
			{$mail_cancelregistration_subject_label}: {$mail_cancelregistration_subject_input}<br /><br />
		</p>
		<p class="pageinput">
			{$mail_cancelregistration_template_input}
		</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput">{$submit} {$cancel}</p>
	</div>
</fieldset>

{$form_end}