<script type="text/javascript" src="{root_url}/modules/EventsManager/js/datepicker-locale/jquery.ui.datepicker-{$datepicker_lang}.js"></script>
{if $datepicker_lang neq 'en'}
	<script type="text/javascript" src="{root_url}/modules/EventsManager/js/i18n/jquery.ui.timepicker-{$datepicker_lang}.js"></script>
{/if}
<script type="text/javascript" src="{root_url}/modules/EventsManager/js/jquery.ui.timepicker.js"></script>
<script type="text/javascript" src="{root_url}/modules/AireLibs/js/jquery.ui.ufd.min.js"></script>

<link rel="stylesheet" type="text/css" href="{root_url}/lib/jquery/css/smoothness/jquery-ui-1.8.12.custom.css" />
<link rel="stylesheet" type="text/css" href="{root_url}/modules/EventsManager/js/jquery.ui.timepicker.css" />
<link rel="stylesheet" type="text/css" href="{root_url}/modules/AireLibs/js/css/ufd-base.css" />
<link rel="stylesheet" type="text/css" href="{root_url}/modules/AireLibs/js/css/plain.css" />

<script style="text/javascript">
{literal}
	$(function() {
		$.datepicker.setDefaults( $.datepicker.regional["{/literal}{$datepicker_lang}{literal}"] );
		// Event dates
		var dates = $( "#m1_start_date, #m1_end_date" ).datepicker({
			defaultDate: "+1w",
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			numberOfMonths: 3,
			onSelect: function( selectedDate ) {
				var option = this.id == "m1_start_date" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
				
				if ( (this.id == "m1_start_date") && $('#m1_end_date').val() == '')
				{
					$('#m1_end_date').val($('#m1_start_date').val());
				}
			}
		});
		// Registration period dates
		var dates2 = $( "#m1_reg_start_date, #m1_reg_end_date" ).datepicker({
			defaultDate: "+1w",
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			numberOfMonths: 3,
			onSelect: function( selectedDate ) {
				var option = this.id == "m1_reg_start_date" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates2.not( this ).datepicker( "option", option, date );

				if ( (this.id == "m1_reg_start_date") && $('#m1_reg_end_date').val() == '')
				{
					$('#m1_reg_end_date').val($('#m1_reg_start_date').val());
				}
			}
		});
		$('#m1_start_time').timepicker();
		$('#m1_end_time').timepicker();
		$('#m1_reg_start_time').timepicker();
		$('#m1_reg_end_time').timepicker();
		
		// Display / hide the registrations informations in event edition
		if ($('#allow_registration input').attr('checked'))
		{
			$('#options_registration').show();
		}
		$('#allow_registration input').click(function () {
			$('#options_registration').toggle();
		});

		// Display / hide the registrations period informations in event edition
		if ($('#limited_reg_period input').attr('checked'))
		{
			$('#reg_period_datetimes').show();
		}
		$('#limited_reg_period input').click(function () {
			$('#reg_period_datetimes').toggle();
		});

		// Select filter in the backend registration page
		$('#user_select select').ufd();

		// Hide capacity in event edit
		$('#m1_capacity_unlimited').change(function () {
			$('#m1_capacity_input').toggle();
		});

		// Hide capacity per feu in event edit
		$('#m1_capacity_per_feu_unlimited').change(function () {
			$('#m1_capacity_per_feu_input').toggle();
		});
	});

{/literal}
</script>