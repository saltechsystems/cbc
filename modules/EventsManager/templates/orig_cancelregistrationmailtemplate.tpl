<p>Hello {$feu_user->username},</p>

<p>This mail is to confirm your cancellation for the registration to the event <strong>{$event->name}</strong></p>
<p>Some informations about this event:
	<ul>
		<li>Start: {$event->start_datetime|cms_date_format}</li>
		<li>End: {$event->end_datetime|cms_date_format}</li>
	</ul>
</p>
<p>Thank you,</p>
<p><a href="{root_url}">Website : {root_url}</a></p>