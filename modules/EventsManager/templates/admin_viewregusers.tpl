
<h3>{$mod->Lang('registrations_for')}: {$event->name}</h3>

{if $items|@count > 0}
<div class="pageoptions"><p class="pageoptions">
	{$backlink} {$addlink} {$exportlink}
	{if isset($nmsexportlink)}
		{$nmsexportlink}
	{/if}
</p></div>

<table cellspacing="0" class="pagetable">
	<thead>
		<tr>
			<th>ID</th>
			<th>{$mod->Lang('regdate')}</th>
			<th>{$mod->Lang('nb_persons')}</th>
			<th>{$mod->Lang('feuname')}</th>
			{foreach from=$feu_props_names item=feu_prop}
				<th>{$feu_prop}</th>
			{/foreach}
			<th class="pageicon">&nbsp;</th>
			<th class="pageicon">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	
	{foreach from=$items item=entry}
		<tr class="{$entry->rowclass}">
			<td>{$entry->id}</td>
			<td>{$entry->modify_datetime|cms_date_format}</td>
			<td>{$entry->nb_persons}</td>
			<td>{$entry->username}</td>
			{section loop=$count_feu_props name='loop'}
				<td>
					{$entry->feu_props[$smarty.section.loop.index]}
				</td>
			{/section}
			<td>{$entry->editlink}</td>
			<td>{$entry->deletelink}</td>
		</tr>
	{/foreach}
	</tbody>
</table>
{else}
	<p>{$mod->Lang('no_registrations')}</p>
{/if}

<div class="pageoptions">
	<p class="pageoptions">
		{$backlink} {$addlink} {$exportlink} 
		{if isset($nmsexportlink)}
			{$nmsexportlink}
		{/if}
	</p>
</div>