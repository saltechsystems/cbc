
<h3>{$mod->Lang('export_regusers_nms')}: {$event->name}</h3>

{$form_start}
<fieldset>
	<legend>{$mod->Lang('export_options')}</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_emailproperty}:</p>
		<p class="pageinput">{$input_emailproperty}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$label_nameproperty}:</p>
		<p class="pageinput">{$input_nameproperty}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$label_nmslist}:</p>
		<p class="pageinput">{$input_nmslist}</p>
		<p class="pageinput">{$mod->Lang('note_export_nms')}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$label_export_feu_groups}:</p>
		<p class="pageinput">{$input_export_feu_groups}</p>
		<p class="pageinput">{$mod->Lang('note_export_nms_feugroups')}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$label_who_export}:</p>
		<p class="pageinput">{$input_who_export}</p>
		<p class="pageinput">{$mod->Lang('note_export_nms_who')}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$mod->Lang('delete_listuser_link')}:</p>
		<p class="pageinput">{$input_delete_listuser_link} {$label_delete_listuser_link}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput">{$submit} {$cancel}</p>
	</div>
</fieldset>
{$form_end}