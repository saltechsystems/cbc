<script type="text/javascript">
{literal}
	$(document).ready(function () {
		$('option.past').each(function () {
			$(this).hide();
		});
		$('#m1_id_event option:not(.past):first').attr('selected', true);

		$('#show_past').change(function () {
			if ($('#show_past:checked').val() == 'on')
			{
				$('option.past').each(function () {
					$(this).show();
				});
			}
			else
			{
				$('option.past').each(function () {
					$(this).hide();
				});

				if ($('#m1_id_event option:selected').css('display') == 'none')
				{
					// Select the first not hidden element
					$('#m1_id_event option:not(.past):first').attr('selected', true);
				}
			}
		});
	});
{/literal}
</script>

<h3>{$title}</h3>

{$form_start}
	<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput">{$submit}{$cancel}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$event_label}:</p>
		<p class="pageinput">{$event_input}&nbsp;<input type="checkbox" id="show_past"><label for="show_past">{$show_past_events}</label></p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$feusers_label}:</p>
		<p class="pageinput" id="user_select">{$feusers_input}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$nb_persons_label}:</p>
		<p class="pageinput">{$nb_persons_input}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput">{$submit}{$cancel}</p>
	</div>
{$hidden}
{$form_end}