{$form_start}

<fieldset>
	<legend>{$mod->Lang('generaloptions')}</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_menuname}:</p>
		<p class="pageinput">{$input_menuname}</p>

		<p class="pagetext">{$label_showtabs}:</p>
		<p class="pageinput">{$input_showtabs}</p>
	</div>
</fieldset>
<fieldset>
	<legend>{$mod->Lang('urloptions')}</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_urlprefix}:</p>
		<p class="pageinput">{$input_urlprefix}</p>
	
		<p class="pagetext">{$label_registerurlprefix}:</p>
		<p class="pageinput">{$input_registerurlprefix}</p>
	</div>
</fieldset>
<fieldset>
	<legend>{$mod->Lang('eventdetailoptions')}</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_dflt_detailpage}:</p>
		<p class="pageinput">{$input_dflt_detailpage}</p>
	</div>
</fieldset>
<fieldset>
	<legend>{$mod->Lang('eventregistrationoptions')}</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_dflt_registrationpage}:</p>
		<p class="pageinput">{$input_dflt_registrationpage}</p>

		<p class="pagetext">{$label_overquota_behaviour}:</p>
		<p class="pageinput">{$input_overquota_behaviour}</p>
	</div>
</fieldset>
<fieldset>
	<legend>{$mod->Lang('reguserslistoptions')}</legend>
	<div class="pageoverflow">
	{if $input_displayed_feu_properties}
			<p class="pagetext">{$label_displayed_feu_properties}:</p>
			<p class="pageinput">{$input_displayed_feu_properties}</p>
	{/if}
		<p class="pagetext">{$label_exportencoding}:</p>
		<p class="pageinput">{$input_exportencoding}</p>

		<p class="pagetext">{$mod->Lang('title_export_cancelled')}:</p>
		<p class="pageinput">{$input_export_cancelled} {$label_export_cancelled}</p>
	</div>
</fieldset>
<fieldset>
	<legend>{$mod->Lang('feuloginoptions')}</legend>
	<div class="pageoverflow">
		<p class="pagetext">{$label_dflt_feuloginpage}:</p>
		<p class="pageinput">
			{$input_dflt_feuloginpage}<br />
			{$mod->Lang('dflt_feuloginpage_help')}
		</p>
	</div>
</fieldset>

<div class="pageoverflow">
	<p class="pagetext">&nbsp;</p>
	<p class="pageinput">
		{$submit} {$cancel}
	</p>
</div>

{$form_end}