
{if $items|@count > 0}
<div class="pageoptions"><p class="pageoptions">{$addlink} {$reg_addlink}</p></div>

<table cellspacing="0" class="pagetable">
	<thead>
		<tr>
			<th>ID</th>
			<th>{$mod->Lang('name')}</th>
			<th>{$mod->Lang('published')}</th>
			<th>&nbsp;</th>
			<th>{$mod->Lang('start_datetime')}</th>
			<th>{$mod->Lang('end_datetime')}</th>
			<th>{$mod->Lang('registration_allowed')}</th>
			<th>{$mod->Lang('number_of_regusers')} / {$mod->Lang('capacity')}</th>
			<th>{$mod->Lang('manage_regusers')}</th>
			<th>{$mod->Lang('export_regusers')}</th>
			<th class="pageicon">&nbsp;</th>
			<th class="pageicon">&nbsp;</th>
			<th class="pageicon">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	
	{foreach from=$items item=entry}
		<tr class="{$entry->rowclass}">
			<td>{$entry->id}</td>
			<td>{$entry->name_editlink}</td>
			<td>{$entry->togglestatuslink}</td>
			<td>
				{if $entry->cancelled}
					<span style="color: red">{$mod->Lang('cancelled')}
					
				{elseif $entry->whenstatus eq "upcoming"}
					<span style="color: orange">{$mod->Lang('event_upcoming')}
				{elseif $entry->whenstatus eq "past"}
					<span style="color: blue">{$mod->Lang('event_past')}
				{else}
					<span style="color: green">{$mod->Lang('event_inprogress')}
					
				{*elseif $entry->start_datetime|cms_date_format:'%s' > $timenow}
					<span style="color: orange">{$mod->Lang('event_upcoming')}
				{elseif $entry->end_datetime|cms_date_format:'%s' < $timenow}
					<span style="color: blue">{$mod->Lang('event_past')}
				{else}
					<span style="color: green">{$mod->Lang('event_inprogress')*}
				{/if}
			</td>
			<td>{$entry->start_datetime}</td>
			<td>{$entry->end_datetime}</td>
			<td>{$entry->toggleregistationlink}</td>
			<td style="color: {if ($entry->get_regusers_number() eq $entry->capacity) && ($entry->capacity neq 0)}red{else}green{/if}; font-weight: bold">
				{$entry->get_regusers_number()} / {if $entry->capacity eq 0}{$mod->Lang('unlimited')}{else}{$entry->capacity}{/if}
			</td>
			<td>{$entry->viewreguserslink}</td>
			<td>{if $entry->get_regusers_number() > 0}
				{$entry->exportreguserslink}
				{if isset($entry->exportreguserslink_nms)}
					&nbsp;{$entry->exportreguserslink_nms}
				{/if}
			{/if}</td>
			<td>{$entry->copylink}</td>
			<td>{$entry->editlink}</td>
			<td>{$entry->deletelink}</td>
		</tr>
	{/foreach}
	</tbody>
</table>
{/if}

<div class="pageoptions"><p class="pageoptions">{$addlink} {if $items|@count > 0}{$reg_addlink}{/if}</p></div>