{* Template for the list of registrations of an event *}

{if $registrations|@count}
	<table style="width: 100%">
		<thead>
			<th>ID</th>
			<th>Registation date</th>
			<th>Number of persons</th>
			<th>Username</th>
		</thead>
		<tbody>
			{foreach from=$registrations item='registration'}
				<tr>
					<td>{$registration->id}</td>
					<td>{$registration->modify_datetime|cms_date_format}</td>
					<td>{$registration->nb_persons}</td>
					<td>{$registration->user->username}</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
{else}
	<p>No registrations</p>
{/if}