{* Event registration default template *}

{if $errors|@count > 0}
	{foreach from=$errors item='error'}
		<p class='error_message'>{$error}</p>
	{/foreach}
{/if}
{if $messages|@count > 0}
	{foreach from=$messages item='message'}
		<p class='message'>{$message}</p>
	{/foreach}
{/if}

{if $event->allow_registration}
	{if $event->limited_reg_period eq 1}
		<p>Registrations for this event runs from {$event->reg_start_datetime|cms_date_format} to {$event->reg_end_datetime|cms_date_format}</p>
	{/if}

	{if $registration->status eq 'registered'}
		<p>You're registered for {$registration->nb_persons} place(s)</p>
	{elseif $registration->status eq 'cancelled'}
		<p>You've cancelled your registration on {$registration->modify_datetime|cms_date_format}</p>
	{else}
		<p>You're not enrolled to this event</p>
	{/if}

	{if $display_event_registration_form}
		{if isset($overquotanb) and isset($overquotatotal)}
			<div class="overquota_message">
				<p style="color: red">You cannot register <strong>{$overquotanb}</strong> places, because this exceed the total number of available places. You can get <strong>{$overquotatotal}</strong> places. Please modify:</p>
			</div>
		{elseif isset($overquotanb) and ($overquotanb gt 0)}
			<div class="overquota_message">
				<p style="color: red">You cannot register <strong>{$overquotanb}</strong> places, because this event is limited to <strong>{$event->capacity_per_feu}</strong> places per user. Please modify:</p>
			</div>
		{/if}
		{$form_start}
			{$hidden}

			{* Note, you can simply use : *}
			{* {$nb_persons_label} {$nb_persons_input}{$submit}{$cancel} *}
			{* If you want to always display the form, even when the user is already registered *}

			{if ( ($event->capacity_per_feu > 1) or ($event->capacity_per_feu eq 0)) AND $registration->nb_persons eq 0}
				{$nb_persons_label} {$nb_persons_input}
			{/if}

			{if $registration->nb_persons eq 0}
				{$submit}
			{else}
				{$cancel}
			{/if}

		{$form_end}
	{/if}
{/if}