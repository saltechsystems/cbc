
{if $items|@count > 0}

<div class="pageoptions"><p class="pageoptions">{$addlink}</p></div>

<table cellspacing="0" class="pagetable">
	<thead>
		<tr>
			<th>ID</th>
			<th>{$EventsManager->Lang('name')}</th>
			<th class="pageicon">&nbsp;</th>
			<th class="pageicon">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	
	{foreach from=$items item=entry}
		<tr class="{$entry->rowclass}">
			<td>{$entry->id}</td>
			<td>{$entry->name}</td>
			<td>{$entry->editlink}</td>
			<td>{$entry->deletelink}</td>
		</tr>
	{/foreach}
	</tbody>
</table>
{/if}

<div class="pageoptions"><p class="pageoptions">{$addlink}</p></div>