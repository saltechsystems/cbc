<h3>{$title}</h3>

{$form_start}
	<div class="pageoverflow">
		<p class="pageinput">{$submit}{$cancel}</p>
	</div>

	{if $showtabs}
		{$tabs_top}
		{$tab_content_start}

		{$tab_generalinfo_start}
	{/if}
		<div class="pageoverflow">
			<p class="pagetext">{$category_label}:</p>
			<p class="pageinput">{$category_input}</p>
		</div>
		</fieldset>
		<div class="pageoverflow">
			<p class="pagetext">*{$name_label}:</p>
			<p class="pageinput">{$name_input}</p>
		</div>
		<div class="pageoverflow">
			<p class="pagetext">{$price_label}:</p>
			<p class="pageinput">{$price_input}</p>

			<p class="pagetext">{$status_label}:</p>
			<p class="pageinput">{$status_input}</p>

			<p class="pagetext">{$cancelled_label}:</p>
			<p class="pageinput">{$cancelled_input}</p>
		
			<p class="pagetext">{$description_label}:</p>
			<p class="pageinput">{$description_input}</p>

			<p class="pagetext">{$start_datetime_label}:</p>
			<p class="pageinput">{$start_date_input} {$start_time_input}</p>

			<p class="pagetext">{$end_datetime_label}:</p>
			<p class="pageinput">{$end_date_input} {$end_time_input}</p>

			<p class="pagetext">{$capacity_label}:</p>
			<p class="pageinput">{$capacity_input} {$capacity_unlimited} {$capacity_unlimited_label}</p>
		</div>

		{$custom_fields_form}

	{if $showtabs}
		{$tab_end}

		{$tab_registrations_start}
	{/if}
		<div class="pageoverflow" id="allow_registration">
			<p class="pagetext">{$mod->Lang('allow_registration')} :</p>
			<p class="pageinput">{$allow_registration_input} {$allow_registration_label}</p>
		</div>
		<div style="display: none" id="options_registration">
			<div class="pageoverflow">
				<p class="pagetext">{$allowed_feu_groups_label}:</p>
				<p class="pageinput">{$allowed_feu_groups_input}</p>
			</div>
			<div class="pageoverflow">
				<p class="pagetext">{$capacity_per_feu_label}:</p>
				<p class="pageinput">{$capacity_per_feu_input} {$capacity_per_feu_unlimited} {$capacity_per_feu_unlimited_label}</p>
			</div>
			<div class="pageoverflow" id="limited_reg_period">
				<p class="pagetext">{$mod->Lang('reg_period')} :</p>
				<p class="pageinput">{$limited_reg_period_input} {$limited_reg_period_label}</p>
			</div>
			<div style="display: none" id="reg_period_datetimes">
				<div class="pageoverflow">
					<p class="pagetext">{$reg_start_datetime_label}:</p>
					<p class="pageinput">{$reg_start_date_input} {$reg_start_time_input}</p>
				</div>
				<div class="pageoverflow">
					<p class="pagetext">{$reg_end_datetime_label}:</p>
					<p class="pageinput">{$reg_end_date_input} {$reg_end_time_input}</p>
				</div>
			</div>
		</div>
	{if $showtabs}
		{$tab_end}

		{$tab_content_end}
	{/if}
	
	<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput">{$submit}{$cancel}</p>
	</div>
{$hidden}
{$form_end}