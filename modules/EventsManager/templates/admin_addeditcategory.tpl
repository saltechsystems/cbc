<h3>{$title}</h3>

{$form_start}
{$hidden}
	<div class="pageoverflow">
		<p class="pagetext">*{$name_label}:</p>
		<p class="pageinput">{$name_input}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">{$description_label}:</p>
		<p class="pageinput">{$description_input}</p>
	</div>
	<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput">{$submit}{$cancel}</p>
	</div>
{$form_end}