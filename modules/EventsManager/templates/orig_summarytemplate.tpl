{* Events summary default template *}
{* Events and details are stored in the $items array *}
{* What can you use in this template? Some examples:
		{$entry->name} : event name
		{$entry->description} : event description
		{$entry->category} : the category object
			{$entry->category->name} : category name
			{$entry->category->description} : category description
		{$entry->start_datetime} : event start datetime (i.e. {$entry->start_datetime|cms_date_format}) - Same with "end_datetime"
		{$entry->capacity} : capacity of the event
		{$entry->fields : an array of extra fields
			If you have an extra field with the alias "place", use :
			{$entry->fields.place->value}

		NOTE : sometimes, the extrafields values are arrays, so use {$entry->fields.place|print_r} to see what is available

		Use {$entry|print_r} to display all the infos available
*}
{if $items|@count}
	{foreach from=$items item=entry}
	
		{* Important, do not remove the line above: *}
		{assign var='overquotanb' value=0}
		
		<h3>{$entry->name}</h3>
		{if isset($entry->category)}
			<p>Category: {$entry->category->name}</p>
		{/if}
		<p>From : {$entry->start_datetime|cms_date_format} to : {$entry->end_datetime|cms_date_format}</p>
		<p><a href="{$entry->detailurl}">Event details</a></p>

		{if $entry->allow_registration}
			<h4>Event registration</h4>
			<p>Registered places: {$entry->nb_regusers} / Capacity: {if $entry->capacity > 0}{$entry->capacity} / Remaining places: {math equation="x - y" x=$entry->capacity y=$entry->nb_regusers} {else}Unlimited{/if}</p>

			{if isset($entry->registration_url)}
				<p><a href="{$entry->registration_url}">Register</a></p>
			{/if}

			{EventsManager action='register' event_id=$entry->id inline=1 allow_notconnected=1}
		{/if}

		{if !empty($entry->description)}
			<h4>Description</h4>
			<p>{$entry->description}</p>
		{/if}

		{* Extra fields *}
		{if isset($entry->fields)}
			{foreach from=$entry->fields item='oneval' key='key'}
				<h4>{$oneval->name}</h4>
				<div>
					{if $oneval->type eq 'image' and $oneval->value neq ''}
						<img src="{$oneval->file_url}" style="max-width: 200px" />
					{elseif $oneval->type eq 'checkboxes' and !empty($oneval->value)}
						<ul>
							{foreach from=$oneval->value item='onecheckbox'}
								<li>{$onecheckbox}</li>
							{/foreach}
						</ul>
					{elseif $oneval->value neq ''}
						{$oneval->value}
					{else}
						<p>No value for {$oneval->name}</p>
					{/if}
				</div>
				<br /><br />

				{* Uncomment to display all the info for the current field : *}
				{*
					{$key} : {$oneval|print_r}<br /><br />
				*}
			{/foreach}
		{/if}

		<a href="{$entry->detailurl}">View event details</a>
		
	{/foreach}
{else}
	<p>No event</p>
{/if}