<?php

$lang['friendlyname'] = "Evènements et manifestations";
$lang['admindescription'] = "Module de gestion d'un calendrier d'&eacute;v&eacute;nements, avec possibilit&eacute; pour les visiteurs du site de s'inscrire via FrontEndUsers";

$lang['postinstall'] = "Le module EventsManager (calendrier d'événements) a bien été installé - Vous pouvez l'utiliser via le menu \"Contenu\". N'oubliez pas de donner les droits liés à \"EventsManager\" à vos admins/rédacteurs/designers.";
$lang['preuninstall'] = 'Êtes-vous sûr de vouloir supprimer le module ainsi que tous vos événements ?';
$lang['postuninstall'] = "Le module EventsManager (calendrier d'événements) a été désinstallé.";

# General
$lang['name'] = 'Nom';
$lang['published'] = 'Publié';
$lang['draft'] = 'Brouillon';

# Tabs
$lang['events'] = 'Evénements';
$lang['preferences'] = 'Préférences';
$lang['categories'] = 'Catégories';
$lang['fielddefs'] = 'Champs extra';
$lang['mails'] = 'E-Mails';

$lang['edit'] = 'Editer';
$lang['delete'] = 'Supprimer';
$lang['areyousure'] = 'Êtes-vous sûr ? Cette opération est irréversible';
$lang['copy'] = 'Copier';

# Events
$lang['addevent'] = 'Ajouter un événement';
$lang['editevent'] = 'Modifier un événement';
$lang['copyevent'] = 'Copier un événement';
$lang['eventdescription'] = 'Description';
$lang['start_datetime'] = 'Date et heure de début';
$lang['end_datetime'] = 'Date et heure de fin';
$lang['number_of_regusers'] = 'Nombre d\'inscrits';
$lang['capacity'] = 'Places';
$lang['unlimitedcapacity'] = 'Capacité illimitée';
$lang['unlimited'] = 'Illimitée';
$lang['capacity_per_feu'] = "Nombre d'inscriptions/places max. pour chaque utilisateur (FEU)";
$lang['unlimitedcapacity_per_feu'] = "Inscriptions/places illimitées pour chaque compte utilisateur";
$lang['view_regusers'] = 'Voir les inscrits';
$lang['allow_registration'] = 'Autoriser les inscriptions';
$lang['allow_registration_detail'] = 'Autoriser les visiteurs du site à s\'inscrire à cet événement';
$lang['registration_allowed'] = 'Inscriptions autorisées';
$lang['toggle_registration'] = 'Autoriser/Interdire l\'inscription';
$lang['toggle_status'] = 'Publier/Masquer';
$lang['allowed_feu_groups'] = "Groupes d'utilisateurs autorisés à s'inscrire à cet événement - Si aucun n'est sélectionné, tous les groupes seront autorisés à s'inscrire";
$lang['back_toeventslist'] = 'Retour à la liste des événements';
$lang['price'] = 'Tarif';

# Registration
$lang['nb_persons'] = 'Nombre de participants';
$lang['register'] = 'Réserver pour cet événement';
$lang['cancelregister'] = 'Annuler la réservation';
$lang['registrations_for'] = 'Inscriptions pour l\'événement ';
$lang['feuname'] = 'Nom d\'utilisateur';
$lang['regdate'] = 'Date d\'inscription';
$lang['export_regusers'] = 'Exporter les inscriptions';
$lang['export_regusers_file'] = 'Vers CSV';
$lang['exportencoding'] = 'Encodage des fichiers CSV exportés';
$lang['no_registrations'] = 'Il n\'y a aucune inscription pour cet évènement';

# NMS Export
$lang['export_regusers_nms'] = 'Exporter vers une liste Newsletter Made Simple ';
$lang['export_regusers_nms_small'] = 'Vers Newsletter';
$lang['export_options'] = "Options d'export";
$lang['nmslist'] = "Liste de Newsletter vers laquelle exporter. Vous pouvez choisir plusieurs listes";
$lang['note_export_nms'] = "Note : les utilisateurs déjà inscrits à une liste ne seront pas réinscrits";
$lang['emailproperty'] = "Propriété FEU qui contient l'e-mail ";
$lang['nameproperty'] = "Propriété FEU qui contient le nom pour la Newsletter ";
$lang['use_username'] = "Utiliser l'identifiant";
$lang['export_feu_groups'] = "Groupes FEU à exporter";
$lang['note_export_nms_feugroups'] = "Ceci est utile pour n'exporter vers les listes de newsletter que les inscrits à un évènement et qui appartiennent au(x) groupe(s) sélectionné(s). Paramétrez cette option si vos groupes FEU n'ont pas les mêmes propriétés d'e-mail et de nom pour la newsletter.";
$lang['nms_export_done'] = '%s utilisateurs ont été exportés vers la/les liste(s) de newsletter';

# Categories
$lang['category'] = 'Catégorie';
$lang['addcategory'] = "Ajouter une catégorie d'événements";
$lang['editcategory'] = "Modifier une catégorie d'événements";

# E-Mails # General
$lang['mail_sendtouser_label'] = "Envoyer un e-mail à l'utilisateur qui s'inscrit (note : ceci ne fonctionne que si l'utilisateur FEU dispose d'un e-mail dans son compte)";
$lang['mail_sendtoother_label'] = "Envoyer un e-mail à ces adresses (liste séparée par des virgules) ";
$lang['mail_template_help'] = 'Objets disponibles :<br />
<strong>{$event}</strong> : infos sur l\'évènement ($event->name, $event->description, $event->extrafieldsbyname.mon-champ->value, etc.)<br />
<strong>{$registration}</strong> : informations sur l\'inscription ($registration->nb_persons)<br />
<strong>{$feu_user}</strong> : l\'utilisateur FEU';
$lang['mail_subject'] = "Sujet de l'e-mail";

#E-Mails # New registration
$lang['mail_newregistration_title'] = "E-mail envoyé lors d'une nouvelle inscription";
$lang['email_to_who_newregistration'] = "Qui doit recevoir un e-mail lors d'une nouvelle inscription ?";
$lang['mail_newregistration_template'] = "Gabarit du mail envoyé lors d'une nouvelle inscription";

# E-mails # Cancel registration
$lang['mail_cancelregistration_title'] = "E-mail envoyé lors d'une annulation d'inscription";
$lang['email_to_who_cancelregistration'] = "Qui doit recevoir un e-mail lors d'une annulation d'inscription ?";
$lang['mail_cancelregistration_template'] = "Gabarit du mail envoyé lors d'une annulation d'inscription";

# Errors
$lang['error_emptyname'] = "Vous n'avez pas saisi de nom";
$lang['error_nameused'] = 'Une catégorie portant ce nom existe déjà';
$lang['error_db'] = 'Une erreur s\'est produite lors de l\'insertion / modification dans la base de données';
$lang['error_registration_notallowed'] = "Vous ne pouvez pas vous inscrire à cet événement";
$lang['error_notconnected'] = "Vous devez être identifié pour vous inscrire";
$lang['error_paramsmissing'] = "Un ou plusieurs paramètres manquent";
$lang['error_nolistselected'] = "Veuillez sélectionner au moins une liste de newsletter pour l'export";

# Messages
$lang['message_registration_ok'] = "L'inscription a bien été enregistrée.";

# Prefs
$lang['generaloptions'] = 'Préférences générales';
$lang['label_menuname'] = 'Nom personnalisé du module (visible dans le menu de l\'admin) - Si vide, le nom par défaut est utilisé';

$lang['urloptions'] = "Préférences des URLs";
$lang['urlprefix'] = "Préfixe à utiliser sur toutes les URL ciblées de ce module";
$lang['registerurlprefix'] = "Préfixe à utiliser pour les liens d'enregistrement";

$lang['eventdetailoptions'] = "Préférences des détails d'un événement";
$lang['label_dflt_detailpage'] = "Page de détail d'un événement par défaut. Si 'Aucun' est sélectionné, c'est la page courante qui sera utilisée";

$lang['eventregistrationoptions'] = "Préférences de l'inscription aux événements";
$lang['label_dflt_registrationpage'] = "Page par défaut sur laquelle afficher le formulaire d'inscription. Si 'Aucun' est sélectionné, c'est la page courante qui sera utilisée";

$lang['reguserslistoptions'] = 'Préférences de la liste des inscrits à un événement';
$lang['displayed_feu_properties'] = "Propriétés FEU affichées dans la liste des inscriptions et à l'export";

# Templates / Admin
$lang['summarytemplates'] = 'Gabarits de sommaire';
$lang['detailtemplates'] = 'Gabarits de détail';
$lang['registrationtemplates'] = "Gabarits d'inscription";
$lang['registrationslisttemplates'] = "Gabarits de listes d'inscrits";

$lang['summarytemplate_addedit'] = "Ajouter / Editer un gabarit de sommaire des événements ";
$lang['detailtemplate_addedit'] = "Ajouter / Editer un gabarit de détail d'un événement ";
$lang['registrationtemplate_addedit'] = "Ajouter / Editer un gabarit de formulaire d'inscription à un événement ";
$lang['registrationslisttemplate_addedit'] = "Ajouter / Editer un gabarit de liste d'inscrits côté site";

# Help with params
$lang['param_action'] = "Définit le comportement du module. Les valeurs possibles sont :
<ul>
	<li><em>default</em> - Affiche la liste des évènements - Vous pouvez aussi utiliser : <em>action='summary'</em></li>
	<li>detail - Affiche le détail d'un évènement - Utilisez le paramètre 'event_id' pour spécifier l'évènement à afficher</li>
	<li>register - Affiche le formulaire d'inscription à un évènement - Utilisez le paramètre event_id pour préciser l'évènement auquel s'inscrire</li>
	<li>registrationslist - Affiche la liste des inscriptions à un évènement - Utilisez le paramètre event_id pour préciser l'évènement concerné (voir l'aide plus haut)</li>
</ul>
";
$lang['param_event_id'] = "Utilisé uniquement avec les actions <em>detail</em>, <em>register</em> et <em>registrationslist</em> - ID de l'événement auquel rattacher l'action";
$lang['param_detailpage'] = "ID de la page à utiliser pour afficher le détail d'un évènement - Si non précisé, la page définie dans les préférences ou la page en cours est utilisée";
$lang['param_category_id'] = "Utilisé uniquement avec l'action <em>default</em>, affiche les évènements liés à le ou les ID(s) de catégorie(s) spécifié(s) - Vous pouvez indiquer plusieurs ID en les séparant par des virgules. Exemple : <em>category_id='2,3'</em>";
$lang['param_category'] = "Utilisé uniquement avec l'action <em>default</em>, affiche les évènements liés à le ou les nom(s) de catégorie(s) spécifié(s) - Vous pouvez indiquer plusieurs noms en les séparant par des virgules. Exemple : <em>category='Spectacles,Formations'</em>";
$lang['param_status'] = "Utilisé uniquement avec l'action <em>default</em>, n'affiche que les évènements ayant le statut spécifié.
<br />Valeurs possibles :
<ul>
	<li>published : évènements publiés - paramètre par défaut</li>
	<li>draft : évènements non publiés</li>
	<li>all : tous les évènements quel que soient leurs statuts</li>
</ul>";
$lang['param_show'] = "Utilisé uniquement avec l'action <em>default</em>, affiche les évènements selon un critère de date.
<br />Valeurs possibles :
<ul>
	<li>all : tous les évènements (passés, en cours, à venir) - paramètre par défaut</li>
	<li>upcoming : évènements en cours et à venir</li>
	<li>past : évènements passés</li>
	<li>currently : évènements en cours (date de début < date actuelle < date de fin)</li>
	<li>custom : vous permet de spécifier une date/heure de début et de fin - Voir les paramètres start_datetime et end_datetime</li>
</ul>";
$lang['param_start_datetime'] = "Utilisé uniquement avec l'action <em>default</em> et lorsque show='custom', vous permet de définir une date/heure de début pour l'affichage de la liste des évènements. La date et l'heure doivent être au format standard : YYYY-MM-DD HH:MM:SS
<br />Exemples :
<ul>
	<li>date/heure complètes : start_datetime='2011-10-07 15:00:00'</li>
	<li>date seule : start_datetime='2011-10-07'</li>
	<li>année seule : start_datetime='2011' - correspond à 2011-01-01</li>
</ul>";
$lang['param_end_datetime'] = "Utilisé uniquement avec l'action <em>default</em> et lorsque show='custom', vous permet de définir une date/heure de fin pour l'affichage de la liste des évènements. La date et l'heure doivent être au format standard : YYYY-MM-DD HH:MM:SS - Voir le paramètre 'start_datetime' pour les exemples";
$lang['param_summarytemplate'] = "Utilisé uniquement avec l'action <em>default</em>, vous permet de préciser le gabarit à utiliser pour l'affichage de la liste des évènements - Si rien n'est spécifié, le gabarit par défaut sera utilisé";
$lang['param_detailtemplate'] = "Utilisé uniquement avec les actions <em>default</em> et <em>detail</em>, vous permet de préciser le gabarit à utiliser pour l'affichage d'un évènement - Si rien n'est spécifié, le gabarit par défaut sera utilisé";
$lang['param_registrationtemplate'] = "Utilisé uniquement avec l'action <em>register</em>, vous permet de préciser le gabarit à utiliser pour l'affichage du formulaire d'inscription à un évènement - Si rien n'est spécifié, le gabarit par défaut sera utilisé";
$lang['param_registrationslisttemplate'] = "Utilisé uniquement avec l'action <em>registrationslist</em>, vous permet de préciser le gabarit à utiliser pour l'affichage de la liste des inscrits à un évènement - Si rien n'est spécifié, le gabarit par défaut sera utilisé";
$lang['param_myevents'] = "true/false - Utilisé uniquement avec l'action <em>default</em>, affiche uniquement les évènements auxquels l'utilisateur FEU actuellement connecté est enregistré. Si personne n'est connecté, le paramètre est ignoré - Astuce : utilisez cette option sur une page de type 'Contenu protégé'";
$lang['param_sortby'] = "Utilisé uniquement avec l'action <em>default</em>, cette option vous permet de définir un critère de tri des évènements. Valeurs possibles :
<ul>
	<li>name : <em>default</em> - Tri par nom d'évènement</li>
	<li>description : tri selon la description</li>
	<li>start_datetime : tri selon la date/heure de début</li>
	<li>end_datetime : tri selon la date/heure de fin</li>
	<li>capacity : tri selon la capacité (note : illimitée vaut 0)</li>
	<li>price : tri selon le prix</li>
	<li>capacity_per_feu : tri selon le nombre d'inscriptions autorisées par utilisateur (note : illimitée vaut 0)</li>
</ul>";
$lang['param_sortorder'] = "ASC / DESC - Utilisé uniquement avec l'action <em>default</em>, cette option vous permet de définir un ordre de tri ascendant ou descendant relatif au critère <em>sortby</em>";

# Help
$lang['help'] = "
<h3>Que fait ce module ?</h3>
<p>Le module Evènements et Manifestations (EventsManager) vous permet de gérer tous types d'événements : spectacles, formations, salons, exposition, .. bref, tout ce qui a une date de début et de fin. Vous pourrez en créer, les afficher et gérer des inscriptions de membres FEU (FrontEndUsers).</p>
<p>Pour l'instant, ce module en est à ses premières versions et devrait proposer plus de fonctions à l'avenir (Intégration E-commerce, champs personnalisés, etc.)</p>
<h3>Comment l'utiliser ?</h3>
<ul>
<li>Créez un ou plusieurs évènements dans la partie admin du module</li>
<li>Ajoutez la balise : <strong>{EventsManager}</strong>
à une page pour afficher la liste des évènements à venir</li>
</ul>
<h3>Afficher uniquement les évènements auxquels l'utilisateur FEU connecté s'est enregistré</h3>
<p>Utilisez simplement le paramètre <strong>myevents</strong> de cette manière : <strong>{EventsManager myevents=true}</strong></p>
<p>Pour personnaliser d'avantage votre liste d'évènements, consultez la liste des options disponibles en bas de cette page. Vous pouvez aussi personnaliser vos gabarits d'affichage grâce à Smarty.</p>

<h3>Afficher la liste des inscriptions pour un évènement particulier</h3>
<p>Utilisez simplement l'action <strong>registrationslist</strong> de cette manière : <strong>{EventsManager action='registrationslist' event_id=5}</strong> où <strong>id_event</strong> correspond à l'id de l'évènement pour lequel afficher les inscrits</p>
<p>Pour personnaliser d'avantage votre liste d'évènements, consultez la liste des options disponibles en bas de cette page. Vous pouvez aussi personnaliser vos gabarits d'affichage grâce à Smarty.</p>

<h3>Sponsors</h3>
<p>Le développement de ce module a été sponsorisé par :
	<ul>
		<li>Alternative DG : <a href='www.alternativedg.com'>www.alternativedg.com</a></li>
		<li>TO : <a href='www.t-o.be'>www.t-o.be</a></li>
	</ul>
</p>
<p>Merci beaucoup à ces entreprises ! Si vous souhaitez sponsoriser le développement d'une fonction, envoyez-moi un e-mail : <a href='mailto:contact@airelibre.fr'>contact@airelibre.fr</a></p>

<h3>Assistance</h3>
<p>Ce module est fourni sans aucune assistance commerciale. Cependant,
vous pouvez tout de même obtenir de l'aide :</p>
<ul>
<li>Pour obtenir la dernière version de ce module, consulter la FAQ,
ou signaler un bug, rendez-vous sur la Developers Forge de CMS Made
Simple, et faites une recherche sur 'EventsManager'.</li>
<li>Pour obtenir une assistance commerciale, contactez par mail
Mathieu Muths, l'auteur de ce module : <a
href=\"mailto:mailto:contact@airelibre.fr\">contact@airelibre.fr</a></li>
<li>Les Forums CMS Made Simple sont aussi une préciseuse source
d'informations.</li>
<li>Suivez l'auteur sur Twitter : <a href='http://www.twitter.com/aire_libre' target='_blank'>http://www.twitter.com/aire_libre</a></li>
<li>Si vous êtes francophone, n'hésitez pas à consulter le Forum CMS
Made Simple Français où l'auteur du module est régulièrement présent
(pseudo : <strong>airelibre</strong>)</li>
</ul>
<h3>Copyright et Licence</h3>
<p>Copyright © 2011, Mathieu Muths (contact@airelibre.fr) - Tous droits
réservés</p>
<p>Ce programme est un logiciel libre - Vous pouvez le redistribuer
et/ou le modifier selon les termes de la GNU GPL (General Public
Licence) telle que diffusée par la Free Software Fundation en version 2
ou supérieure.</p>
<p>De plus, et telle une exception supplémentaire de la licence GPL, ce
logiciel est un module pour CMS Made Simple. Vous ne devez donc pas
l'utiliser dans une version non conforme à la GPL de CMS Made Simple,
ou dans n'importe quelle version de CMS Made Simple qui n'indiquerait
pas clairement dans son administration que le site a été conçu avec CMS
Made Simple.</p>
<p>Ce module a été distribué dans l'espoir d'être utile, mais sans
AUCUNE GARANTIE. Il vous appartient de le tester avant toute mise en
production, que ce soit dans le cadre d'une nouvelle installation ou
d'une mise à jour du module. L'auteur du module ne pourrait être tenu
pour responsable de tout dysfonctionnement du site provenant de ce
module. Pour plus d'informations, <a
href=\"http://www.gnu.org/licenses/licenses.html#GPL\" target=\"_blank\">consultez
la licence GNU GPL</a>.</p>
<p>L'icône du module a été créée par Everaldo. Vous pouvez télécharger ses belles icônes sur : <a href='http://www.everaldo.com' target='_blank'>www.everaldo.com</a></p>";
?>