<?php

$lang['friendlyname'] = "Events Manager";
$lang['admindescription'] = "A module to manage Events like shows, training lessons, etc.. and allow your FEUsers to register to them";

$lang['postinstall'] = "The EventsManager module has been successfully installed. You can access it in the 'Content' Menu. Be sure to set 'Use EventsManager' permissions to use this module!";
$lang['preuninstall'] = "Really? You're sure you want to uninstall the EventsManager module and all it's data?";
$lang['postuninstall'] = "The EventsManager module has been uninstalled.";

# General
$lang['name'] = 'Name';
$lang['published'] = 'Published';
$lang['draft'] = 'Draft';

# Tabs
$lang['events'] = 'Events';
$lang['preferences'] = 'Preferences';
$lang['categories'] = 'Categories';
$lang['fielddefs'] = 'Fields definitions';
$lang['mails'] = 'E-Mails';

$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['areyousure'] = 'Are you sure?';
$lang['copy'] = 'Copy';

# Events
$lang['event'] = 'Event';
$lang['addevent'] = 'Add a new event';
$lang['editevent'] = 'Edit an event';
$lang['copyevent'] = 'Copy an event';
$lang['eventdescription'] = 'Description';
$lang['start_datetime'] = 'Start date and time';
$lang['end_datetime'] = 'End date and time';
$lang['reg_start_datetime'] = 'Registration period start date and time';
$lang['reg_end_datetime'] = 'Registration period end date and time';
$lang['number_of_regusers'] = 'Number of registrations';
$lang['capacity'] = 'Capacity';
$lang['unlimitedcapacity'] = 'Unlimited capacity';
$lang['unlimited'] = 'Unlimited';
$lang['capacity_per_feu'] = "Max number of registrations per FrontEnd User";
$lang['unlimitedcapacity_per_feu'] = "Unlimited number of registrations for each FrontEnd User";
$lang['view_regusers'] = 'View registered users';
$lang['manage_regusers'] = 'Manage registrations';
$lang['manage'] = 'Manage';
$lang['allow_registration'] = 'Allow registrations';
$lang['allow_registration_detail'] = 'Allow FEUsers to register to that event';
$lang['registration_allowed'] = 'Registrations allowed';
$lang['registrations_not_allowed'] = 'Registrations not allowed';
$lang['toggle_registration'] = 'Allow/disallow registrations';
$lang['toggle_status'] = 'Publish/Hide';
$lang['status'] = 'Visibiliy status';
$lang['event_cancelled'] = 'Cancelled event';
$lang['cancelled'] = 'Cancelled';
$lang['allowed_feu_groups'] = "FEUsers groups allowed to register to that event - If none selected, all FEUsers will be able to register";
$lang['back_toeventslist'] = 'Back to the events list';
$lang['price'] = 'Price';
$lang['event_upcoming'] = 'Upcoming';
$lang['event_past'] = 'Past';
$lang['event_inprogress'] = 'In progress';
$lang['reg_period'] = 'Registration period';
$lang['limited_reg_period_detail'] = 'Limit the registration period. If this is checked, the users will only be able to register to an event from start to end datetimes set below. Uncheck to allow registrations all the time';

# Event edit tabs
$lang['generalinfo_tab'] = 'Event infos';
$lang['registrations_tab'] = 'Registration options';

# Registration
$lang['nb_persons'] = 'Number of users';
$lang['register'] = 'Register to that event';
$lang['modify_registration'] = 'Modify the registration';
$lang['addregistration'] = 'Add a new registration';
$lang['editregistration'] = 'Edit a registration';
$lang['cancelregister'] = 'Cancel the registration';
$lang['registrations_for'] = 'Registrations to the event';
$lang['feuname'] = 'Username';
$lang['regdate'] = 'Registration date';
$lang['export_regusers'] = 'Export the registrations';
$lang['export_regusers_file'] = 'To CSV';
$lang['exportencoding'] = 'Registrations CSV file encoding';
$lang['no_registrations'] = 'There are no registrations for this event';
$lang['show_past_events'] = 'Show past events in list?';

# NMS Export
$lang['export_regusers_nms'] = 'Export to a Newsletter Made Simple list';
$lang['export_regusers_nms_small'] = 'To Newsletter';
$lang['export_options'] = "Export options";
$lang['nmslist'] = "Newsletter list(s) to which export. You can choose one or more";
$lang['note_export_nms'] = "Note: users still registered to a list won't be registered again";
$lang['emailproperty'] = "FEU property that contains the E-mail address";
$lang['nameproperty'] = "FEU property that contains the name for NMS";
$lang['use_username'] = "Use the username";
$lang['export_feu_groups'] = "FEU groups to export";
$lang['note_export_nms_feugroups'] = "This is useful if you want to export the users that belongs only to some FEU groups. Use this option if your FEU groups doesn't have the same e-mail and name properties for Newsletter Made Simple";
$lang['nms_export_done'] = '%s users have been exported to the newsletter list(s)';
$lang['nms_who_export'] = 'Which users export';
$lang['nms_who_export_all'] = 'All: registered and cancelled';
$lang['nms_who_export_registered'] = 'Only registered users';
$lang['nms_who_export_cancelled'] = 'Only cancelled users';
$lang['note_export_nms_who'] = 'You can specifically export All / Only registered / cancelled registrations users. For instance, this will allow you to send an e-mail to cancelled registered users to revive them';
$lang['delete_listuser_link'] = 'Delete all the NMS users from the selected lists';
$lang['delete_listuser_link_label'] = 'Check this box if you want to delete ALL the users from the selected export lists. This is useful if you want to get a clean list before exporting, but please be careful because this cannot be cancelled!';

# Categories
$lang['category'] = 'Category';
$lang['addcategory'] = 'Add an events category';
$lang['editcategory'] = 'Edit an events category';

# E-Mails # General
$lang['mail_sendtouser_label'] = 'Send an e-mail to the registered user (note: this will not work if the user has no e-mail address in his FEU account)';
$lang['mail_sendtoother_label'] = 'Send an e-mail to those e-mail addresses (comma seperated list)';
$lang['mail_template_help'] = 'Available objects:<br />
	<strong>{$event}</strong> : the event informations ($event->name, $event->description, $event->extrafieldsbyname.myfield->value, etc.)<br />
	<strong>{$registration}</strong> : informations about the registration ($registration->nb_persons)<br />
	<strong>{$feu_user}</strong> : the FEU user';
$lang['mail_subject'] = 'Mail subject';

#E-Mails # New registration
$lang['mail_newregistration_title'] = 'Mail sent on a new registration';
$lang['email_to_who_newregistration'] = 'Who must get an e-mail on a new registration?';
$lang['mail_newregistration_template'] = 'New registration e-mail template';

# E-mails # Cancel registration
$lang['mail_cancelregistration_title'] = 'Mail sent on a registration cancellation';
$lang['email_to_who_cancelregistration'] = 'Who must get an e-mail on registration cancellation?';
$lang['mail_cancelregistration_template'] = 'Registration cancellation e-mail template';


# Errors
$lang['error_emptyname'] = "Please give a name";
$lang['error_nameused'] = "Another category with the same name already exists";
$lang['error_db'] = "An error occured during the insertion/update process in the database";
$lang['error_registration_notallowed'] = "You cannot register to that event";
$lang['error_registration_notallowed_badperiod'] = "You cannot register to that event: you're too early or too late!";
$lang['error_notconnected'] = "You must be connected to register";
$lang['error_paramsmissing'] = "One or more parameters are missing";
$lang['error_nolistselected'] = "Please select at least one newsletter list";
$lang['error_no_nb_users'] = "Please give a number of persons";

# Messages
$lang['message_registration_ok'] = "Your registration has been saved.";

# Prefs
$lang['generaloptions'] = 'General preferences';
$lang['label_menuname'] = "Custom module name in the admin menu - If none, the default name will be used";
$lang['label_showtabs'] = 'Show tabs in event edit view (event infos and registrations infos will be splitted)';

$lang['urloptions'] = "URLs preferences";
$lang['urlprefix'] = "Prefix to use for every URLs in the module";
$lang['registerurlprefix'] = "Prefix to use for every registration link";

$lang['eventdetailoptions'] = "Event details preferences";
$lang['label_dflt_detailpage'] = "Event detail default page. If 'None' is selected, the current page will be used";

$lang['eventregistrationoptions'] = "Event registration preferences";
$lang['label_dflt_registrationpage'] = "Registration default page. If 'None' is selected, the current page will be used";
$lang['label_overquota_behaviour'] = "Behaviour of the registration system when the user tries to registered a number of places that is over quota";
$lang['reg_with_limit'] = 'Do the registration, but limit the number to the max per user';
$lang['ask_confirm'] = 'Show a message and ask for a confirmation';

$lang['reguserslistoptions'] = 'Event registrations list / export preferences';
$lang['displayed_feu_properties'] = "FEU properties displayed in the registrations list and in the export file";
$lang['title_export_cancelled'] = 'Export cancelled registrations in CSV files?';
$lang['export_cancelled'] = 'If checked, the  cancelled registrations will be exported';

$lang['feuloginoptions'] = "FrontEnd Users login preferences";
$lang['dflt_feuloginpage'] = "Default FEU login page:";
$lang['dflt_feuloginpage_help'] = "If the user starts a registrations, and he's not connected, he will be redirected to this page. See help for more details";

# Templates / Admin
$lang['summarytemplates'] = 'Summary templates';
$lang['detailtemplates'] = 'Detail templates';
$lang['registrationtemplates'] = "Registration templates";
$lang['registrationslisttemplates'] = "FE Registrations list templates";

$lang['summarytemplate_addedit'] = "Add / Edit an events summary template";
$lang['detailtemplate_addedit'] = "Add / Edit an event details template";
$lang['registrationtemplate_addedit'] = "Add / Edit an event registration form template";
$lang['registrationslisttemplate_addedit'] = "Add / Edit a frontend registrations list template";

# CMSMS Events help
# Events
$lang['eventdesc_EventsManagerEventAdded'] = 'Sent when an event is added/created';
$lang['eventhelp_EventsManagerEventAdded'] = '<p>Sent when an event is added/created</p>
<h4>Parameters</h4>
<ul>
	<li>"event_id" - ID of the new event</li>
</ul>
<p>You can then use something like :</p>
<code>
	$the_event = new EventsManager_event($params[$event_id]);<br>
	echo $the_event->name;<br>
	print_r($the_event);<br><br>
</code>
<p>to get the event in your PHP code</p>';
$lang['eventdesc_EventsManagerEventEdited'] = 'Sent when an event is edited';
$lang['eventhelp_EventsManagerEventEdited'] = '<p>Sent when an event is edited</p>
<h4>Parameters</h4>
<ul>
	<li>"event_id" - ID of the edited event</li>
</ul>
<p>You can then use something like :</p>
<code>
	$the_event = new EventsManager_event($params[$event_id]);<br>
	echo $the_event->name;<br>
	print_r($the_event);<br><br>
</code>
<p>to get the event in your PHP code</p>';
$lang['eventdesc_EventsManagerEventDeleted'] = 'Sent when an event is deleted';
$lang['eventhelp_EventsManagerEventDeleted'] = '<p>Sent when an event is deleted</p>
<h4>Parameters</h4>
<ul>
	<li>"event_id" - ID of the deleted event</li>
</ul>';
# Categories
$lang['eventdesc_EventsManagerCategoryAdded'] = 'Sent when an events category is added/created';
$lang['eventhelp_EventsManagerCategoryAdded'] = '<p>Sent when an events category is added/created</p>
<h4>Parameters</h4>
<ul>
	<li>"category_id" - ID of the new category</li>
</ul>
<p>You can then use something like :</p>
<code>
	$the_category = new EventsManager_category($params[$category_id]);<br>
	echo $the_category->name;<br>
	print_r($the_category);<br><br>
</code>
<p>to get the events category in your PHP code</p>';
$lang['eventdesc_EventsManagerCategoryEdited'] = 'Sent when an events category is edited';
$lang['eventhelp_EventsManagerCategoryEdited'] = '<p>Sent when an events category is edited</p>
<h4>Parameters</h4>
<ul>
	<li>"category_id" - ID of the edited category</li>
</ul>
<p>You can then use something like :</p>
<code>
	$the_category = new EventsManager_category($params[$category_id]);<br>
	echo $the_category->name;<br>
	print_r($the_category);<br><br>
</code>
<p>to get the events category in your PHP code</p>';
$lang['eventdesc_EventsManagerCategoryDeleted'] = 'Sent when an events category is deleted';
$lang['eventhelp_EventsManagerCategoryDeleted'] = '<p>Sent when an events category is deleted</p>
<h4>Parameters</h4>
<ul>
	<li>"id_category" - ID of the deleted category</li>
</ul>';
# Registrations
$lang['eventdesc_EventsManagerRegistrationAdded'] = 'Sent when a registration is added/created';
$lang['eventhelp_EventsManagerRegistrationAdded'] = '<p>Sent when a registration is added/created</p>
<h4>Parameters</h4>
<ul>
	<li>"registration_id" - ID of the new registration</li>
</ul>
<p>You can then use something like :</p>
<code>
	$the_registration = new EventsManager_registration($params[$registration_id]);<br>
	echo $the_registration->nb_persons;<br>
	print_r($the_registration);<br><br>
</code>
<p>to get the registration in your PHP code</p>';
$lang['eventdesc_EventsManagerRegistrationEdited'] = 'Sent when a registration is edited';
$lang['eventhelp_EventsManagerRegistrationEdited'] = '<p>Sent when a registration is edited</p>
<p>Important note: for the moment, a <strong>deleted</strong> registration is a modified registration where <strong>nb_persons is 0</strong>. So if you want to test if a registration is deleted, you should test this value</p>
<h4>Parameters</h4>
<ul>
	<li>"registration_id" - ID of the modified registration</li>
</ul>
<p>You can then use something like :</p>
<code>
	$the_registration = new EventsManager_registration($params[$registration_id]);<br>
	echo $the_registration->nb_persons;<br>
	print_r($the_registration);<br><br>
</code>
<p>to get the registration in your PHP code</p>';

# Help with params
$lang['param_action'] = "Specify the behaviour of the module. Possible values are:
<ul>
	<li><em>default</em> - Display a summary of events - You can use <em>action='summary'</em> too</li>
	<li>detail - Display a detail view of a single event - Use the 'event_id' parameter to specify the event to display</li>
	<li>register - Display a form to registrer to an event - Use the 'event_id' parameter to specify the event to which register</li>
	<li>registrationslist - Display the list of the registrations for an event - Use the 'event_id' parameter to specify the event (view help above)</li>
</ul>
";

$lang['param_event_id'] = "Useful only with the <em>detail</em>, <em>register</em> and <em>registrationslist</em> actions - Specifies the event to which the action is linked";
$lang['param_detailpage'] = "Page ID to use to display an event detail - If not filled, the current page, or the one defined in the preferences will be used";
$lang['param_category_id'] = "Useful only with the <em>default</em> action, this parameter will be used to display only the events that are in the specified category id - You can use a comma seperated list to specify multiples categories IDs. Example : <em>category_id='2,3'</em>";
$lang['param_category'] = "Useful only with the <em>default</em> action, this parameter will be used to display only the events that are in the specified category name - You can use a comma seperated list to specify multiples categories names. Example : <em>category_id='show,training'</em>";

$lang['param_status'] = "Useful only with the <em>default</em> action, this parameter specifies the status of events to display
<br />
Possible values are:
<ul>
	<li>published : published events - default param</li>
	<li>draft : draft events</li>
	<li>all : all the events</li>
</ul>";
$lang['param_show'] = "Useful only with the <em>default</em> action, thie parameter is used to display events according to their datetimes
<br />
Possible values are:
<ul>
	<li>all : all the events (past, current, upcoming) - default param</li>
	<li>upcoming : upcoming events</li>
	<li>past : past events</li>
	<li>currently : current events (start date < current date < end date)</li>
	<li>custom : specify a start and end date and time - See the start/end_datetime params</li>
</ul>";
$lang['param_start_datetime'] = "Useful only with the <em>default</em> action, and only when show='custom', this parameter defines the start date and time of the events to be displayed. Date and time must be in standard format : YYYY-MM-DD HH:MM:SS
<br />Examples:
<ul>
	<li>complete date/time: start_datetime='2011-10-07 15:00:00'</li>
	<li>date only: start_datetime='2011-10-07'</li>
	<li>year only: start_datetime='2011' - '2011' means 2011-01-01</li>
</ul>";
$lang['param_end_datetime'] = "Useful only with the <em>default</em> action, and only when show='custom', this parameter defines the end date and time of the events to be displayed. Date and time must be in standard format : YYYY-MM-DD HH:MM:SS - See the start_datetime param for examples";
$lang['param_summarytemplate'] = "Useful only with the <em>default</em> action, this parameter specifies the template that should be used for the summary mode display. If no value is specified, the default template of that type is used";
$lang['param_detailtemplate'] = "Useful only with the <em>detail</em> action, this parameter specifies the template that should be used for the detail mode display. If no value is specified, the default template of that type is used";
$lang['param_registrationtemplate'] = "Useful only with the <em>register</em> action, this parameter specifies the template that should be used for the register form display. If no value is specified, the default template of that type is used";
$lang['param_registrationslisttemplate'] = "Useful only with the <em>registrationslist</em> action, this parameter specifies the template that should be used for the registrations list display. If no value is specified, the default template of that type is used";
$lang['param_myevents'] = "true/false - Useful only with the <em>default</em> action, with this parameter you can display only the events the current logged in FEUser is registered to. If no one is connected, this parameter is simply ignored - Tip: use this option on a private FEU page";
$lang['param_sortby'] = "Useful only with the <em>default</em> action, this parameter specifies how to sort the events. Possible values:
<ul>
	<li>name : <em>default</em> - By event name</li>
	<li>description : by description</li>
	<li>start_datetime : by start date/time</li>
	<li>end_datetime : by end date/time</li>
	<li>capacity : order by event capacity (note: unlimited means 0)</li>
	<li>price : by price</li>
	<li>capacity_per_feu : order by capacity per FEU (note: unlimited means 0)</li>
</ul>";
$lang['param_sortorder'] = "ASC / DESC - Useful only with the <em>default</em> action, this parameter defines the sort order for the events. This parameter is used with <em>sortby</em>";
$lang['param_allow_notconnected'] = "Useful only with the <em>register</em> action. If this parameter value is 1, then the registration form will be displayed even if the user is not connected in FEU. See how this parameter is useful in the 'Examples and tips' help tab.";

# Help
$lang['main_help_tab'] = 'General Help';
$lang['examples_help_tab'] = 'Examples and tips';
$lang['help'] = "
<h3>What does this do?</h3>
<p>The EventsManager module is designed to manage Events like Shows, Training lessons, Conferences, ... and all kind of 'events' that does have a start and end date and time. You can create events, display them and handle FEU registrations.</p>
<p>For the time being, this module is under development and will provide more functions later (E-commerce, Fields definitions, etc.)</p>

<h3>How do I use it?</h3>
<ul>
	<li>Create one or more events in the admin</li>
	<li>Add the <strong>{EventsManager}</strong> tag to a page to display an upcoming events summary view</li>
</ul>

<h3>Sponsors</h3>
<p>The development of this module has been sponsored by :
	<ul>
		<li>Alternative DG: <a href='http://www.alternativedg.com'  target='_blank'>www.alternativedg.com</a></li>
		<li>TO: <a href='http://www.t-o.be' target='_blank'>www.t-o.be</a></li>
		<li>FUSE : <a  target='_blank' href='http://www.lightafuse.ca'>www.lightfuse.ca</a></li>
	</ul>
</p>
<p>Thanks to these companies! If you want to sponsor the development of a function, send me an e-mail: <a href='mailto:contact@airelibre.fr'>contact@airelibre.fr</a></p>

<h3>Support</h3>
<p>This module does not include commercial support. However there are a number of resources available to help you with it:</p>
<ul>
	<li>For the latest version of this module, FAQs or to file a bug report, please visit the cms made simple Developers Forge and do a search for 'EventsManager'</li>
	<li>To obtain commercial support, please send an email to the author Mathieu Muths (french prefered but I can speak english) : <a href=\"mailto:mailto:contact@airelibre.fr\">contact@airelibre.fr</a></li>
	<li>Follow the author on Twitter : <a href='http://www.twitter.com/aire_libre' target='_blank'>http://www.twitter.com/aire_libre</a></li>
	<li>Additional discussion of this module may also be found in the CMS Made Simple Forms.</li>
</ul>
<h3>Copyright et Licence</h3>
<p>Copyright © 2011, Mathieu Muths (contact@airelibre.fr) - All rights reserved</p>
<p>This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.</p>
<p>However, as a special exception to the GPL, this software is distributed as an addon module to CMS Made Simple. You may not use this software in any Non GPL version of CMS Made simple, or in any version of CMS Made simple that does not indicate clearly and obviously in its admin section that the site was built with CMS Made simple.</p>
<p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA Or <a
href=\"http://www.gnu.org/licenses/licenses.html#GPL\" target=\"_blank\">read it online</a></p>
<p>The module icon has be designed by Everaldo - You can download some nice icons on: <a href='http://www.everaldo.com' target='_blank'>www.everaldo.com</a></p>
";
$lang['help_examples'] = "
<h2>Only display the current logged in FEUser events</h2>
<p>Use the <strong>myevents</strong> parameter this way: <strong>{EventsManager myevents=true}</strong></p>
<p>You can customize your templates with some parameters and options (see below)</p>

<h2>Display the list of the registrations for an event</h2>
<p>Use the <strong>registrationslist</strong> action like that : <strong>{EventsManager action='registrationslist' event_id=5}</strong> where <strong>event_id</strong> is the id of the event you want to display the registrations.</p>
<p>You can customize your templates with some parameters and options (see below)</p>

<h2>How to handle registrations when the FEU user is not loggedin?</h2>
<p>Use the <strong>allow_notconnected</strong> param in the <em>register</em> action. This will display the registration form even if the user is not connected</p>
<p>Once the form submitted, the registration is stored in the current session, and the user is redirected to a page - defined in the option 'Default FEU login page' in the module prefs - where you can put your FEU login / SelfRegistration tags.</p>
<p>Finally, EventsManager detects the <em>OnLogin</em> CMSMS event, and creates the registrations once the user is logged in. Of course, the verifications about the remaining places / FEU groups are done before registering. E-Mails are then sent to the user/admin according to the e-mails options you defined (Email tab)</p>

<h2>How to send some e-mails on registration / cancellation / to all the registered users?</h2>
<p>There are many ways to do that:</p>
<h3>a. On registration / cancellation</h3>
<p>An e-mail can automatically be send, and you can customize it with a template. See the 'E-mails' tab in the module main admin page>/p>
<h3>b. How to send a mass e-mail to the registered / cancelled users</h3>
<p>As Newsletter Made Simple (NMS) is the perfect module for that, there is no mass-mail function in EventsManager. To use EventsManager with NMS :</p>
<p>
	<ol>
		<li>Configure NMS and create a list, for instance 'My first event list'</li>
		<li>Go to EventsManager, and click on 'To Newsletter' on an event ('Export the registrations' column in the admin events list)</li>
		<li>Here you can configure the export. The most important function to easily manage your lists is <strong>Which users export</strong>. Here you can choose what kind of registrations you want to export to NMS.</li>
		<li>Another important function to keep your lists up to date: you can delete all the users of the selected list(s) with the last checkbox. Just be careful because this cannot be reverted</li>
	</ol>
</p>
<h3>c. Use the CMSMS events</h3>
<p>You can use the various technical CMSMS events send by the EventsManager. Go to Extensions / Event Manager to see what you can use</p>
";
?>