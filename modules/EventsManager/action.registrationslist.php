<?php
if (!isset($gCms)) exit;

if (isset($params['event_id']))
{
	$event = new EventsManager_event($params['event_id']);
	$registrations = $event->get_registrations();
	
	$registrations_smarty = array();

	if ($registrations)
	{
		foreach ($registrations as $registration)
		{
			$registrations_smarty[] = $registration->get_smarty_object();
		}
	}
	
	$smarty->assign('registrations', $registrations_smarty);
	
	$template = $this->GetPreference('dflt_registrationslist_template');
	if  (!empty($params['registrationslisttemplate']))
		$template = $params['registrationslisttemplate'];
	echo $this->ProcessTemplateFromDatabase('registrationslist_' . $template);
}
?>