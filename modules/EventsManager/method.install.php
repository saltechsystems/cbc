<?php
if (!isset($gCms)) exit;

// Database operations
$db = cmsms()->GetDb();

$dict = NewDataDictionary($db);
$taboptarray = array('mysql' => 'TYPE=MyISAM');

// table : module_events **********************************
$flds = "
	id I KEY AUTO,
	id_category I, 
	status C(40),
	name C(255) NOT NULL,
	url C(255),
	description X,
	start_datetime " . CMS_ADODB_DT . ",
	end_datetime " . CMS_ADODB_DT . ",
	capacity I,
	allow_registration I1,
	allowed_feu_groups C(200),
	price F,
	capacity_per_feu I,
	create_datetime " . CMS_ADODB_DT . ",
	modify_datetime " .CMS_ADODB_DT . ",
	limited_reg_period I1 DEFAULT '0', 
	reg_start_datetime ".CMS_ADODB_DT . ",
	reg_end_datetime ".CMS_ADODB_DT . ",
	cancelled I";
$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_events", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);

// table : module_events_categories **********************************
$flds = "
	id I KEY AUTO,
	name C(255) NOT NULL,
	description X,
	create_datetime " . CMS_ADODB_DT . ",
	modify_datetime " .CMS_ADODB_DT;
$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_events_categories", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);

# table : module_events_registrations ********************************
$flds = "
	id I KEY AUTO,
	id_event I,
	id_user I,
	nb_persons I,
	create_datetime " . CMS_ADODB_DT . ",
	modify_datetime " .CMS_ADODB_DT;
$sqlarray = $dict->CreateTableSQL(cms_db_prefix()."module_events_registrations", $flds, $taboptarray);
$dict->ExecuteSQLArray($sqlarray);

# Templates ***************************************************
# Summary template
$fn = cms_join_path(dirname(__FILE__),'templates','orig_summarytemplate.tpl');
if( file_exists( $fn ) )
{
	$template = file_get_contents( $fn );
	$this->SetPreference('orig_summary_template',$template);
	$this->SetTemplate('summary_Sample',$template);
	$this->SetPreference('dflt_summary_template','Sample');
}

# Detail template
$fn = cms_join_path(dirname(__FILE__),'templates','orig_detailtemplate.tpl');
if( file_exists( $fn ) )
{
	$template = file_get_contents( $fn );
	$this->SetPreference('orig_detail_template',$template);
	$this->SetTemplate('detail_Sample',$template);
	$this->SetPreference('dflt_detail_template','Sample');
}

# Registration template
$fn = cms_join_path(dirname(__FILE__),'templates','orig_registertemplate.tpl');
if( file_exists( $fn ) )
{
	$template = file_get_contents( $fn );
	$this->SetPreference('orig_registration_template',$template);
	$this->SetTemplate('registration_Sample',$template);
	$this->SetPreference('dflt_registration_template','Sample');
}

# Registrations list template
$fn = cms_join_path(dirname(__FILE__),'templates','orig_registrationslisttemplate.tpl');
if( file_exists( $fn ) )
{
	$template = file_get_contents( $fn );
	$this->SetPreference('orig_registrationslist_template',$template);
	$this->SetTemplate('registrationslist_Sample',$template);
	$this->SetPreference('dflt_registrationslist_template','Sample');
}

# Mails templates
$fn = cms_join_path(dirname(__FILE__),'templates','orig_newregistrationmailtemplate.tpl');
if( file_exists( $fn ) )
{
	$template = file_get_contents( $fn );
	$this->SetTemplate('newregistrationmail_Sample',$template);
}
$fn = cms_join_path(dirname(__FILE__),'templates','orig_cancelregistrationmailtemplate.tpl');
if( file_exists( $fn ) )
{
	$template = file_get_contents( $fn );
	$this->SetTemplate('cancelregistrationmail_Sample',$template);
}

// Permissions
$this->CreatePermission('Use EventsManager', 'Use EventsManager');

// Preferences
$this->SetPreference('menuname', '');
$this->SetPreference('dflt_detailpage', '-1');
$this->SetPreference('dflt_registrationpage', '-1');
$this->SetPreference('dflt_feuloginpage', '-1');

$this->SetPreference('newregistration_mail_sendtouser', 0);
$this->SetPreference('newregistration_mail_sendtoother', 0);
$this->SetPreference('newregistration_mail_sendtoother_mails', '');

$this->SetPreference('cancelregistration_mail_sendtouser', 0);
$this->SetPreference('cancelregistration_mail_sendtoother', 0);
$this->SetPreference('cancelregistration_mail_sendtoother_mails', '');

// Mail subjects
$this->SetPreference('newregistration_mail_subject', 'New registration to the event {$event->name}');
$this->SetPreference('cancelregistration_mail_subject', 'Registration cancelled for the event {$event->name}');

// Some other prefs
// By default, do the registration when a user enters an over-quota number of registrations
$this->SetPreference('overquota_behaviour', 'reg_with_limit');
// Export cancelled users in CSV
$this->SetPreference('export_cancelled', 1);
// Show tabs in event edit window
$this->SetPreference('showtabs', 1);

// CMSMS Events
$this->CreateEvent('EventsManagerEventAdded');
$this->CreateEvent('EventsManagerEventEdited');
$this->CreateEvent('EventsManagerEventDeleted');

$this->CreateEvent('EventsManagerCategoryAdded');
$this->CreateEvent('EventsManagerCategoryEdited');
$this->CreateEvent('EventsManagerCategoryDeleted');

$this->CreateEvent('EventsManagerRegistrationAdded');
$this->CreateEvent('EventsManagerRegistrationEdited');

// Handle the registration after FEU login
$this->AddEventHandler('FrontEndUsers', 'OnLogin', true);

?>