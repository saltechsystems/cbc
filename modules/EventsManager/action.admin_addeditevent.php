<?php
if (!isset($gCms)) exit;
if (!$this->HasPermission()) exit;

/* Prepare the display */
$errors = array();
$messages = array();
$this->SetCurrentTab('events');

/* Customfields managed by AireLibs */
$FieldsManager = new AireFieldsManager();

// Add or edit ?
if (isset($params['id_event']))
{
	$mode = 'edit';
	$id_event=$params['id_event'];
}
else
{
	$mode = 'add';
	$id_event=false;
}

$event = new EventsManager_event($id_event);

// The event is loaded, but if it's a copy, we have to tell the system that it's not a modification but a creation
if (isset($params['copy']))
{
	$mode = 'copy';
	$event->id = false;
}
/******************************************************************************
// Submit */
if (isset($params['submit']))
{
	// Get the params
	$event->load_from_array($params);
	// Specific operations
	//$event->name = trim($params['name']);
	//$event->description = trim($params['description']);
	$event->start_datetime = trim($params['start_date']) . ' ' . trim($params['start_time'] . ':00');
	$event->end_datetime = trim($params['end_date']) . ' ' . trim($params['end_time'] . ':00');
	$event->reg_start_datetime = trim($params['reg_start_date']) . ' ' . trim($params['reg_start_time'] . ':00');
	$event->reg_end_datetime = trim($params['reg_end_date']) . ' ' . trim($params['reg_end_time'] . ':00');

	if (isset($params['url']))
		$event->url = trim($params['url']);
	//$event->price = trim($params['price']);

	if (!isset($params['cancelled']))
		$event->cancelled=0;
	
	// Manage the capacity
	if (isset($params['capacity_unlimited']) OR trim($params['capacity_input']) == '')
		$event->capacity = 0;
	else
		$event->capacity = trim($params['capacity_input']);
	
	// Allow registration ?
	if (isset($params['allow_registration']))
		$event->allow_registration = 1;
	else
		$event->allow_registration = 0;
	// Use reg period ?
	if (isset($params['limited_reg_period']))
		$event->limited_reg_period = 1;
	else
		$event->limited_reg_period = 0;
		
	// FEU Groups allowed to register
	if (isset($params['allowed_feu_groups']))
	{
		$tmp = implode(',', $params['allowed_feu_groups']);
		$event->allowed_feu_groups = $tmp;
	}
	else
		$event->allowed_feu_groups = '';
		
	// Manage the capacity allowed for each FEU account
	if (isset($params['capacity_per_feu_unlimited']) OR trim($params['capacity_per_feu_input']) == '')
		$event->capacity_per_feu = 0;
	else
		$event->capacity_per_feu = trim($params['capacity_per_feu_input']);
	
	if (empty($event->name))
		$errors[] = $this->Lang('error_emptyname');
	else
	{
		// Ready - Just have to store the actual id to see if it's an addition or update
		$id_before_addedit = $event->id;
		
		// Name is OK - We can do the insert / update in db
		$res = $event->addedit();
		
		if ($res)
		{
			// Send the event
			$parms['event_id'] = $event->id;
			
			if ($id_before_addedit)
				$this->SendEvent('EventsManagerEventEdited', $parms);
			else
				$this->SendEvent('EventsManagerEventAdded', $parms);

			// CustomFields management
			$FieldsManager->store_fields_from_params($id, $params, $event->id);
			
			$this->RedirectToTab($id);
		}
		else
			$errors[] = $this->Lang('error_db');
	}
}
elseif (isset($params['cancel']))
	$this->RedirectToTab($id);

/******************************************************************************
// Assign to smarty */

/* Form general vars */
$smarty->assign('form_start', $this->CreateFormStart($id, 'admin_addeditevent', $returnid, 'POST', 'multipart/form-data'));
$smarty->assign('submit', $this->CreateInputSubmit($id, 'submit', lang('submit')));
$smarty->assign('cancel', $this->CreateInputSubmit($id, 'cancel', lang('cancel')));
$smarty->assign('form_end', $this->CreateFormEnd());

// Hidden values
$hidden = '';
if ($mode == 'edit')
	$hidden .= $this->CreateInputHidden($id, 'id_event', $event->id);
$smarty->assign('hidden', $hidden);

/* Fields */
$smarty->assign('title', $this->Lang($mode . 'event'));

// Name
$smarty->assign('name_label', $this->CreateLabelForInput($id, 'name', lang('name')));
$smarty->assign('name_input', $this->CreateInputText($id, 'name', $event->name, 40, 255));

// URL - TODO
$smarty->assign('url_label', $this->CreateLabelForInput($id, 'url', 'URL'));
$smarty->assign('url_input', $this->CreateInputText($id, 'url', $event->url, 60, 255));

// Price
$smarty->assign('price_label', $this->CreateLabelForInput($id, 'price', $this->Lang('price')));
$smarty->assign('price_input', $this->CreateInputText($id, 'price', $event->price, 20, 30));

// Status
$status_drop = array($this->Lang('published')=>'published', $this->Lang('draft')=>'draft');
$smarty->assign('status_label', $this->Lang('status'));
$smarty->assign('status_input', $this->CreateInputDropdown($id, 'status', $status_drop, '', $event->status));

// Cancelled ?
$smarty->assign('cancelled_label', $this->CreateLabelForInput($id, 'cancelled', $this->Lang('event_cancelled')));
$smarty->assign('cancelled_input', $this->CreateInputCheckbox($id, 'cancelled', 1, $event->cancelled, 'id='.$id.'cancelled'));

// Description
$smarty->assign('description_label', lang('description'));
$smarty->assign('description_input', $this->CreateTextArea(1, $id, $event->description, 'description'));

// Category list
$categories_dropdown = EventsManager_categories_ops::get_categories_for_dropdown(true);
$smarty->assign('category_label', $this->CreateLabelForInput($id, 'id_category', $this->Lang('category')));
$smarty->assign('category_input', $this->CreateInputDropdown($id, 'id_category', $categories_dropdown,'', $event->id_category));

// Start date and hour
$start_datetime = explode(' ', $event->start_datetime);
$start_date = $start_datetime[0];
$start_time = explode(':', $start_datetime[1]);
$start_time = $start_time[0] . ':' . $start_time[1];
$smarty->assign('start_datetime_label', $this->CreateLabelForInput($id, 'start_date', $this->Lang('start_datetime')));
$smarty->assign('start_date_input', $this->CreateInputText($id, 'start_date', $start_date));
$smarty->assign('start_time_input', $this->CreateInputText($id, 'start_time', $start_time, '10'));

// End date and hour
$end_datetime = explode(' ', $event->end_datetime);
$end_date = $end_datetime[0];
$end_time = explode(':', $end_datetime[1]);
$end_time = $end_time[0] . ':' . $end_time[1];
$smarty->assign('end_datetime_label', $this->CreateLabelForInput($id, 'end_datetime', $this->Lang('end_datetime')));
$smarty->assign('end_date_input', $this->CreateInputText($id, 'end_date', $end_date));
$smarty->assign('end_time_input', $this->CreateInputText($id, 'end_time', $end_time, '10'));

// Capacity - Input text and checkbox if unlimited (in db means 0)
$smarty->assign('capacity_label', $this->CreateLabelForInput($id, 'capacity_input', $this->Lang('capacity')));
$smarty->assign('capacity_input', $this->CreateInputText($id, 'capacity_input', $event->capacity, '12', '20',($event->capacity == 0) ? 'style="display: none"' : ''));
$smarty->assign('capacity_unlimited_label', $this->CreateLabelForInput($id, 'capacity_unlimited', $this->Lang('unlimitedcapacity')));
$smarty->assign('capacity_unlimited', $this->CreateInputCheckbox($id, 'capacity_unlimited', 'unlimited', ($event->capacity == 0) ? 'unlimited' : '', 'id='.$id.'capacity_unlimited'));

// Allow registration ?
$smarty->assign('allow_registration_label', $this->CreateLabelForInput($id, 'allow_registration', $this->Lang('allow_registration_detail')));
$smarty->assign('allow_registration_input', $this->CreateInputCheckbox($id, 'allow_registration', '1', ($event->allow_registration == 1) ? '1' : '', 'id='.$id.'allow_registration'));

// Which FEU groups can register to this event ?
$feu_module = cms_utils::get_module('FrontEndUsers');
$feu_groups = $feu_module->GetGroupList();
$event_groups = explode(',',$event->allowed_feu_groups);
$smarty->assign('allowed_feu_groups_label', $this->CreateLabelForInput($id, 'allowed_feu_groups', $this->Lang('allowed_feu_groups')));
$smarty->assign('allowed_feu_groups_input', $this->CreateInputSelectList($id, 'allowed_feu_groups[]', $feu_groups, $event_groups, 3));

// Capacity for each FEU - Input text and checkbox if unlimited (in db means 0)
$smarty->assign('capacity_per_feu_label', $this->CreateLabelForInput($id, 'capacity_per_feu_input', $this->Lang('capacity_per_feu')));
$smarty->assign('capacity_per_feu_input', $this->CreateInputText($id, 'capacity_per_feu_input', $event->capacity_per_feu, '12', '20',($event->capacity_per_feu == 0) ? 'style="display: none"' : ''));
$smarty->assign('capacity_per_feu_unlimited_label', $this->CreateLabelForInput($id, 'capacity_per_feu_unlimited', $this->Lang('unlimitedcapacity_per_feu')));
$smarty->assign('capacity_per_feu_unlimited', $this->CreateInputCheckbox($id, 'capacity_per_feu_unlimited', 'unlimited', ($event->capacity_per_feu == 0) ? 'unlimited' : '', 'id='.$id.'capacity_per_feu_unlimited'));

// Start and End datetimes for the registration -----------------
$smarty->assign('limited_reg_period_label', $this->CreateLabelForInput($id, 'limited_reg_period', $this->Lang('limited_reg_period_detail')));
$smarty->assign('limited_reg_period_input', $this->CreateInputCheckbox($id, 'limited_reg_period', '1', ($event->limited_reg_period == 1) ? '1' : '', 'id='.$id.'limited_reg_period'));

// Start date and hour
$start_datetime = explode(' ', $event->reg_start_datetime);
$start_date = $start_datetime[0];
$start_time = explode(':', $start_datetime[1]);
$start_time = $start_time[0] . ':' . $start_time[1];
$smarty->assign('reg_start_datetime_label', $this->CreateLabelForInput($id, 'reg_start_date', $this->Lang('reg_start_datetime')));
$smarty->assign('reg_start_date_input', $this->CreateInputText($id, 'reg_start_date', $start_date));
$smarty->assign('reg_start_time_input', $this->CreateInputText($id, 'reg_start_time', $start_time, '10'));

// End date and hour
$end_datetime = explode(' ', $event->reg_end_datetime);
$end_date = $end_datetime[0];
$end_time = explode(':', $end_datetime[1]);
$end_time = $end_time[0] . ':' . $end_time[1];
$smarty->assign('reg_end_datetime_label', $this->CreateLabelForInput($id, 'reg_end_datetime', $this->Lang('reg_end_datetime')));
$smarty->assign('reg_end_date_input', $this->CreateInputText($id, 'reg_end_date', $end_date));
$smarty->assign('reg_end_time_input', $this->CreateInputText($id, 'reg_end_time', $end_time, '10'));

// Custom fields
$smarty->assign('custom_fields_form', $FieldsManager->get_edit_form($id, 'EventsManager', $event->id, '', '', ''));

/******************************************************************************
// Display */

$smarty->assign('showtabs', $this->GetPreference('showtabs'));
if ($this->GetPreference('showtabs'))
{
	/* Tabs */
	// Tabs top
	$tabs_top = $this->StartTabHeaders();
	$tabs_top .= $this->SetTabHeader('generalinfo', $this->Lang('generalinfo_tab'));
	$tabs_top .= $this->SetTabHeader('registrations', $this->Lang('registrations_tab'));
	$tabs_top .= $this->EndTabHeaders();

	$toassign = array(
		'tabs_top'=>$tabs_top,
		'tab_content_start'=>$this->StartTabContent(),
		'tab_content_end'=>$this->EndTabContent(),
		'tab_generalinfo_start'=>$this->StartTab('generalinfo'),
		'tab_registrations_start'=>$this->StartTab('registrations'),
		'tab_end'=>$this->EndTab()
		);
	$smarty->assign($toassign);
}

// Errors
foreach ($errors as $error)
	echo $this->ShowErrors($error);

// Form template
echo $this->ProcessTemplate('admin_addeditevent.tpl');
?>