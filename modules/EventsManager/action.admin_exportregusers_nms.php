<?php
if (!isset($gCms)) exit;
if (!$this->HasPermission()) exit;

$messages = array();
$errors = array();

if (isset($params['cancel']))
	$this->Redirect($id, 'admin_viewregusers', $returnid, array('id_event'=>$params['id_event']));

if (isset($params['id_event']) AND ($nms_module = cms_utils::get_module('NMS')))
{
	$event = new EventsManager_event($params['id_event']);
	$feu_module = cms_utils::get_module('FrontEndUsers');
	
	if (isset($params['submit']) AND isset($params['nms_lists']))
	{
		// Process the export to NMS

		// Delete all the links users / nms lists
		if (isset ($params['delete_listuser_link']))
			foreach ($params['nms_lists'] as $onelist)
			{
				$db = cmsms()->GetDb();
				$query='DELETE FROM '.NMS_LISTUSER_TABLE.' WHERE listid=?';
				$qparams = array($onelist);
				$res = $db->Execute($query, $qparams);
			}
		
		// Retrieve the registrations
		$registrations = $event->get_registrations();
		
		$count = 0;
		foreach ($registrations as $registration)
		{
			$continue_export = true;
			
			// Verify if we need to sort cancelled/registered registrations
			if (isset($params['who_export']) && ($params['who_export'] != 'all'))
			{
				if ( ($params['who_export'] == 'registered') && ($registration->nb_persons <= 0))
					$continue_export=false;
				elseif ( ($params['who_export'] == 'cancelled') && ($registration->nb_persons > 0))
					$continue_export=false;
			}
			
			// Verify the FEU Group
			if (isset($params['export_feu_groups']) && ($continue_export))
			{
				// Default to false, and only if the user belongs to 1 (or more) group, we continue
				$continue_export = false;
				foreach ($params['export_feu_groups'] as $groupid)
					if ($feu_module->MemberOfGroup($registration->id_user, $groupid))
						$continue_export = true;
			}
			
			if ($continue_export)
			{
				// Get the user mail
				if ($params['emailproperty'] == 'use_username')
					$usermail = $feu_module->GetUserName($registration->id_user);
				else
					$usermail = $feu_module->GetUserPropertyFull($params['emailproperty'], $registration->id_user, '');
				
				if (!empty($usermail))
				{
					// Mail OK - Continue with the name (which can be empty)
					if ($params['nameproperty'] == 'use_username')
						$username = $feu_module->GetUserName($registration->id_user);
					else
						$username = $feu_module->GetUserPropertyFull($params['nameproperty'], $registration->id_user, '');
					
					// INSERT TO NMS and confirm the account
					echo $nms_module->AddUser($usermail, $params['nms_lists'], $username, 0, 1);
					
					$count++;
				}
			}
		}
		
		$messages[] = $this->Lang('nms_export_done', $count);
	}
	else
	{
		if (isset($params['submit']))
			$errors[] = $this->Lang('error_nolistselected');
	}
	
	// General stuff
	$smarty->assign('form_start', $this->CreateFormStart($id, 'admin_exportregusers_nms') . $this->CreateInputHidden($id, 'id_event', $params['id_event']));
	$smarty->assign('form_end', $this->CreateFormEnd());
	$smarty->assign('submit', $this->CreateInputSubmit($id, 'submit', lang('submit')));
	$smarty->assign('cancel', $this->CreateInputSubmit($id, 'cancel', lang('cancel')));
	
	// Ask for the field which contains the e-mail and the name		
	$feu_properties = $feu_module->GetPropertyDefns();
	$options_list = array($this->Lang('use_username')=>'use_username');
	foreach ($feu_properties as $key=>$val)
		$options_list[$val['prompt']] = $key;
	
	// E-mail
	$smarty->assign('label_emailproperty', $this->CreateLabelForInput($id, 'emailproperty', $this->Lang('emailproperty')));
	$smarty->assign('input_emailproperty', $this->CreateInputDropdown($id, 'emailproperty', $options_list));
	// NMS name
	$smarty->assign('label_nameproperty', $this->CreateLabelForInput($id, 'nameproperty', $this->Lang('nameproperty')));
	$smarty->assign('input_nameproperty', $this->CreateInputDropdown($id, 'nameproperty', $options_list));
	
	// NMS export list(s)
	$nms_lists = $nms_module->GetLists();
	$options = array();
	foreach ($nms_lists as $list)
	{
		$options[$list['name']] = $list['listid'];
	}
	$smarty->assign('label_nmslist', $this->CreateLabelForInput($id, 'nmslist', $this->Lang('nmslist')));
	$smarty->assign('input_nmslist', $this->CreateInputSelectList($id, 'nms_lists[]', $options, array(), 5));
	
	// FEU groups to export
	// Search the FEU groups - We could use the Event groups, but if we decide to change the FEU group for an event that has registrations, we could loose some registrations for the NMS export (for the that have been allowed, and then disallowed)
	$feu_groups = $feu_module->GetGroupList();
	$event_groups = explode(',',$event->allowed_feu_groups);
	$smarty->assign('label_export_feu_groups', $this->CreateLabelForInput($id, 'export_feu_groups', $this->Lang('export_feu_groups')));
	$smarty->assign('input_export_feu_groups', $this->CreateInputSelectList($id, 'export_feu_groups[]', $feu_groups, $event_groups, 3));

	// Who export: registered / cancelled / all ?
	$options_list = array($this->Lang('nms_who_export_all')=>'all', $this->Lang('nms_who_export_registered')=>'registered', $this->Lang('nms_who_export_cancelled')=>'cancelled');
	$smarty->assign('label_who_export', $this->CreateLabelForInput($id, 'who_export', $this->Lang('nms_who_export')));
	$smarty->assign('input_who_export', $this->CreateInputDropdown($id, 'who_export', $options_list, 0));

	// Delete all users from the target NMS list
	$smarty->assign('label_delete_listuser_link', $this->CreateLabelForInput($id, 'delete_listuser_link', $this->Lang('delete_listuser_link_label')));
	$smarty->assign('input_delete_listuser_link', $this->CreateInputCheckbox($id, 'delete_listuser_link', '1', 0, 'id="'.$id.'delete_listuser_link"'));
	
	// Event object
	$smarty->assign('event', $event->get_smarty_object());
	
	// DISPLAY
	// Errors
	foreach ($errors as $error)
		echo $this->ShowErrors($error);
		
	// Messages
	foreach($messages as $message)
		echo $this->ShowMessage($message);
	
	echo $this->ProcessTemplate('admin_exportregusers_nms.tpl');
}


?>