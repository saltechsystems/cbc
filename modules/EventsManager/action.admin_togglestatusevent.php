<?php
if (!isset($gCms)) exit;
if (!$this->HasPermission()) exit;

$errors = array();
$messages = array();

$event = new EventsManager_event($params['id_event'], false);

$this->SetCurrentTab('events');

if (isset($params['status']))
{
	$event->status = $params['status'];
	
	if ($event->addedit('status'))
		$this->RedirectToTab($id);
}
else
{
	$errors[] = $this->Lang('error_db');
}

// Errors
foreach ($errors as $error)
	echo $this->ShowErrors($error);

?>