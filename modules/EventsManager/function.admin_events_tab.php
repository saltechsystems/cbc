<?php
if (!isset($gCms)) exit;

// Display the list of events
$events = EventsManager_events_ops::get_events('all', '', '', 'all', '', '', false, 'start_datetime', 'DESC');
$items = array();

$theme = $gCms->variables['admintheme'];
$themeName = $theme->__get('themeName');

$rowclass = 'row1';
foreach ($events as $event)
{
	// Admin Links
	$event->name_editlink = $this->CreateLink($id, 'admin_addeditevent', $returnid, $event->name, array('id_event'=>$event->id));
	
	$event->togglestatuslink = $this->CreateLink($id, 'admin_togglestatusevent', $returnid, $theme->DisplayImage('icons/system/' . ( ($event->status=='published') ? 'true' : 'false') . '.gif', $this->Lang('toggle_status'), '', '', 'systemicon'), array('id_event'=>$event->id, 'status'=>($event->status=='published') ? 'draft' : 'published'));

	if ($themeName == 'OneEleven')
		$image = 'icons/topfiles/usersgroups.png';
	else
		$image = 'icons/topfiles/usersgroups.gif';
	
	$event->viewreguserslink = $this->CreateLink($id, 'admin_viewregusers', $returnid, $theme->DisplayImage($image, $this->Lang('view_regusers'), '24', '29', 'systemicon') . ' ' . $this->Lang('manage'), array('id_event'=>$event->id));
	
	$event->exportreguserslink = $this->CreateLink($id, 'admin_exportregusers', $returnid, $theme->DisplayImage('icons/system/export.gif', $this->Lang('export_regusers'), '24', '29', 'systemicon') . ' ' . $this->Lang('export_regusers_file'), array('id_event'=>$event->id, 'disable_theme'=>1));
	
	if ($nms_module = cms_utils::get_module('NMS'))
	{
		$event->exportreguserslink_nms = $this->CreateLink($id, 'admin_exportregusers_nms', $returnid, $theme->DisplayImage('icons/system/export.gif', $this->Lang('export_regusers_nms'), '24', '29', 'systemicon') . ' ' . $this->Lang('export_regusers_nms_small'), array('id_event'=>$event->id));
	}
	
	$event->toggleregistationlink = $this->CreateLink($id, 'admin_toggleregistrationevent', $returnid, $theme->DisplayImage('icons/system/' . ( ($event->allow_registration) ? 'true' : 'false') . '.gif', $this->Lang('toggle_registration'), '', '', 'systemicon'), array('id_event'=>$event->id, 'allow_registration'=>($event->allow_registration) ? '0' : '1'));
	
	$event->copylink = $this->CreateLink($id, 'admin_addeditevent', $returnid, $theme->DisplayImage('icons/system/copy.gif', $this->Lang('copy'), '', '', 'systemicon'), array('id_event'=>$event->id, 'copy'=>1));
	
	$event->editlink = $this->CreateLink($id, 'admin_addeditevent', $returnid, $theme->DisplayImage('icons/system/edit.gif', $this->Lang('edit'), '', '', 'systemicon'), array('id_event'=>$event->id));
	
	$event->deletelink = $this->CreateLink($id, 'admin_deleteevent', $returnid, $theme->DisplayImage('icons/system/delete.gif', $this->Lang('delete'), '', '', 'systemicon'), array('id_event'=>$event->id), $this->Lang('areyousure'));

	// Status
	$now = date('YmdHis');
	$start_time = strftime('%Y%m%d%H%M%S', strtotime($event->start_datetime));
	$end_time = strftime('%Y%m%d%H%M%S', strtotime($event->end_datetime));

	if ($start_time > $now)
		$event->whenstatus = "upcoming";
	elseif ($end_time < $now)
		$event->whenstatus = "past";
	else
		$event->whenstatus = "inprogress";
	
	$event->rowclass = $rowclass;
	$items[] = $event;
	
	($rowclass=="row1" ? $rowclass="row2" : $rowclass="row1");
}

// Assign to smarty
$smarty->assign('items', $items);
$smarty->assign('addlink', $this->CreateLink($id, 'admin_addeditevent', $returnid, $gCms->variables['admintheme']->DisplayImage('icons/system/newobject.gif', $this->Lang('addevent'), '', '', 'systemicon') . $this->Lang('addevent')));

// Add a new registration link
$smarty->assign('reg_addlink', $this->CreateLink($id, 'admin_addeditregistration', $returnid, $gCms->variables['admintheme']->DisplayImage('icons/system/newobject.gif', $this->Lang('addregistration'), '', '', 'systemicon') .' '.$this->Lang('addregistration')));

echo $this->ProcessTemplate('admin_tab_events.tpl');

?>