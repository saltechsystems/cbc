<?php

class EventsManager_event extends AireDbObject
{
	protected $table = 'events';

	protected $custom_fields = array(
		'name'=>'',
		'description'=>'',
		'id_category'=>0,
		'status'=>'draft',
		'start_datetime'=>'',
		'end_datetime'=>'', 
		'url'=>'', 
		'capacity'=>0, 
		'allow_registration'=>0, 
		'allowed_feu_groups'=>'', 
		'price'=>'0.00', 
		'capacity_per_feu'=>0,
		'limited_reg_period'=>0,
		'reg_start_datetime'=>'',
		'reg_end_datetime'=>'',
		'cancelled'=>0
		);
	
	public function __construct($id=false, $load_fields=true)
	{
		if ($load_fields)
		{
			$this->custom_fields['start_datetime'] = AireTools::GetDateTime();
			$this->custom_fields['end_datetime'] = AireTools::GetDateTime();
			$this->custom_fields['reg_start_datetime'] = AireTools::GetDateTime();
			$this->custom_fields['reg_end_datetime'] = AireTools::GetDateTime();
		}
		
		parent::__construct($id, $load_fields);
	}
	
	// Returns the number of registered users for this event
	public function get_regusers_number()
	{
		if (isset($this->cache_regusers_number))
			return $this->cache_regusers_number;
		else
		{
			$db = cmsms()->GetDb();
			$query = "SELECT SUM(nb_persons) FROM ".cms_db_prefix()."module_events_registrations WHERE id_event=? GROUP BY id_event";
			$res = $db->GetOne($query, array($this->id));
			
			if (!$res)
				$res = 0;
			
			// Cache the result
			$this->cache_regusers_number = $res;
			return($res);
		}
	}
	
	// Returns true if users can register to this event
	// nb_to_add : calculates the availability with this number (the script adds that number to the number of the regusers
	// nb_before_modif = when you update a registration, this is the old number
	// For example, you want to remplace "5" registrations (nb_before_modif) to "7" (nb_to_add)
	public function registration_allowed($nb_to_add=false, $nb_before_modif=0)
	{
		$nb_to_add = $nb_to_add - $nb_before_modif;
		
		if ($this->allow_registration)
		{
			// User connected and in the right group(s) ?
			$feu_module = cms_utils::get_module('FrontEndUsers');
			$user_id = $feu_module->LoggedInId();
			
			if ($user_id)
			{
				// User is connected - check if he's in the right group(s)
				$user_groups = $feu_module->GetMemberGroupsArray($user_id);
				$auth = false;
				
				if ($this->allowed_feu_groups == '')
					$auth = true;
				else
				{
					// Check if the current user belongs to one of the allowed groups
					foreach ($user_groups as $group_test)
						if (in_array($group_test['groupid'], explode(',',$this->allowed_feu_groups)))
							$auth = true;
				}
				
				if ($auth)
				{
					if ($this->capacity > 0)
					{
						// Check capacity
						$nb_regusers = $this->get_regusers_number();
						
						if ($nb_to_add)
							$nb_regusers += $nb_to_add;

						// Do the comparison
						if ($nb_regusers <= $this->capacity)
							return true;
					}
					else
						return true;
				}
			}
		}
		return false;
	}
	
	// Returns an array of registrations objects
	public function get_registrations()
	{
		$db = cmsms()->GetDb();
		$query = 'SELECT * FROM '.cms_db_prefix().'module_events_registrations WHERE id_event=? ORDER BY `create_datetime`';
		
		$res = $db->GetArray($query, array($this->id));
		
		if ($res)
		{
			$registrations = array();
			foreach($res as $oneline)
			{
				// Create an empty object
				$registration = new EventsManager_registration();
				$registration->load_from_array($oneline);

				// And add values from db
				$registrations[] = $registration;
			}
			return $registrations;
		}
		
		return false;
	}
	
	// Load the category object
	public function load_category()
	{
		if ($this->id_category)
		{
			$this->category = new EventsManager_category($this->id_category);
			
			if ($this->category)
				return true;
		}
		return false;
	}
	
	// Event specific info for the smarty object
	public function get_smarty_object()
	{
		$obj = &parent::get_smarty_object();
		
		// Add the category name
		if ($this->load_category())
		{
			$obj->category = $this->category->get_smarty_object();
		}
		// Add the number of registered users
		if (!isset($this->nb_regusers))
			$this->nb_regusers = $this->get_regusers_number();
		$obj->nb_regusers = $this->nb_regusers;
		
		return $obj;
	}

	// Load from array with an extra value for the reg users count
	public function load_from_array(array $infos)
	{
		parent::load_from_array($infos);
		if (array_key_exists('nb_regusers', $infos))
			$this->nb_regusers = $infos['nb_regusers'];
	}
}

?>