<?php

class EventsManager_user
{
	public function __construct($id)
	{
		$feu_module = cms_utils::get_module('FrontEndUsers');
		
		$this->id = $id;

		$this->username = $feu_module->GetUserName($id);
		
		$props = $feu_module->GetUserProperties($id);

		if ($props)
		{
			foreach ($props as $prop)
			{
				$this->$prop['title'] = $prop['data'];
			}
		}
	}
}

?>