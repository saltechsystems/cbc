<?php

// Some useful functions for categories
class EventsManager_categories_ops
{
	/* Returns an array with all the categories */
	static public function get_categories()
	{
		$db = cmsms()->GetDb();
		$query = 'SELECT * FROM '.cms_db_prefix().'module_events_categories ORDER BY `name`';
		return ($db->GetArray($query));
	}
	
	/* Returns an array ready to be included in a CreateInputDropdown method */
	// display_none = if true, adds a "none" option with value 0
	static public function get_categories_for_dropdown($display_none=false)
	{
		$categories = self::get_categories();
		
		$result = array();
		if ($display_none)
			$result[lang('none')] = 0;
		
		foreach ($categories as $onerow)
		{
			$result[$onerow['name']] = $onerow['id'];
		}
		return $result;
	}
	
	/* Returns true if a category with the name $name exists 
	excludeid = a category to exclude in the query (useful for test before renaming a category)
	Returns TRUE if a category exists
	*/
	static public function category_exist($name, $excludeid=false)
	{
		if ($name != '')
		{
			$db = cmsms()->GetDb();
			$query = 'SELECT id FROM '.cms_db_prefix().'module_events_categories WHERE name=?';
			$qparams[] = $name;
			
			if ($excludeid != false)
			{
				$query .= ' AND id!=?';
				$qparams[] = $excludeid;
			}
			
			$res = $db->GetOne($query, $qparams);
					
			if ($res)
				return true;
			return false;
		}
	}
}

?>