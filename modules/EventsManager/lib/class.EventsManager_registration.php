<?php

class EventsManager_registration extends AireDbObject
{
	protected $table = 'events_registrations';
	
	protected $custom_fields = array(
		'id_event'=>'',
		'id_user'=>'',
		'nb_persons'=>0
	);
	
	// Constructor - we try to load the registration from the id_event and current logged in user
	public function __construct($id=false, $load_fields=true, $id_event=false, $id_user=false)
	{
		if ($id_event)
		{
			$db = cmsms()->GetDb();
			
			// Try to load the id from the db
			$query = "SELECT id FROM ".cms_db_prefix()."module_".$this->table." WHERE id_event=? AND id_user=?";
			$id_registration = $db->GetOne($query, array($id_event, $id_user));
			
			if ($id_registration)
			{
				// Load with id
				parent::__construct($id_registration);
			}
			else
			{
				$this->custom_fields['id_event'] = $id_event;
				$this->custom_fields['id_user'] = $id_user;
				parent::__construct($id, $load_fields);
			}
		}
		else
			parent::__construct($id, $load_fields);
		
		if ($this->id_user)
			$this->load_user();
	}
	
	// Load the user with the properties
	public function load_user()
	{
		$this->user = new EventsManager_user($this->id_user);
	}

	// Load the event
	public function load_event()
	{
		if ($this->id_event)
			$this->event = new EventsManager_event($this->id_event);
	}
	
	// Smarty object with the user
	public function get_smarty_object()
	{
		if (!isset($this->user))
			$this->load_user();
		
		$obj = parent::get_smarty_object();
		$obj->user = $this->user;

		// Status of the registration
		if (($this->id) && ($this->nb_persons==0))
			$status = 'cancelled';
		elseif (($this->id) && ($this->nb_persons>0))
			$status = 'registered';
		else
			$status = 'unregistered';
		$obj->status = $status;
		
		return $obj;
	}

	// Send a mail alert
	public function send_mail_alerts()
	{
		// See if we have to send something
		$module = cms_utils::get_module('EventsManager');

		if ($this->nb_persons > 0)
		{
			$mode = 'new';
			// Confirm the registration ?
			$sendtouser = $module->GetPreference('newregistration_mail_sendtouser');
			$sendtoother = $module->GetPreference('newregistration_mail_sendtoother');
		}
		else
		{
			$mode = 'cancel';
			// Confirm the cancellation ?
			$sendtouser = $module->GetPreference('cancelregistration_mail_sendtouser');
			$sendtoother = $module->GetPreference('cancelregistration_mail_sendtoother');
		}

		if ($sendtouser)
		{
			// Try to find an e-mail
			$feu = cms_utils::get_module('FrontEndUsers');
			$user_mail = $feu->GetEmail($this->id_user);

			if (empty($user_mail))
				$sentouser = false;
		}

		if ($sendtoother)
		{
			// Get the emails
			$admin_mails = $module->GetPreference($mode.'registration_mail_sendtoother_mails');
			if (!empty($admin_mails))
			{
				$admin_mails = explode(',',$admin_mails);
			}
			else
				$sendtoother = false;
		}

		if ($sendtouser || $sendtoother)
		{
			// Ok, we have to send something
			$smarty = cmsms()->GetSmarty();
			
			// Load the objects
			$registration = $this->get_smarty_object();
			$feu_user = $this->user;
			$this->load_event();
			$event = $this->event->get_smarty_object();

			$smarty->assign('registration', $registration);
			$smarty->assign('feu_user', $feu_user);
			$smarty->assign('event', $event);

			// Process with the mails
			$cmsmailer = cms_utils::get_module('CMSMailer');
			if (!$cmsmailer) return false;

			$cmsmailer->reset();
			$cmsmailer->IsHTML(true);

			// Get the subject
			$subject = $module->GetPreference($mode.'registration_mail_subject');
			$subject = $module->ProcessTemplateFromData($subject);
			// The body
			$body = $module->GetTemplate($mode.'registrationmail_Sample');
			$body = $module->ProcessTemplateFromData($body);
			
			$cmsmailer->SetSubject($subject);
			$cmsmailer->SetBody($body);

			// Add the addresses
			// Try to find an e-mail
			$feu = cms_utils::get_module('FrontEndUsers');
			$user_mail = $feu->GetEmail($this->id_user);

			if ($sendtouser)
				$cmsmailer->AddAddress($user_mail);

			if ($sendtoother)
			{
				foreach ($admin_mails as $onemail)
					$cmsmailer->AddAddress($onemail);
			}

			$cmsmailer->Send();
			$res = true;
			if ($cmsmailer->IsError())
			{
				$res = false;
				@trigger_error('Problem sending email: '.$cmsmailer->GetErrorInfo());
			}
			$cmsmailer->reset();

			return $res;
		}
	}
}

?>