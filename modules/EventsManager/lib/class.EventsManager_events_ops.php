<?php

// Some useful functions for events
class EventsManager_events_ops
{
	/* Returns an array with all the events */
	/* PARAMS :
		status : the status of the events to be returned
		category_id : only get events in that or these category/categories
		category_name : same with the name or names
		show : upcoming, past, all or custom
		start_datetime / end_datetime : if "show=custom", only display the events that are valid for the start and end datetime
		myevents : if true, only display the current FEUser events (to which he is registered to)
	*/
	static public function get_events($status='published', $category_id='', $category_name='', $show='all', $start_datetime='', $end_datetime='', $myevents=false, $sortby='name', $sortorder='ASC')
	{
		$db = cmsms()->GetDb();
		$query = 'SELECT e.*, COALESCE(SUM(er.nb_persons), 0) AS "nb_regusers" FROM '.cms_db_prefix().'module_events e';
		// For the registrations count
		$query .= ' LEFT JOIN '.cms_db_prefix().'module_events_registrations er ON (e.id=er.id_event)';
		
		if (!empty($category_name))
			$query .= ' LEFT JOIN '.cms_db_prefix().'module_events_categories ec ON (e.id_category=ec.id)';
		
		//****************************************************************************
		// Specific params ?
		
		// Save the fact that the "where" has been used
		$where_used = false;
		$qparams = array();

		if (!empty($category_id) OR !empty($category_name))
		{
			$query .= ' WHERE ';
			$where_used = true;
			$qclauses = array();
		}
		
		// Specific categories IDs ?
		if (!empty($category_id))
		{
			// List in an array
			$category_id_array = explode(',', $category_id);
			
			foreach($category_id_array as $val)
			{
				$qclauses[] = "e.`id_category`=?";
				$qparams[] = trim($val);
			}
		}
		
		// Specific categories names ?
		if (!empty($category_name))
		{
			// List in an array
			$category_array = explode(',', $category_name);
			
			foreach ($category_array as $val)
			{
				$qclauses[] = "ec.`name`=?";
				$qparams[] = trim($val);
			}
		}
		
		// ***********************************************************************************************
		// Build the query
		if (isset($qclauses) and count($qclauses))
		{
			$clauses = implode(' OR ', $qclauses);
			$query .= $clauses;
		}
		
		// Status
		if ($status != 'all' && $status != '')
		{
			$query .= ($where_used) ? " AND " : " WHERE ";
			$query .="e.status=?";
			$qparams[] = $status;
			
			$where_used = true;
		}
		
		// Past, upcoming, current or all events ?
		if ($show!= 'all' && $show != '')
		{
			$query .= ($where_used) ? " AND " : " WHERE ";

			$now = $db->DbTimeStamp(time());
			
			switch ($show) {
				case 'past':
					$query .= "e.end_datetime < " . $now;
					break;
				case 'upcoming':
					$query .= "e.end_datetime > " . $now;
					break;
				case 'currently':
					$query .="e.start_datetime < " . $now . " AND e.end_datetime > " . $now;
					break;
				case 'custom':
					if ($start_datetime != '')
					{
						$query .= "e.end_datetime > ?";
						$qparams[] = $start_datetime;
						
						if ($end_datetime != '')
							$query .= ' AND ';
					}
					if ($end_datetime != '')
					{
						$query .= "e.start_datetime < ?";
						$qparams[] = $end_datetime;
					}
			}
			
			$where_used = true;
		}

		// Show only current FEUser events ?
		if ($myevents)
		{
			$feu_module = cms_utils::get_module('FrontEndUsers');
			$id_user = $feu_module->LoggedInId();

			if ($id_user)
			{
				$query .= ($where_used) ? " AND " : " WHERE ";
				$query .= "er.id_user=? AND er.nb_persons > 0";
				$qparams[] = $id_user;
			}
		}
		
		$query .=' GROUP BY e.id';
		$query .=' ORDER BY `' . $sortby . '` ' . $sortorder;
		
		$res = $db->GetArray($query, $qparams);
		//echo $db->sql;
		$events = array();
		if ($res)
		{
			foreach($res as $oneline)
			{
				// Create an empty object
				$event = new EventsManager_event();
				$event->load_from_array($oneline);
				
				// And add values from db
				$events[] = $event;
			}
		}
		
		return $events;
	}

	/* Get all the events in a dropdown selector */
	public function get_events_dropdown_formatted($actionid, $field_name, $id_event)
	{
		$events = self::get_events();
		$res = false;
		$EventsManager = cms_utils::get_module('EventsManager');
		$notallowed = $EventsManager->Lang('registrations_not_allowed');

		if ($events)
			$res = '';
		else
			return '';

		// Create the select
		$res = '<select id="' . $actionid . $field_name . '" name="' . $actionid . $field_name . '">';
		
		foreach ($events as $event)
		{
			// Get the registrations to show it
			$nb_regs = $event->get_regusers_number();

			$description = '';

			if ($event->allow_registration)
			{
				$capacity = $event->capacity;
				if ( ($event->allow_registration) && ($event->capacity==0))
					$capacity = $EventsManager->Lang('unlimited');
				
				$description = $event->name.' ('.$nb_regs.'/'.$capacity.')';
			}
			else
			{
				$description = $event->name . ' (' . $notallowed . ')';
			}
			$description .= ' - ID ' . $event->id;

			$class = '';
			$event_enddatetime = strtotime($event->end_datetime);
			if ($event_enddatetime < time())
				$class = ' class="past"';
			
			$res .= '<option' . $class . ' value="' . $event->id . '">' . $description . '</option>';
		}
		$res .= '</select>';

		return $res;
	}
}

?>