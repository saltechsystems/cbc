<?php

class EventsManager_users_ops
{
	public static function get_users_dropdown()
	{
		$res = array();

		$feu_module = cms_utils::get_module('FrontEndUsers');
		$users = $feu_module->GetUsersInGroup();

		foreach ($users as $user)
			$res[$user['username']] = $user['id'];

		return $res;
	}
}

?>