<?php
if (!isset($gCms)) exit;
if (!$this->HasPermission()) exit;

$tabs_available = array('events', 'fielddefs', 'categories', 'summarytemplates', 'detailtemplates','registrationtemplates', 'registrationslisttemplates', 'mails', 'preferences');

if (isset($params['active_tab']))
	$this->SetCurrentTab($params['active_tab']);

echo $this->StartTabHeaders();
foreach ($tabs_available as $tab_name)
{
	echo $this->SetTabHeader($tab_name, $this->Lang($tab_name));
}
echo $this->EndTabHeaders();

echo $this->StartTabContent();

foreach ($tabs_available as $tab_name)
{
	echo $this->StartTab($tab_name, $params);
	include(dirname(__FILE__).'/function.admin_'.$tab_name.'_tab.php');
	echo $this->EndTab();
}

echo $this->EndTabContent();

?>