<?php
if( !isset($gCms) ) exit;

// We have to valid a registration if there is something pending in the session
$sess = new cge_session($this->GetName());
$id_event = $sess->get('event_registration');

$feu = cms_utils::get_module('FrontEndUsers');
$id_user = $feu->LoggedInId();

if (!empty($id_event) && $id_user)
{
	$nb_persons = $sess->get('nb_persons');
	$event = new EventsManager_event($id_event);

	// Verify capacity
	if ($event->registration_allowed($nb_persons) && ($nb_persons != 0))
	{
		// Process to the registration
		$registration = new EventsManager_registration(false, true, $id_event, $id_user);
		$registration->nb_persons = $nb_persons;

		if ($registration->addedit())
		{
			// Clear the session
			$sess->clear();

			// Send events
			$parms['registration_id'] = $registration->id;

			if ($id_before_addedit)
				$this->SendEvent('EventsManagerRegistrationEdited', $parms);
			else
				$this->SendEvent('EventsManagerRegistrationAdded', $parms);

			// Send e-mails if necessary
			$registration->send_mail_alerts();
		}
	}
}

?>