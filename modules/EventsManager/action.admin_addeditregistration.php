<?php
if (!isset($gCms)) exit;
if (!$this->HasPermission()) exit;

/* Prepare the display */
$errors = array();
$messages = array();

// Add or edit ?
if (isset($params['id_registration']))
{
	$mode = 'edit';
	$id_item=$params['id_registration'];
}
else
{
	$mode = 'add';
	$id_item=false;
}

$item = new EventsManager_registration($id_item);

if (isset($params['id_event']) && ($params['id_event'] != 0))
	$item->id_event = $params['id_event'];

if ($item->id_event)
	$event = new EventsManager_event($item->id_event);

/******************************************************************************
// Submit */
if (isset($params['submit']))
{
	// We have to check if the registration is still existing in the db
	// In this case, we have to update it, not to add a new one, because each registration (user/event) must be unique
	$item_tmp = new EventsManager_registration(false, true, $params['id_event'], $params['id_user']);
	if ($item_tmp->id)
	{
		$moved_item = false;
		if ($mode == 'edit')
		{
			// We delete the old one if the ids of the registrations are not the same between the one the system wants to modify (in params) and the one we found with the new user/event values
			if ($item_tmp->id != $item->id)
			{
				$moved_item = true;
				$item->delete();
			}
		}
		// Force the edit mode
		$mode == 'edit';

		$item = $item_tmp;
		// Add the new number to the old one, but in two cases only :
		//  - If this is an addition, if we are here we add to an existing registration
		//  - If we moved the registration from a user or event to another
		if ($mode == 'add' || $moved_item)
			$item->nb_persons += $params['nb_persons'];
		else
			$item->nb_persons = $params['nb_persons'];
	}
	else
		$item->load_from_array($params);

	if ($item->nb_persons == '')
		$errors[] = $this->Lang('error_no_nb_users');
	else
	{
		// Ready - Just have to store the actual id to see if it's an addition or update (for cmsms event send)
		$id_before_addedit = $item->id;
		
		// We can do the insert / update in db		
		if ($item->addedit())
		{
			// Send the event
			$parms['registration_id'] = $item->id;
			
			if ($id_before_addedit)
				$this->SendEvent('EventsManagerRegistrationEdited', $parms);
			else
				$this->SendEvent('EventsManagerRegistrationAdded', $parms);
			
			$this->Redirect($id, 'admin_viewregusers', $returnid, array('id_event'=>$item->id_event));
		}
		else
			$errors[] = $this->Lang('error_db');
	}
}
elseif (isset($params['cancel']))
{
	if (isset($item->id_event) && isset($params['id_event_return']))
		$this->Redirect($id, 'admin_viewregusers', $returnid, array('id_event'=>$params['id_event_return']));
	else
		$this->Redirect($id, 'defaultadmin', $returnid);
}

/******************************************************************************
// Assign to smarty */

/* Form general vars */
$smarty->assign('form_start', $this->CreateFormStart($id, 'admin_addeditregistration', $returnid, 'POST'));
$smarty->assign('submit', $this->CreateInputSubmit($id, 'submit', lang('submit')));
$smarty->assign('cancel', $this->CreateInputSubmit($id, 'cancel', lang('cancel')));
$smarty->assign('form_end', $this->CreateFormEnd());

// Hidden values
$hidden = '';
if ($mode == 'edit')
	$hidden .= $this->CreateInputHidden($id, 'id_registration', $item->id);
if (isset($params['id_event_return']) || isset($params['id_event']))
	$hidden .= $this->CreateInputHidden($id, 'id_event_return', ($params['id_event']) ? $params['id_event'] : $item->id_event);
$smarty->assign('hidden', $hidden);

/* Fields */
/* Events */
$events_dropdown = EventsManager_events_ops::get_events_dropdown_formatted($id, 'id_event', $item->id_event);
$smarty->assign('event_input', $events_dropdown);
$smarty->assign('event_label', $this->CreateLabelForInput($id, 'id_event', $this->Lang('event')));

/* Feu users */
$users_dropdown = EventsManager_users_ops::get_users_dropdown();
$smarty->assign('feusers_input', $this->CreateInputDropdown($id, 'id_user', $users_dropdown, '', $item->id_user));
$smarty->assign('feusers_label', $this->CreateLabelForInput($id, 'id_user', $this->Lang('feuname')));

/* Number of registrations */
$smarty->assign('nb_persons_input', $this->CreateInputText($id, 'nb_persons', $item->nb_persons));
$smarty->assign('nb_persons_label', $this->CreateLabelForInput($id, 'nb_persons', $this->Lang('nb_persons')));

/******************************************************************************
// Display */
$smarty->assign('title', $this->Lang($mode . 'registration'));
$smarty->assign('show_past_events', $this->Lang('show_past_events'));

// Errors
foreach ($errors as $error)
	echo $this->ShowErrors($error);

// Form template
echo $this->ProcessTemplate('admin_addeditregistration.tpl');
?>