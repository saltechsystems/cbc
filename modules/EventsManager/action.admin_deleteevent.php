<?php
if (!isset($gCms)) exit;
if (!$this->HasPermission()) exit;

$errors = array();
$messages = array();

$event = new EventsManager_event($params['id_event']);

/* Customfields managed by AireLibs */
/* Delete the values for this element */
$FieldsManager = new AireFieldsManager();
if ($FieldsManager->delete_values_for_item($id, 'EventsManager', $event->id, '', '', ''))
{
	$this->SetCurrentTab('events');
	if ($event->delete())
	{
		$this->SendEvent('EventsManagerEventDeleted', array('id_event'=>$params['id_event']));

		$this->RedirectToTab($id);
	}
}

?>