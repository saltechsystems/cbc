<?php
if (!isset($gCms)) exit;

// Drop tables
$db = cmsms()->GetDb();
$dict = NewDataDictionary($db);

$tables = array('events', 'events_categories', 'events_registrations');

foreach ($tables as $table)
{
	$sqlarray = $dict->DropTableSQL(cms_db_prefix()."module_" . $table);
	$dict->ExecuteSQLArray($sqlarray);
}

// Preferences, permissions and templates
$this->DeleteTemplate();
$this->RemovePreference();
$this->RemovePermission('Use EventsManager');

// CMSMS Events
$this->RemoveEvent('EventsManagerEventAdded');
$this->RemoveEvent('EventsManagerEventEdited');
$this->RemoveEvent('EventsManagerEventDeleted');

$this->RemoveEvent('EventsManagerCategoryAdded');
$this->RemoveEvent('EventsManagerCategoryEdited');
$this->RemoveEvent('EventsManagerCategoryDeleted');

$this->RemoveEvent('EventsManagerRegistrationAdded');
$this->RemoveEvent('EventsManagerRegistrationEdited');

// Events handlers
$this->RemoveEventHandler('FrontEndUsers', 'OnLogin');


?>