<?php
if (!isset($gCms)) exit;

/* Default action : displays a summary view */

// Some default values *************************************
// Inline ?
$inline = 0;
if ( (isset($params['inline'])) AND ($params['inline'] != '') )
	$inline = $params['inline'];

// Detail page ID ?
$detailpage = $this->GetPreference('dflt_detailpage', $returnid);
if (isset($params['detailpage']) and !empty($params['detailpage']))
	$detailpage = $params['detailpage'];
elseif ($detailpage == -1)
	$detailpage = $returnid;

// Registration page ID ?
$registrationpage = '';
if (isset($params['registrationpage']) and !empty($params['registrationpage']))
	$registrationpage = $params['registrationpage'];
else
{
	$registrationpage = $this->GetPreference('dflt_registrationpage', '-1');
	if ($registrationpage == '-1')
		$registrationpage = $returnid;
}

// Dates
$show = 'upcoming';
if (isset($params['show']))
	$show = $params['show'];
$start_datetime = '';
$end_datetime = '';
if ($show == 'custom')
{
	if ($params['start_datetime'] !='' OR $params['end_datetime']!='')
	{
		$start_datetime = $params['start_datetime'];
		$end_datetime = $params['end_datetime'];
	}
	else
		$show = 'all';
}

// Status
$status = 'published';
if (isset($params['status']))
	$status = $params['status'];

// Category or category_id ?
$category_id='';
$category_name='';
if (isset($params['category_id']))
	$category_id = $params['category_id'];
if (isset($params['category']))
	$category_name = $params['category'];
	
// MyEvents ? if 1, only display the current loggedin user events (to which he is registered to)
$myevents = false;
if (isset($params['myevents']) AND ($params['myevents']==1 OR $params['myevents'] == 'true'))
	$myevents = true;
	
// Sortby and sortorder
$sortby = 'name';
$sortorder = 'ASC';
if (isset($params['sortby']) and !empty($params['sortby']))
	$sortby = $params['sortby'];
if (isset($params['sortorder']) and !empty($params['sortorder']))
	$sortorder = $params['sortorder'];
	
// Load the events
$events = EventsManager_events_ops::get_events($status, $category_id, $category_name, $show, $start_datetime, $end_datetime, $myevents, $sortby, $sortorder);

// Add links
$events_smarty = array();
if (count($events) != 0)
{
	$FieldsManager = new AireFieldsManager();
	
	$eventsurlprefix = $this->GetPrefix('events');
	$registrationurlprefix = $this->GetPrefix('register');
	
	foreach ($events as &$event)
	{
		$event_smarty = $event->get_smarty_object();
		
		$aliased_title = munge_string_to_url($event->name);
		
		// Detail URL
		$prettyurl = $eventsurlprefix.'/'.$event->id.'/'.$detailpage.'/'.$aliased_title;
		$sendtodetail['event_id'] = $event->id;
		
		// Detail template
		if (isset($params['detailtemplate']))
		{
			$sendtodetail['detailtemplate'] = $params['detailtemplate'];
			$prettyurl .= '/d,' . $params['detailtemplate'];
		}
		
		$event_smarty->detailurl = $this->CreateFrontendLink($id, $detailpage, 'detail', '', $sendtodetail, '', true, $inline, '', '', $prettyurl);
		
		// Registration URL
		if ($event->registration_allowed())
		{
			$prettyurl = $eventsurlprefix.'/'.$registrationurlprefix.'/'.$event->id.'/'.$registrationpage.'/'.$aliased_title;
			// Params
			$sendtoregistration['event_id'] = $event->id;
			
			// Registration template
			if (isset($params['registrationtemplate']))
			{
				$sendtoregistration['registrationtemplate'] = $params['registrationtemplate'];
				$prettyurl .= '/d,' . $params['registrationtemplate'];
			}
			
			$event_smarty->registration_url = $this->CreateFrontendLink($id, $registrationpage, 'register', '', $sendtoregistration, '', 1, $inline, '', '', $prettyurl);
		}

		// Add custom fields
		$tmp = $FieldsManager->get_fieldsbyname('EventsManager', $event->id, '', '', '');
		if ($tmp)
			$event_smarty->fields = $tmp;

		// Add the number of registered users
		
		$events_smarty[] = $event_smarty;
	}
}

/******************************************************************************
// Display */

// Smarty
if (isset($events_smarty))
	$smarty->assign('items', $events_smarty);

// Display
$template = $this->GetPreference('dflt_summary_template');
if  (!empty($params['summarytemplate']))
	$template = $params['summarytemplate'];
echo $this->ProcessTemplateFromDatabase('summary_' . $template);
//echo $this->ProcessTemplate('orig_summarytemplate.tpl');
?>