<?php
if (!isset($gCms)) exit;
if (!$this->HasPermission()) exit;

$errors = array();
$messages = array();

$category = new EventsManager_category($params['id_category']);

$this->SetCurrentTab('categories');
if ($category->delete())
{
	$this->SendEvent('EventsManagerCategoryDeleted', array('id_category'=>$params['id_category']));
	
	$this->RedirectToTab($id);
}

?>