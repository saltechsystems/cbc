<?php
if (!isset($gCms)) exit;
if (!$this->HasPermission()) exit;

// Submit ?
if (isset($params['cancel']))
	$this->RedirectToTab($id, 'preferences');

if (isset($params['submit']))
{
	// Update the preferences
	$this->SetPreference('menuname', trim($params['menuname']));

	if (isset($params['showtabs']))
		$value_tmp = 1;
	else
		$value_tmp = 0;
	$this->SetPreference('showtabs', $value_tmp);
	
	$this->SetPreference('dflt_detailpage', $params['dflt_detailpage']);
	$this->SetPreference('urlprefix', trim($params['urlprefix']));
	$this->SetPreference('registerurlprefix', trim($params['registerurlprefix']));
	$this->SetPreference('dflt_registrationpage', $params['dflt_registrationpage']);
	$this->SetPreference('overquota_behaviour', $params['overquota_behaviour']);
	
	$this->SetPreference('dflt_feuloginpage', $params['dflt_feuloginpage']);
	
	$tmp = implode(',', $params['displayed_feu_properties']);
	if ($tmp)
		$this->SetPreference('displayed_feu_properties', $tmp);
	else
		$this->SetPreference('displayed_feu_properties', '');
	
	$this->SetPreference('exportencoding', $params['exportencoding']);
	if (isset($params['export_cancelled']))
		$value_export_cancelled = 1;
	else
		$value_export_cancelled = 0;
	$this->SetPreference('export_cancelled', $value_export_cancelled);
	
	$this->RedirectToTab($id, 'preferences');
}

// Form
$smarty->assign('form_start', $this->CreateFormStart($id, 'defaultadmin'));
$smarty->assign('form_end', $this->CreateFormEnd());
$smarty->assign('submit', $this->CreateInputSubmit($id, 'submit', lang('submit')));
$smarty->assign('cancel', $this->CreateInputSubmit($id, 'cancel', lang('cancel')));

// Content operations
$contentops = cmsms()->GetContentOperations();

// General options
// Menu name
$smarty->assign('label_menuname', $this->CreateLabelForInput($id, 'menuname', $this->Lang('label_menuname')));
$smarty->assign('input_menuname', $this->CreateInputText($id, 'menuname', $this->GetPreference('menuname'), 50));

// Show tabs in event edition ?
$smarty->assign('label_showtabs', $this->CreateLabelForInput($id, 'showtabs', $this->Lang('label_showtabs')));
$smarty->assign('input_showtabs', $this->CreateInputCheckbox($id, 'showtabs', 1, $this->GetPreference('showtabs'),'id="'.$id.'showtabs"'));

// Default detail page for an event
$smarty->assign('label_dflt_detailpage', $this->CreateLabelForInput($id, 'dflt_detailpage', $this->Lang('label_dflt_detailpage')));
$smarty->assign('input_dflt_detailpage', $contentops->CreateHierarchyDropdown('', $this->GetPreference('dflt_detailpage'), $id.'dflt_detailpage'));

// URL prefixes (url rewriting)
$smarty->assign('label_urlprefix', $this->CreateLabelForInput($id, 'urlprefix', $this->Lang('urlprefix')));
$smarty->assign('input_urlprefix', $this->CreateInputText($id, 'urlprefix', $this->GetPreference('urlprefix'), 50));
// Register prefix
$smarty->assign('label_registerurlprefix', $this->CreateLabelForInput($id, 'registerurlprefix', $this->Lang('registerurlprefix')));
$smarty->assign('input_registerurlprefix', $this->CreateInputText($id, 'registerurlprefix', $this->GetPreference('registerurlprefix'), 50));

// Default page for an event registration
$smarty->assign('label_dflt_registrationpage', $this->CreateLabelForInput($id, 'dflt_registrationpage', $this->Lang('label_dflt_registrationpage')));
$smarty->assign('input_dflt_registrationpage', $contentops->CreateHierarchyDropdown('', $this->GetPreference('dflt_registrationpage'), $id.'dflt_registrationpage'));
// How to handle overquota registrations
$smarty->assign('label_overquota_behaviour', $this->CreateLabelForInput($id, 'overquota_behaviour', $this->Lang('label_overquota_behaviour')));
$items = array(
	$this->Lang('reg_with_limit')=>'reg_with_limit',
	$this->Lang('ask_confirm')=>'ask_confirm');
$smarty->assign('input_overquota_behaviour', $this->CreateInputDropdown($id, 'overquota_behaviour', $items, '', $this->GetPreference('overquota_behaviour')));

// FEU Properties to display in the registerd users list and in export
$feu_module = cms_utils::get_module('FrontEndUsers');
$feu_properties = $feu_module->GetPropertyDefns();

if ($feu_properties)
{
	$options_list = array();
	foreach ($feu_properties as $key=>$val)
		$options_list[$val['prompt']] = $key;
	$current_options_list = explode(',', $this->GetPreference('displayed_feu_properties'));

	$smarty->assign('label_displayed_feu_properties', $this->CreateLabelForInput($id, 'displayed_feu_properties', $this->Lang('displayed_feu_properties')));
	$smarty->assign('input_displayed_feu_properties', $this->CreateInputSelectList($id, 'displayed_feu_properties[]', $options_list, $current_options_list, 5));
}

// CSV export encoding
$smarty->assign('label_exportencoding', $this->CreateLabelForInput($id, 'exportencoding', $this->Lang('exportencoding')));
$smarty->assign('input_exportencoding', $this->CreateInputText($id, 'exportencoding', $this->GetPreference('exportencoding', 'UTF-8')));

// CSV export : export cancelled?
$smarty->assign('label_export_cancelled', $this->CreateLabelForInput($id, 'export_cancelled', $this->Lang('export_cancelled')));
$smarty->assign('input_export_cancelled', $this->CreateInputCheckbox($id, 'export_cancelled', 1, $this->GetPreference('export_cancelled'),'id="'.$id.'export_cancelled"'));

// Default FEU login page (used when the user makes a registration, but is not connected. The module redirects to the page
$smarty->assign('label_dflt_feuloginpage', $this->CreateLabelForInput($id, 'dflt_feuloginpage', $this->Lang('dflt_feuloginpage')));
$smarty->assign('input_dflt_feuloginpage', $contentops->CreateHierarchyDropdown('', $this->GetPreference('dflt_feuloginpage'), $id.'dflt_feuloginpage'));

// Display the form
echo $this->ProcessTemplate('admin_tab_preferences.tpl');

?>