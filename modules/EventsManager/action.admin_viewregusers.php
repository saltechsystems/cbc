<?php
if (!isset($gCms)) exit;
if (!$this->HasPermission()) exit;

if (isset($params['id_event']))
{
	$event = new EventsManager_event($params['id_event']);
	
	$registrations = $event->get_registrations();
	$feu_module = cms_utils::get_module('FrontEndUsers');
	
	$pref_displayed_feu_properties = $this->GetPreference('displayed_feu_properties');
	if ($pref_displayed_feu_properties)
		$displayed_feu_properties = explode(',', $pref_displayed_feu_properties);
	else
		$displayed_feu_properties = '';

	// Get the FEU properties to display
	$groups = $feu_module->GetGroupList();

	// For each group, search the properties and remove double properties
	$props_to_display = array();
	foreach ($groups as $group=>$id_group)
	{
		// Property in the group
		$tmp = $feu_module->GetGroupPropertyRelations($id_group);

		foreach ($tmp as $oneproparray)
		{
			// test if the value :
			// is OK for display according to the preferences
			// is NOT in the actual props_to_display res array
			if (in_array($oneproparray['name'], $displayed_feu_properties) && (!in_array($oneproparray['name'], $props_to_display)))
				$props_to_display[] = $oneproparray['name'];
		}
	}
	$count_props = count($props_to_display);
	
	// Add some FEU informations
	$registrations_smarty = array();
	if ($registrations && (count($registrations) > 0))
	{
		foreach ($registrations as &$registration)
		{
			$registration_smarty = $registration->get_smarty_object();

			$registration_smarty->username = $feu_module->GetUserName($registration->id_user);
			$tmp_feu_props = $feu_module->GetUserProperties($registration->id_user);

			$res_props = array();
			$invert_props = array_flip($props_to_display);
			foreach ($tmp_feu_props as $oneproparray)
			{
				if (in_array($oneproparray['title'], $props_to_display))
					$res_props[$invert_props[$oneproparray['title']]] = $oneproparray['data'];
			}
			$registration_smarty->feu_props = $res_props;

			// Edit link
			$registration_smarty->editlink = $this->CreateLink($id, 'admin_addeditregistration', $returnid, $gCms->variables['admintheme']->DisplayImage('icons/system/edit.gif', $this->Lang('edit'), '', '', 'systemicon'), array('id_registration'=>$registration->id));

			// Delete link
			$registration_smarty->deletelink = $this->CreateLink($id, 'admin_deleteregistration', $returnid, $gCms->variables['admintheme']->DisplayImage('icons/system/delete.gif', $this->Lang('delete'), '', '', 'systemicon'), array('id_registration'=>$registration->id, 'id_event'=>$registration->id_event), $this->Lang('areyousure'));

			$registrations_smarty[] = $registration_smarty;
		}
	}

	$smarty->assign('event', $event);
	$smarty->assign('items', $registrations_smarty);
	$smarty->assign('feu_props_names', $props_to_display);
	$smarty->assign('count_feu_props', $count_props);

	// Add a new registration link
	$smarty->assign('addlink', $this->CreateLink($id, 'admin_addeditregistration', $returnid, $gCms->variables['admintheme']->DisplayImage('icons/system/newobject.gif', $this->Lang('addregistration'), '', '', 'systemicon') .' '.$this->Lang('addregistration'), array('id_event'=>$event->id)));

	$smarty->assign('backlink', $this->CreateLink($id, 'defaultadmin', $returnid, $gCms->variables['admintheme']->DisplayImage('icons/system/back.gif', $this->Lang('back_toeventslist'), '', '', 'systemicon') .' '.$this->Lang('back_toeventslist')));
	
	$smarty->assign('exportlink', $this->CreateLink($id, 'admin_exportregusers', $returnid, $gCms->variables['admintheme']->DisplayImage('icons/system/export.gif', $this->Lang('export_regusers'), '24', '29', 'systemicon') . ' ' . $this->Lang('export_regusers'), array('id_event'=>$event->id, 'disable_theme'=>1)));
	
	// Export to NMS link - Only if NMS is installed
	if ($nms_module = cms_utils::get_module('NMS'))
	{
		$smarty->assign('nmsexportlink', $this->CreateLink($id, 'admin_exportregusers_nms', $returnid, $gCms->variables['admintheme']->DisplayImage('icons/system/export.gif', $this->Lang('export_regusers_nms'), '24', '29', 'systemicon') . ' ' . $this->Lang('export_regusers_nms'), array('id_event'=>$event->id)));
	}
	
	echo $this->ProcessTemplate('admin_viewregusers.tpl');
}

?>