<?php
if (!isset($gCms)) exit;
if (!$this->HasPermission()) exit;

/* Prepare the display */
$errors = array();
$messages = array();
$this->SetCurrentTab('categories');

// Add or edit ?
if (isset($params['id_category']))
{
	$mode = 'edit';
	$id_category=$params['id_category'];
}
else
{
	$mode = 'add';
	$id_category=false;
}

$category = new EventsManager_category($id_category);
/******************************************************************************
// Submit */
if (isset($params['submit']))
{
	// Get the params
	$category->name = trim($params['name']);
	$category->description = trim($params['description']);
	
	if (empty($category->name))
		$errors[] = $this->Lang('error_emptyname');
	else
	{
		// Verify if the item exists (same name) in the db
		if (!EventsManager_categories_ops::category_exist($category->name, $category->id))
		{
			// Ready - Just have to store the actual id to see if it's an addition or update
			$id_before_addedit = $category->id;
			
			// Name is OK - We can do the insert / update in db
			$res = $category->addedit();
			
			if ($res)
			{
				// Send the event
				$parms['category_id'] = $category->id;
				
				if ($id_before_addedit)
					$this->SendEvent('EventsManagerCategoryEdited', $parms);
				else
					$this->SendEvent('EventsManagerCategoryAdded', $parms);

				$this->RedirectToTab($id);
			}
			else
				$errors[] = $this->Lang('error_db');
		}
		else
			$errors[] = $this->Lang('error_nameused');
	}
}
elseif (isset($params['cancel']))
	$this->RedirectToTab($id);

/******************************************************************************
// Assign to smarty */

/* Form general vars */
$smarty->assign('form_start', $this->CreateFormStart($id, 'admin_addeditcategory', $returnid));
$smarty->assign('submit', $this->CreateInputSubmit($id, 'submit', lang('submit')));
$smarty->assign('cancel', $this->CreateInputSubmit($id, 'cancel', lang('cancel')));
$smarty->assign('form_end', $this->CreateFormEnd());

// Hidden values
$hidden = '';
if ($mode == 'edit')
	$hidden .= $this->CreateInputHidden($id, 'id_category', $category->id);
$smarty->assign('hidden', $hidden);

/* Fields */
$smarty->assign('title', $this->Lang($mode . 'category'));

$smarty->assign('name_label', lang('name'));
$smarty->assign('name_input', $this->CreateInputText($id, 'name', $category->name, 30, 255));

$smarty->assign('description_label', lang('description'));
$smarty->assign('description_input', $this->CreateTextArea(1, $id, $category->description, 'description'));

/******************************************************************************
// Display */

// Errors
foreach ($errors as $error)
	echo $this->ShowErrors($error);

// Form template
echo $this->ProcessTemplate('admin_addeditcategory.tpl');
?>